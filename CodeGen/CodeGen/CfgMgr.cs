﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace CodeGen
{
    public class CfgMgr<T>
    {
        public String FileName
        {
            get { return nm; }
        }
        public T Config
        {
            get { return real; }
            set { real = value; }
        }
        private T real;
        private String nm;


        public CfgMgr(String fileName)
        {
            nm = fileName;
        }

        public bool Save()
        {

            if (File.Exists(nm)) File.Delete(nm);
            FileStream fs = new FileStream(nm, FileMode.CreateNew);
            //IFormatter saver = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            XmlSerializer saver = new XmlSerializer(typeof(T));
            saver.Serialize(fs, real);
            fs.Close();
            return true;

        }

        public bool Load()
        {
            if (!File.Exists(nm)) return false;
            FileStream fs = new FileStream(nm, FileMode.Open);
            //IFormatter saver = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            XmlSerializer loader = new XmlSerializer(typeof(T));
            real = (T)loader.Deserialize(fs);
            fs.Close();
            return true;
        }
    }

    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        public SerializableDictionary()
        {
        }
        public void WriteXml(XmlWriter wrt)
        {
            XmlSerializer ks = new XmlSerializer(typeof(TKey));
            XmlSerializer vs = new XmlSerializer(typeof(TValue));
            foreach (KeyValuePair<TKey, TValue> kv in this)
            {
                wrt.WriteStartElement("SerializableDictionary");
                wrt.WriteStartElement("key");
                ks.Serialize(wrt, kv.Key);
                wrt.WriteEndElement();
                wrt.WriteStartElement("value");
                vs.Serialize(wrt, kv.Value);
                wrt.WriteEndElement();
                wrt.WriteEndElement();
            }
        }
        public void ReadXml(XmlReader rd)
        {
            rd.Read();
            XmlSerializer ks = new XmlSerializer(typeof(TKey));
            XmlSerializer vs = new XmlSerializer(typeof(TValue));
            while (rd.NodeType != XmlNodeType.EndElement)
            {
                rd.ReadStartElement("SerializableDictionary");
                rd.ReadStartElement("key");
                TKey tk = (TKey)ks.Deserialize(rd);
                rd.ReadEndElement();
                rd.ReadStartElement("value");
                TValue tv = (TValue)vs.Deserialize(rd);
                rd.ReadEndElement();
                rd.ReadEndElement();
                this.Add(tk, tv);
                rd.MoveToContent();
            }
            rd.ReadEndElement();
        }
        public XmlSchema GetSchema()
        {
            return null;
        }
    }
}