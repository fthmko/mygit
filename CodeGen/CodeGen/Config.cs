﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class Config : Form
    {
        private GenConfig cfg;
        private ConfigFile frmFile;
        private ConfigParam frmParam;
        public Config(GenConfig gc)
        {
            InitializeComponent();
            cfg = gc;
            lstFile.DataSource = cfg.FileList;
            lstParam.DataSource = cfg.GlobalParamList;
        }

        private void lstFile_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int idx = lstFile.IndexFromPoint(e.Location);
            if (idx != -1)
            {
                if (frmFile == null)
                {
                    frmFile = new ConfigFile(lstFile.Items[idx] as GenFile, cfg);
                }
                else
                {
                    frmFile.File = lstFile.Items[idx] as GenFile;
                }
                if (frmFile.ShowDialog() == DialogResult.OK)
                {

                }
            }
        }

        private void lstParam_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int idx = lstParam.IndexFromPoint(e.Location);
            if (idx != -1)
            {
                if (frmParam == null)
                {
                    frmParam = new ConfigParam(lstParam.Items[idx] as GenParam);
                }
                else
                {
                    frmParam.Param = lstParam.Items[idx] as GenParam;
                }
                if (frmParam.ShowDialog() == DialogResult.OK)
                {

                }
            }
        }
    }
}
