﻿namespace CodeGen
{
    partial class ConfigFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtRelPath = new System.Windows.Forms.TextBox();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.cmbPadtype = new System.Windows.Forms.ComboBox();
            this.cmbEncode = new System.Windows.Forms.ComboBox();
            this.txtPadStr = new System.Windows.Forms.TextBox();
            this.txtPadRow = new System.Windows.Forms.NumericUpDown();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.txtPadRow)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "相对路径";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "内容模板";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "附加类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "参数1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(412, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "参数2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "文件编码";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(100, 8);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(100, 21);
            this.txtName.TabIndex = 3;
            // 
            // txtRelPath
            // 
            this.txtRelPath.Location = new System.Drawing.Point(100, 35);
            this.txtRelPath.Name = "txtRelPath";
            this.txtRelPath.Size = new System.Drawing.Size(523, 21);
            this.txtRelPath.TabIndex = 3;
            // 
            // btnTemplate
            // 
            this.btnTemplate.Location = new System.Drawing.Point(100, 101);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnTemplate.TabIndex = 4;
            this.btnTemplate.Text = "编辑...";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(181, 101);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 4;
            this.btnPreview.Text = "预览";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // cmbPadtype
            // 
            this.cmbPadtype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPadtype.FormattingEnabled = true;
            this.cmbPadtype.Location = new System.Drawing.Point(100, 61);
            this.cmbPadtype.Name = "cmbPadtype";
            this.cmbPadtype.Size = new System.Drawing.Size(168, 20);
            this.cmbPadtype.TabIndex = 5;
            // 
            // cmbEncode
            // 
            this.cmbEncode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEncode.FormattingEnabled = true;
            this.cmbEncode.Location = new System.Drawing.Point(326, 7);
            this.cmbEncode.Name = "cmbEncode";
            this.cmbEncode.Size = new System.Drawing.Size(121, 20);
            this.cmbEncode.TabIndex = 5;
            // 
            // txtPadStr
            // 
            this.txtPadStr.Location = new System.Drawing.Point(453, 61);
            this.txtPadStr.Name = "txtPadStr";
            this.txtPadStr.Size = new System.Drawing.Size(170, 21);
            this.txtPadStr.TabIndex = 3;
            // 
            // txtPadRow
            // 
            this.txtPadRow.Location = new System.Drawing.Point(326, 61);
            this.txtPadRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtPadRow.Name = "txtPadRow";
            this.txtPadRow.Size = new System.Drawing.Size(64, 21);
            this.txtPadRow.TabIndex = 6;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(467, 101);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "保存(&S)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(548, 101);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // ConfigFile
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(656, 132);
            this.Controls.Add(this.txtPadRow);
            this.Controls.Add(this.cmbEncode);
            this.Controls.Add(this.cmbPadtype);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnTemplate);
            this.Controls.Add(this.txtPadStr);
            this.Controls.Add(this.txtRelPath);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigFile";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "文件设置";
            this.Shown += new System.EventHandler(this.ConfigFile_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.txtPadRow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtRelPath;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.ComboBox cmbPadtype;
        private System.Windows.Forms.ComboBox cmbEncode;
        private System.Windows.Forms.TextBox txtPadStr;
        private System.Windows.Forms.NumericUpDown txtPadRow;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}