﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class ConfigFile : Form
    {
        public GenFile File { get; set; }
        private GenConfig cfg;
        private string template;
        private FileTemplate frmTemplate;
        public ConfigFile(GenFile gf, GenConfig config)
        {
            InitializeComponent();
            File = gf;
            cfg = config;
            //cmbPadtype.DataSource = Enum.GetValues(typeof(PadType));
            cmbPadtype.DataSource = PadTypeConst.PAD_TYPE_LIST;
            cmbPadtype.DisplayMember = "Key";
            cmbPadtype.ValueMember = "Value";

            cmbEncode.Items.Add("gb2312");
            cmbEncode.Items.Add("utf-8");
            frmTemplate = new FileTemplate();
        }

        private void ConfigFile_Shown(object sender, EventArgs e)
        {
            txtName.Text = File.Name;
            txtPadRow.Value = File.PadRow;
            txtPadStr.Text = File.PadStr;
            txtRelPath.Text = File.RelPath;
            template = File.Template;
            cmbPadtype.SelectedValue = File.PadType;
            cmbEncode.SelectedItem = File.Encoding.BodyName;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            File.PadRow = (int)txtPadRow.Value;
            File.PadStr = txtPadStr.Text;
            File.RelPath = txtRelPath.Text;
            File.Encode = cmbEncode.SelectedItem.ToString();
            File.Template = template;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            frmTemplate.Template = template;
            frmTemplate.Edit = true;
            if (frmTemplate.ShowDialog() == DialogResult.OK)
            {
                template = frmTemplate.Template;
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            string t1 = File.Template;
            File.Template = template;
            frmTemplate.Template = GenUtil.GenFileContent(File, cfg);
            File.Template = t1;
            frmTemplate.Edit = false;
            frmTemplate.ShowDialog();
        }
    }
}
