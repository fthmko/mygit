﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class ConfigParam : Form
    {
        public GenParam Param { get; set; }
        public ConfigParam(GenParam gp)
        {
            InitializeComponent();
            Param = gp;
        }

        private void ckIsList_CheckedChanged(object sender, EventArgs e)
        {
            this.Height = ckIsList.Checked ? 228 : 106;
            pnlList.Visible = ckIsList.Checked;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Param.IsList = ckIsList.Checked;
            if (ckIsList.Checked)
            {
                var dc = new SerializableDictionary<string, string>();
                foreach (string key in txtParams.Text.Split(','))
                {
                    if (key.Length > 0) dc.Add(key.Trim(), "");
                }
                if (Param.ValueList.Count > 0) Param.ValueList.RemoveAt(0);
                Param.ValueList.Insert(0, dc);
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void ConfigParam_Shown(object sender, EventArgs e)
        {
            this.txtName.Text = Param.Name;
            this.ckIsList.Checked = Param.IsList;
            this.Height = ckIsList.Checked ? 228 : 106;
            pnlList.Visible = ckIsList.Checked;
            if (ckIsList.Checked && Param.ValueList.Count > 0)
            {
                var dc = Param.ValueList[0];
                foreach (string str in dc.Keys)
                {
                    txtParams.Text += str + ",";
                }
                txtParams.Text = txtParams.Text.Substring(0, txtParams.Text.Length - 1);
            }
        }
    }
}
