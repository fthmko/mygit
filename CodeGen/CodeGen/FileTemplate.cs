﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class FileTemplate : Form
    {
        public string Template{get;set;}
        public bool Edit { get; set; }
        public FileTemplate()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Template = txtTemplate.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void FileTemplate_Shown(object sender, EventArgs e)
        {
            txtTemplate.ReadOnly = !Edit;
            btnOk.Visible = Edit;
            btnCancel.Text = Edit ? "取消(&C)" : "关闭(&C)";
            txtTemplate.Text = Template;
        }
    }
}
