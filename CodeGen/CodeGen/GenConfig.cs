﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace CodeGen
{
    [Serializable]
    public class GenConfig
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public List<GenParam> GlobalParamList { get; set; }
        public List<GenFile> FileList { get; set; }
        public string BaseDir { get; set; }

        public GenConfig()
        {
            GlobalParamList = new List<GenParam>();
            FileList = new List<GenFile>();
        }
    }

    [Serializable]
    public class GenParam
    {
        public string Name { get; set; }
        public List<SerializableDictionary<string, string>> ValueList { get; set; }
        public string Value { get; set; }
        public bool IsList { get; set; }
        public GenParam()
        {
            IsList = false;
            Value = "";
            ValueList = new List<SerializableDictionary<string, string>>();
        }
        public GenParam(string name, string value)
        {
            Name = name;
            Value = value;
            IsList = false;
            ValueList = new List<SerializableDictionary<string, string>>();
        }
        public string Process(string content)
        {
            if (IsList)
            {
                MatchCollection mcs = Regex.Matches(content.Replace("\r\n", "!-#"), GenUtil.LOOP_BEF + Name + GenUtil.LOOP_MID + "(.+?)" + GenUtil.LOOP_AFT + "(.*?)" + GenUtil.LOOP_AFT, RegexOptions.None);
                if (mcs.Count > 0)
                {
                    string mcstr, mcall, appstr;
                    foreach (Match mc in mcs)
                    {
                        mcstr = "";
                        mcall = "";
                        for (int i = 1; i < ValueList.Count; i++)
                        {
                            mcstr = mc.Groups[1].Value.Replace("!-#", "\r\n");
                            appstr = mc.Groups[2].Value;
                            foreach (String ky in ValueList[i].Keys)
                            {
                                mcstr = SingleRep(mcstr, ky, ValueList[i][ky]);
                            }
                            mcall += mcstr;
                            if (i != ValueList.Count - 1)
                            {
                                mcall += appstr;
                            }
                        }
                        content = content.Replace(mc.Groups[0].Value.Replace("!-#", "\r\n"), mcall);
                    }
                }
            }
            else
            {
                content = SingleRep(content, Name, Value);
            }
            return content;
        }
        private string SingleRep(string content, string param, string value)
        {
            content = content.Replace(GenUtil.PARAM_BEF + param + GenUtil.PARAM_AFT, value);
            content = content.Replace(GenUtil.PARAM_BEF + GenUtil.PARAM_UPP + param + GenUtil.PARAM_AFT, GenUtil.FUpper(value));
            content = content.Replace(GenUtil.PARAM_BEF + GenUtil.PARAM_LOW + param + GenUtil.PARAM_AFT, GenUtil.FLower(value));
            content = content.Replace(GenUtil.PARAM_BEF + GenUtil.PARAM_SPLIT_UPP + param + GenUtil.PARAM_AFT, GenUtil.FSUpper(value));
            content = content.Replace(GenUtil.PARAM_BEF + GenUtil.PARAM_SPLIT_LOW + param + GenUtil.PARAM_AFT, GenUtil.FSLower(value));
            return content;
        }
    }

    [Serializable]
    public class GenFile
    {
        public bool Checked { get; set; }
        public string Name { get; set; }
        public string RelPath { get; set; }
        public XmlCDataSection _Template;
        [XmlIgnore]
        public string Template
        {
            get
            {
                return _Template == null ? "" : _Template.Data.Replace("《！【CDATA【", "<![CDATA[").Replace("】】》", "]]>").Replace("\n", Environment.NewLine);
            }
            set
            {
                if (_Template == null)
                {
                    var doc = new XmlDataDocument();
                    _Template = doc.CreateCDataSection("");
                }
                _Template.Data = value.Replace("<![CDATA[", "《！【CDATA【").Replace("]]>", "】】》");
            }
        }
        public int PadType { get; set; }
        public int PadRow { get; set; }
        public string PadStr { get; set; }
        //public List<GenParam> FileParamList { get; set; }
        //public GenConfig Global { get; set; }
        public string Encode { get; set; }

        public Encoding Encoding
        {
            get
            {
                return Encoding.GetEncoding(Encode);
            }
        }
        public string FilePath { get; set; }

        public GenFile()
        {
            //FileParamList = new List<GenParam>();
            Checked = true;
        }
    }

    public class PadTypeConst
    {
        public const int FIX_ROW = 0, LAST_ROW = 1, AFT_FIRST = 2, BEF_FIRST = 3, AFT_LAST = 4, BEF_LAST = 5, REPLACE = 6;
        private static List<KeyValuePair<string, int>> padTypeList;
        public static List<KeyValuePair<string, int>> PAD_TYPE_LIST
        {
            get
            {
                if (padTypeList == null)
                {
                    padTypeList = new List<KeyValuePair<string, int>>();
                    padTypeList.Add(new KeyValuePair<string, int>("第一个{参数2}后面", PadTypeConst.AFT_FIRST));
                    padTypeList.Add(new KeyValuePair<string, int>("最后一个{参数2}后面", PadTypeConst.AFT_LAST));
                    padTypeList.Add(new KeyValuePair<string, int>("第一个{参数2}前面", PadTypeConst.BEF_FIRST));
                    padTypeList.Add(new KeyValuePair<string, int>("最后一个{参数2}前面", PadTypeConst.BEF_LAST));
                    padTypeList.Add(new KeyValuePair<string, int>("第{参数1}行", PadTypeConst.FIX_ROW));
                    padTypeList.Add(new KeyValuePair<string, int>("倒数第{参数1}行", PadTypeConst.LAST_ROW));
                    padTypeList.Add(new KeyValuePair<string, int>("替换{参数2}", PadTypeConst.REPLACE));
                }
                return padTypeList;
            }
        }
    }

    public class GenUtil
    {
        public const string PARAM_BEF = "{";
        public const string PARAM_AFT = "}";
        /// <summary>
        /// testString -> TestString
        /// </summary>
        public const string PARAM_UPP = "#";
        /// <summary>
        /// TestString -> testString
        /// </summary>
        public const string PARAM_LOW = "%";
        /// <summary>
        /// testStringIsThis -> TEST_STRING_IS_THIS
        /// </summary>
        public const string PARAM_SPLIT_UPP = "@";
        /// <summary>
        /// testStringisThis -> test_string_is_this
        /// </summary>
        public const string PARAM_SPLIT_LOW = "&";
        public const string LOOP_BEF = "\\^\\^";
        public const string LOOP_MID = "\\@\\@";
        public const string LOOP_AFT = "\\$\\$";

        public static string FUpper(string str)
        {
            if (str != null)
            {
                if (str.Length > 1)
                {
                    return str.Substring(0, 1).ToUpper() + str.Substring(1);
                }
                else
                {
                    return str.ToUpper();
                }
            }
            return null;
        }
        public static string FLower(string str)
        {
            if (str != null)
            {
                if (str.Length > 1)
                {
                    return str.Substring(0, 1).ToLower() + str.Substring(1);
                }
                else
                {
                    return str.ToLower();
                }
            }
            return null;
        }
        public static string FSUpper(string str)
        {
            if (str != null)
            {
                string ret = "";
                string cr;
                for (int i = 0; i < str.Length; i++)
                {
                    cr = str.Substring(i, 1);
                    if (cr == cr.ToUpper() && i > 0)
                    {
                        ret += "_";
                    }
                    ret += cr;
                }
                return ret.ToUpper();
            }
            return null;
        }
        public static string FSLower(string str)
        {
            if (str != null)
            {
                return FSUpper(str).ToLower();
            }
            return null;
        }

        public static string GenFileContent(GenFile gf, GenConfig gc)
        {
            string content = gf.Template;
            gf.FilePath = gf.RelPath;
            foreach (GenParam gp in gc.GlobalParamList)
            {
                content = gp.Process(content);
                gf.FilePath = gp.Process(gf.FilePath);
            }
            //foreach (GenParam gp in FileParamList)
            //{
            //    content = gp.Process(content);
            //    FilePath = gp.Process(FilePath);
            //}
            return content;
        }


        public static bool WriteFile(GenFile gf, GenConfig gc)
        {
            FileInfo fi = new FileInfo(gc.BaseDir + gf.FilePath);
            if (fi.Exists)
            {
                fi.Attributes = FileAttributes.Normal;
                fi.CopyTo(fi.FullName + ".bak", true);
            }
            else
            {
                fi.CreateText().Close();
            }
            string wt = GenFileContent(gf, gc);
            if (gf.PadType == PadTypeConst.FIX_ROW || gf.PadType == PadTypeConst.LAST_ROW)
            {
                string[] fileLine = File.ReadAllText(gc.BaseDir + gf.FilePath, gf.Encoding).Replace("\r\n", "\n").Replace("\r", "\n").Split('\n');
                List<string> fileContent = new List<string>();
                fileContent.AddRange(fileLine);

                string file = "";

                if (gf.PadType == PadTypeConst.FIX_ROW)
                {
                    fileContent.Insert(gf.PadRow - 1, wt);
                }
                else
                {
                    if (gf.PadRow != 1)
                    {
                        fileContent.Insert(fileContent.Count - gf.PadRow, wt);
                    }
                    else
                    {
                        fileContent.Add(wt);
                    }
                }
                for (int i = 0; i < fileContent.Count - 1; i++)
                {
                    file = file + fileContent[i] + Environment.NewLine;
                }
                file = file + fileContent[fileContent.Count - 1];

                File.WriteAllText(gc.BaseDir + gf.FilePath, file, gf.Encoding);
            }
            else if (gf.PadType == PadTypeConst.REPLACE)
            {
                File.WriteAllText(gc.BaseDir + gf.FilePath, wt, gf.Encoding);
            }
            else
            {
                string fileContent = File.ReadAllText(gc.BaseDir + gf.FilePath, gf.Encoding);
                int idx = -1;
                if (gf.PadType == PadTypeConst.AFT_LAST || gf.PadType == PadTypeConst.BEF_LAST)
                {
                    idx = fileContent.LastIndexOf(gf.PadStr);
                }
                else
                {
                    idx = fileContent.IndexOf(gf.PadStr);
                }
                if (gf.PadType == PadTypeConst.BEF_LAST || gf.PadType == PadTypeConst.BEF_FIRST)
                {
                    fileContent = fileContent.Insert(idx, wt);
                }
                else
                {
                    fileContent = fileContent.Insert(idx + gf.PadStr.Length, wt);
                }
                File.WriteAllText(gc.BaseDir + gf.FilePath, fileContent, gf.Encoding);
            }
            return true;
        }

        public static GenConfig EcifConfig(string baseDir)
        {
            GenConfig gf = new GenConfig();
            gf.BaseDir = baseDir;
            gf.Name = "ECIF";
            gf.Desc = @"D:\CCShare\elis_ecif_dev!.b.0\elis_ecif\ecif_j2ee";

            gf.GlobalParamList.Add(new GenParam("接口描述", "根据卡号和卡类型查找该卡持有人的客户clientInfo信息"));
            gf.GlobalParamList.Add(new GenParam("接口名", "getClientInfoByCardNoAndCardTypeForGcc"));
            gf.GlobalParamList.Add(new GenParam("接口种类", "special"));
            gf.GlobalParamList.Add(new GenParam("返回类型", "HashMap"));
            gf.GlobalParamList.Add(new GenParam("作者", "zhangzheng419"));
            gf.GlobalParamList.Add(new GenParam("调用者", "gcc"));

            GenParam gp = new GenParam("参数列表", "");
            gf.GlobalParamList.Add(gp);
            gp.IsList = true;
            var dc = new SerializableDictionary<string, string>();
            gp.ValueList.Add(dc);
            dc.Add("参数名", "");
            dc.Add("参数", "");
            dc.Add("参数类型", "");
            dc.Add("参数描述", "");
            dc = new SerializableDictionary<string, string>();
            gp.ValueList.Add(dc);
            dc.Add("参数名", "卡号");
            dc.Add("参数", "cardNo");
            dc.Add("参数类型", "String");
            dc.Add("参数描述", "");
            dc = new SerializableDictionary<string, string>();
            gp.ValueList.Add(dc);
            dc.Add("参数名", "卡类型");
            dc.Add("参数", "cardType");
            dc.Add("参数类型", "String");
            dc.Add("参数描述", "\"cardTypeAid\":援助卡 \"cardTypePacc\":万里通卡");

            GenFile gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "Action";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "    /**\r\n     * {接口描述}\r\n     * @author {作者} <br>\r\n     * @param  serviceRequest of HashMap key值如下： <br/>\r\n^^参数列表@@     *  {%参数}({#参数类型}) : {参数名} {参数描述}<br />\r\n$$$$     *\r\n     * @return result of {返回类型} key值如下：<br/>\r\n     *         TODO HERE 结果key值说明\r\n     *\r\n     *         ecif_exception of String : 异常描述信息\r\n     *\r\n     * @throws LBSBusinessException：查询结果过多（超过100条）或查询条件输入太少和其他异常 <br/>\r\n     *\r\n     * @since 关联系统说明 本接口有{调用者}调用\r\n     */\r\n    public ServiceResponse {%接口名}(ServiceRequest serviceRequest) throws LBSBusinessException;\r\n";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\biz\action\ECIF{#接口种类}ClientInfoAction.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "ActionImpl";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n     * @param serviceRequest\r\n     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public ServiceResponse {%接口名}(ServiceRequest serviceRequest) throws LBSBusinessException {\r\n\r\n        long startTime = System.currentTimeMillis();\r\n\r\n        ServiceResponse serviceResponse = new ServiceResponse();\r\n        Map model = new HashMap();\r\n\r\n        try {\r\n            if (serviceRequest == null) {\r\n                throw new PafaRuntimeException(\"系统异常,传入参数为空\");\r\n            }\r\n\r\n            Map map = (Map) serviceRequest.getCurrentRequestObject();\r\n^^参数列表@@            {#参数类型} {%参数} = {#参数类型} map.get(ECIFKeyNames.{@参数});\r\n$$$$\r\n\r\n^^参数列表@@            if (StringUtils.isEmpty({%参数})) {\r\n                throw new ObjectNotFoundException(\"{参数名}为空！\");\r\n            }\r\n$$$$\r\n\r\n            //调用服务完成处理\r\n            List returnList = this.get{#接口种类}ClientInfoService().{%接口名}(^^参数列表@@{%参数}$$,$$);\r\n\r\n            if (returnList == null || returnList.size() <= 0) {\r\n                returnList = null;\r\n            }\r\n\r\n            model.put(ECIFKeyNames.RESULT, returnList);\r\n            serviceResponse.setSuccess(true);\r\n            long endTime = System.currentTimeMillis();\r\n\r\n            //打印日志\r\n            Tracer.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}\" +\r\n^^参数列表@@                    \"parameter {%参数} is\" + {%参数} + \r\n$$$$                    \" result is \" + model, \"该方法执行时间：\" + (endTime - startTime) + \"ms.\");\r\n        } catch (LBSBusinessException e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}\", e.toString(), e, \"\");\r\n            model.put(ECIFKeyNames.ECIF_EXCEPTION, e.toString());\r\n            serviceResponse.setSuccess(false);\r\n        } catch (ObjectNotFoundException e) {\r\n            AuditLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}\", e.getMessage());\r\n            model.put(ECIFKeyNames.ECIF_EXCEPTION, e.getMessage());\r\n            serviceResponse.setSuccess(false);\r\n        } catch (PafaRuntimeException e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}\", e.toString(), e, \"\");\r\n            model.put(ECIFKeyNames.ECIF_EXCEPTION, e.getMessage());\r\n            serviceResponse.setSuccess(false);\r\n        }\r\n        serviceResponse.setModel(model);\r\n        return serviceResponse;\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\biz\action\impl\ECIF{#接口种类}ClientInfoPojoAction.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "Service";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException;\r\n";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\biz\service\ECIF{#接口种类}ClientInfoService.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "ServiceImpl";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException {\r\n        List returnList = new ArrayList();\r\n\r\n        try {\r\n// TODO HERE DELETE\r\n//             if (!ECIFKeyNames.ALL.equals(clientRole) && !ECIFKeyNames.APPLICANT.equals(clientRole) && !ECIFKeyNames.INSURED.equals(clientRole)\r\n//                     && !ECIFKeyNames.JNTINS.equals(clientRole)) {\r\n//                 throw new LBSBusinessException(\"角色不正确： \" + clientRole + \"。\", null);\r\n//             }\r\n\r\n// TODO CORE CODE\r\n        {返回类型} queryResult = get{#接口种类}ClientInfoDAO().{%接口名}(^^参数列表@@{%参数}$$, $$);\r\n\r\n        } catch (Exception e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}_SERVICE_EXCEPTION \"\r\n^^参数列表@@                + \"{%参数}:\" + {%参数}\r\n$$$$                , e.toString(), e, \"\");\r\n            throw new LBSBusinessException(e);\r\n        }\r\n\r\n        return returnList != null && returnList.size() > 0 ? returnList : null;\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\biz\service\impl\ECIF{#接口种类}ClientInfoServiceImpl.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "Dao";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException;\r\n";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\integration\dao\ECIF{#接口种类}ClientInfoDAO.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "DaoImpl";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException {\r\n        try {\r\n            Map param = new HashMap();\r\n^^参数列表@@            param.put(ECIFKeyNames.{@参数}, {%参数});\r\n$$$$            return ({返回类型}) this.getSqlMapClientTemplate().queryFor{返回类型}(ECIF{#接口种类}SqlID.SQLMAP_{@接口名}, param);\r\n        } catch (DataAccessException e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}_DAO_EXCEPTION \"\r\n^^参数列表@@                + \"{%参数}:\" + {%参数}\r\n$$$$                , e.getMessage(), e, \"\");\r\n            throw new LBSBusinessException(\"{接口描述}异常：\", e);\r\n        } catch (Exception e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}_DAO_EXCEPTION \"\r\n^^参数列表@@                + \"{%参数}:\" + {%参数}\r\n$$$$                , e.getMessage(), e, \"\");\r\n            throw new LBSBusinessException(\"{接口描述}异常！\", e);\r\n        }\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\integration\dao\impl\ECIF{#接口种类}ClientInfoIbatisDAO.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "Facade";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException;\r\n";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\facade\ECIFQueryFacade.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "FacadeImpl";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n^^参数列表@@     * @param {%参数}\r\n$$$$     * @return\r\n     * @throws LBSBusinessException\r\n     */\r\n    public {返回类型} {%接口名}(^^参数列表@@{#参数类型} {%参数}$$, $$) throws LBSBusinessException {\r\n        {返回类型} returnList = new ArrayList();\r\n\r\n        try {\r\n// TODO HERE DELETE\r\n//             if (!ECIFKeyNames.ALL.equals(clientRole) && !ECIFKeyNames.APPLICANT.equals(clientRole) && !ECIFKeyNames.INSURED.equals(clientRole)\r\n//                     && !ECIFKeyNames.JNTINS.equals(clientRole)) {\r\n//                 throw new LBSBusinessException(\"角色不正确： \" + clientRole + \"。\", null);\r\n//             }\r\n\r\n// TODO CORE CODE\r\n        {#返回类型} queryResult = get{#接口种类}ClientInfoDAO().{%接口名}(^^参数列表@@{%参数}$$, $$);\r\n\r\n        } catch (Exception e) {\r\n            ErrorLogger.log(ECIFContextUtils.getUserID(), Level.INFO, this.getClass().getName(), \"{%接口名}_SERVICE_EXCEPTION \"\r\n^^参数列表@@                + \"{%参数}:\" + {%参数}\r\n$$$$                , e.toString(), e, \"\");\r\n            throw new LBSBusinessException(e);\r\n        }\r\n\r\n        return queryResult != null && @queryResult.isEmpty() ? queryResult : null;\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\facade\ECIFQueryFacadeImpl.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "sqlmap";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n	<!-- {接口描述} -->\r\n	<select id=\"ecif-{&接口名}\" parameterClass=\"java.util.Map\" resultClass=\"java.util.HashMap\">\r\n	<![CDATA[\r\n^^参数列表@@		#{%参数}#\r\n$$$$\r\n	]]>\r\n   </select>";
            gl.RelPath = @"\src\config\biz\sqlmap-mapping-{%接口种类}.xml";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            // gl.Global = gf;
            gl.Name = "biz-context";
            gl.PadRow = 24;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.RelPath = @"\src\config\biz\biz-context-{%接口种类}.xml";
            gl.Template = "	<bean id=\"com.palic.elis.ecif.biz.action.ECIF{#接口种类}ClientInfoAction.{%接口名}\"\r\n		class=\"com.palic.elis.ecif.biz.action.impl.ECIF{#接口种类}ClientInfoPojoAction\">\r\n		<property name=\"methodName\">\r\n			<value>{%接口名}</value>\r\n		</property>\r\n		<property name=\"{%接口种类}ClientInfoService\">\r\n			<ref local=\"com.palic.elis.ecif.biz.service.ECIF{#接口种类}ClientInfoService\" />\r\n		</property>\r\n	</bean>";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "sqlid";
            gl.PadRow = 2;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    // {接口描述}\r\n    public static final String SQLMAP_{@接口名} = \"ecif-{%接口种类}.ecif-{&接口名}\";\r\n";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\integration\util\ECIF{#接口种类}SqlID.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "testaction";
            gl.PadRow = 7;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n     * @throws ObjectNotFoundException\r\n     * @throws RemoteException\r\n     * @throws PafaException\r\n     */\r\n    public void test{#接口名}() throws ObjectNotFoundException, RemoteException, PafaException {\r\n        Map paraMap = new HashMap();\r\n^^参数列表@@        {#参数类型} {%参数} = null;\r\n$$$$\r\n^^参数列表@@        paraMap.put(\"{%参数}\", {%参数});\r\n$$$$\r\n\r\n        String requestID = \"com.palic.elis.ecif.biz.action.ECIF{#接口种类}ClientInfoAction.{%接口名}\";\r\n        Map result = getModel(ac, paraMap, requestID);\r\n        System.out.println(result.get(\"result\"));\r\n        assertNotNull(result.get(\"result\"));\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\junit\testcase\ECIF{#接口种类}ActionTestCase.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            gl = new GenFile();
            //gl.Global = gf;
            gl.Name = "testservice";
            gl.PadRow = 7;
            gl.PadType = PadTypeConst.LAST_ROW;
            gl.Template = "\r\n    /**\r\n     * {接口描述}\r\n     * @throws ObjectNotFoundException\r\n     * @throws RemoteException\r\n     * @throws PafaException\r\n     */\r\n    public void test{#接口名}() throws ObjectNotFoundException, RemoteException, PafaException {\r\n^^参数列表@@        {#参数类型} {%参数} = null;\r\n$$$$        {返回类型} retResult = this.getFacade().{%接口名}(^^参数列表@@{%参数}$$, $$);\r\n        System.out.println(retResult);\r\n        assertNotNull(retResult);\r\n    }";
            gl.RelPath = @"\src\java\com\palic\elis\ecif\junit\testcase\ECIF{#接口种类}ServiceTestCase.java";
            gl.Encode = "gb2312";
            gf.FileList.Add(gl);

            return gf;
        }
    }
}
