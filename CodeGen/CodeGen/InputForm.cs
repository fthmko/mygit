﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class InputForm : Form
    {
        private int mxButtom = 0;
        private List<GenParam> genParams;

        public InputForm(GenConfig gc)
        {
            InitializeComponent();
            genParams = gc.GlobalParamList;
            this.Icon = global::CodeGen.Properties.Resources.edit_clear;
        }
        private void addInp(GenParam gp)
        {
            Label lbl = new Label();
            pnlMain.Controls.Add(lbl);
            lbl.Text = gp.Name;
            lbl.AutoSize = false;
            lbl.TextAlign = ContentAlignment.MiddleRight;
            lbl.Height = 21;
            lbl.Width = 105;
            lbl.Left = 0;
            lbl.Top = mxButtom;
            if (gp.IsList)
            {
                DataGridView dgv = new DataGridView();
                pnlMain.Controls.Add(dgv);
                dgv.Name = gp.Name;
                dgv.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                dgv.RowHeadersWidth = 22;
                dgv.Width = 345;
                dgv.Height = 140;
                dgv.Left = 110;
                dgv.Top = mxButtom;
                dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                if (gp.ValueList.Count > 0)
                {
                    var cg = gp.ValueList[0];
                    foreach (string ky in cg.Keys)
                    {
                        var dh = new DataGridViewTextBoxColumn();
                        dh.HeaderText = ky;
                        dh.Name = ky;
                        dh.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        dgv.Columns.Add(dh);
                    }
                    for (int i = 1; i < gp.ValueList.Count; i++)
                    {
                        var dr = gp.ValueList[i];
                        List<String> row = new List<string>();
                        foreach (string vl in dr.Values)
                        {
                            row.Add(vl);
                        }
                        dgv.Rows.Add(row.ToArray());
                    }
                }

                mxButtom += 146;
            }
            else
            {
                TextBox txt = new TextBox();
                pnlMain.Controls.Add(txt);
                txt.Name = gp.Name;
                txt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                txt.Text = gp.Value;
                txt.Width = 345;
                txt.Left = 110;
                txt.Top = mxButtom;
                mxButtom += 27;
            }
        }

        private void InputForm_Shown(object sender, EventArgs e)
        {
            foreach (GenParam gp in genParams)
            {
                addInp(gp);
            }
            this.Width = 600;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (GenParam gp in genParams)
            {
                if (gp.IsList && gp.ValueList.Count > 0 && gp.ValueList[0].Count > 0)
                {
                    var dv = gp.ValueList[0];
                    gp.ValueList.Clear();
                    gp.ValueList.Add(dv);
                    var dgv = pnlMain.Controls[gp.Name] as DataGridView;
                    SerializableDictionary<string, string> rv;
                    for (int i = 0; i < dgv.Rows.Count - 1; i++)
                    {
                        var row = dgv.Rows[i];
                        rv = new SerializableDictionary<string, string>();
                        foreach (string ky in dv.Keys)
                        {
                            rv.Add(ky, row.Cells[ky].Value as string);
                        }
                        gp.ValueList.Add(rv);
                    }
                }
                else
                {
                    gp.Value = (pnlMain.Controls[gp.Name] as TextBox).Text;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
