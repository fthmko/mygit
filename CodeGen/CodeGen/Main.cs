﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CodeGen
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            this.Icon = global::CodeGen.Properties.Resources.edit_clear;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            string cfgPath = Environment.CurrentDirectory + "\\config";

            foreach (string file in Directory.GetFiles(cfgPath, "*.xml"))
            {
                CfgMgr<GenConfig> cfg = new CfgMgr<GenConfig>(file);
                if (cfg.Load())
                {
                    cmbConfig.Items.Add(cfg.Config);
                }
            }
            cmbConfig.DisplayMember = "Name";
            if (cmbConfig.Items.Count > 0) cmbConfig.SelectedIndex = 0;
        }

        private void cmbConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDesc.Text = cmbConfig.SelectedItem == null ? "" : (cmbConfig.SelectedItem as GenConfig).Desc;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cmbConfig.SelectedItem != null)
            {
                Preview pw = new Preview(cmbConfig.SelectedItem as GenConfig);
                pw.ShowDialog();
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (cmbConfig.SelectedItem != null)
            {
                GenConfig gc = cmbConfig.SelectedItem as GenConfig;
                gc.BaseDir = txtPath.Text;
                foreach (GenFile gf in gc.FileList)
                {
                    GenUtil.WriteFile(gf, gc);
                }
                MessageBox.Show("生成完毕！");
            }
        }

        private void btnMore_Click(object sender, EventArgs e)
        {
            if (cmbConfig.SelectedItem != null)
            {
                Config pw = new Config(cmbConfig.SelectedItem as GenConfig);
                pw.ShowDialog();
            }
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            if (cmbConfig.SelectedItem != null)
            {
                InputForm pw = new InputForm(cmbConfig.SelectedItem as GenConfig);
                pw.ShowDialog();
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (dlgFolder.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = dlgFolder.SelectedPath;
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            string cfgPath = Environment.CurrentDirectory + "\\config\\";

            foreach (GenConfig gc in cmbConfig.Items)
            {
                CfgMgr<GenConfig> cfg = new CfgMgr<GenConfig>(cfgPath + gc.Name + ".xml");
                cfg.Config = gc;
                cfg.Save();
            }
        }
    }
}
