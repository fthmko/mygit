﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGen
{
    public partial class Preview : Form
    {
        private GenConfig config;
        public Preview(GenConfig gc)
        {
            InitializeComponent();
            config = gc;

            foreach (GenFile gf in gc.FileList)
            {
                cmbFile.Items.Add(gf);
            }
            cmbFile.DisplayMember = "Name";

            this.Icon = global::CodeGen.Properties.Resources.edit_clear;
        }

        private void cmbFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbFile.SelectedItem != null)
            {
                GenFile gf = cmbFile.SelectedItem as GenFile;
                txtPreview.Text = GenUtil.GenFileContent(gf, config);
                txtPath.Text = gf.FilePath;
            }
        }
    }
}
