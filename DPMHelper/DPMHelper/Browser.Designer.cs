﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DPMHelper {
    partial class Browser {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.IE = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // IE
            // 
            this.IE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IE.Location = new System.Drawing.Point(0, 0);
            this.IE.MinimumSize = new System.Drawing.Size(20, 20);
            this.IE.Name = "IE";
            this.IE.Size = new System.Drawing.Size(944, 521);
            this.IE.TabIndex = 0;
            // 
            // Browser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 521);
            this.Controls.Add(this.IE);
            this.Name = "Browser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DPMHelper";
            this.ResumeLayout(false);

        }

        #endregion

        public WebBrowser IE;

    }
}