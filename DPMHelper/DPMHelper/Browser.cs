﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper
{
    [System.Runtime.InteropServices.ComVisible(true)]
    public partial class Browser : Form
    {
        const int InternetOptionProxy = 38;
        const int InternetOpenTypeProxy = 3;

        public struct StructInternetProxyInfo
        {
            public int DwAccessType;
            public IntPtr Proxy;
            public IntPtr ProxyBypass;
        };

        public Browser()
        {
            InitializeComponent();
            Icon = Resources.ico;
            RefreshIeSettings("127.0.0.1:" + Common.ProxyPort);
            this.IE.ObjectForScripting = this;
        }

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

        public void RefreshIeSettings(string strProxy)
        {
            StructInternetProxyInfo structIpi;
            structIpi.DwAccessType = InternetOpenTypeProxy;
            structIpi.Proxy = Marshal.StringToHGlobalAnsi(strProxy);
            structIpi.ProxyBypass = Marshal.StringToHGlobalAnsi("local");
            var intptrStruct = Marshal.AllocCoTaskMem(Marshal.SizeOf(structIpi));
            Marshal.StructureToPtr(structIpi, intptrStruct, true);
            var iReturn = InternetSetOption(IntPtr.Zero, InternetOptionProxy, intptrStruct, Marshal.SizeOf(structIpi));
        }

        public void CloseWindow()
        {
            this.Close();
        }
    }
}
