﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DPMHelper {
    public class CfgMgr<T> {
        public static CfgMgr<Storage> Manager { get; set; }

        public String FileName { get; private set; }
        public T Config { get; set; }

        public bool Status {
            get;
            set;
        }

        public CfgMgr(String fileName) {
            FileName = fileName;
        }

        public bool Save() {
            Status = false;
            try {
                if (File.Exists(FileName)) File.Delete(FileName);
                var fs = new FileStream(FileName, FileMode.CreateNew);
                var saver = new BinaryFormatter();
                saver.Serialize(fs, Config);
                fs.Close();
                Status = true;
            }
            catch
            {
                // ignored
            }
            return Status;
        }

        public bool Load() {
            Status = false;
            try {
                if (!File.Exists(FileName)) return false;
                var fs = new FileStream(FileName, FileMode.Open);
                var loader = new BinaryFormatter();
                Config = (T)loader.Deserialize(fs);
                fs.Close();
                Status = true;
            }
            catch
            {
                // ignored
            }
            return Status;
        }
    }


    [Serializable]
    public class Storage {
        private string _pwd;
        public string User { get; set; }
        public string Pwd { get { return StringDecoding(_pwd); } set { _pwd = StringEncoding(value); } }
        public Storage() {
            User = "";
            Pwd = "";
        }
        static string StringEncoding(string pwd) {
            var arrChar = pwd.ToCharArray();
            var strChar = "";
            for (var i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] + 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }

        static string StringDecoding(string pwd) {
            var arrChar = pwd.ToCharArray();
            var strChar = "";
            for (var i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] - 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }
    }
}