﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using mshtml;
using Newtonsoft.Json.Linq;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace DPMHelper {
    internal class Common {
        public const string DpmBaseUrl = "http://sqms-dpm.paic.com.cn";
        public const string HtmlPostLogin = "http://sqms-dpm.paic.com.cn/dpm/j_security_check";
        public const string JsonPostSplitSr = "http://sqms-dpm.paic.com.cn/dpm/submitSr.do";
        public const string JsonPostGetSrInfo = "http://sqms-dpm.paic.com.cn/dpm/getSrByAjax.do";
        public const string JsonPostFinishSr = "http://sqms-dpm.paic.com.cn/dpm/confirmSrFinish.do";

        public const string HtmlGetGetRmInfo = "http://sqms-dpm.paic.com.cn/dpm/showRm.do";
        public const string HtmlGetGetRmPlan = "http://sqms-dpm.paic.com.cn/dpm/showRmTaskPlan.do";
        public const string HtmlGetGetSrCds = "http://sqms-dpm.paic.com.cn/dpm/distribute.do";
        public const string HtmlGetGetSrDev = "http://sqms-dpm.paic.com.cn/dpm/clooseCoder.do";
        public const string HtmlGetRefresh = "http://sqms-dpm.paic.com.cn/dpm/getSrPendingList.do";

        public const string HtmlGetGetNeedInfo = "http://sqms-dpm.paic.com.cn/dpm/showNeed.do";
        public const string HtmlGetGetNeedSr = "http://sqms-dpm.paic.com.cn/dpm/analyzeNeed.do";
        public const string JsonPostSplitDev = "http://sqms-dpm.paic.com.cn/dpm/saveCd.do";
        public const string JsonPostFinishCd = "http://sqms-dpm.paic.com.cn/dpm/finishCd.do";

        public const string HtmlPostSearchRmInfo = "http://sqms-dpm.paic.com.cn/dpm/rmManage.do";
        public const string HtmlGetGetRmContent = "http://sqms-dpm.paic.com.cn/dpm/showRmContent.do";
        public const string HtmlGetGetRmStgEnv = "http://sqms-dpm.paic.com.cn/dpm/showStgEnv.do";

        public const string RefRmManage = "http://sqms-dpm.paic.com.cn/dpm/showRmContent.do?rmId=";
        public const string RefSrSplitDeveloper = "http://sqms-dpm.paic.com.cn/dpm/clooseCoder.do?srId=";

        public static List<Form> FrmPool;
        public static string LastSrc;
        private static string _hcMsg;
        private const string Encode = "UTF-8";
        private static readonly HttpClient Hc = new HttpClient();
        private static readonly Browser Bw = new Browser();
        private static readonly MapDict Cache = new MapDict();

        public delegate void OnLoadEvent();

        private static OnLoadEvent _bwEvt;

        public const int ProxyPort = 6802;
        private static readonly TcpListener TcpListener;
        private static readonly WaitCallback ProxyCallback;

        static Common()
        {
            TcpListener = new TcpListener(IPAddress.Any, ProxyPort);
            TcpListener.Start();
            var daemon = new Thread(Listen) { IsBackground = true };
            daemon.Start();
            Hc.Proxy = new WebProxy("127.0.0.1", ProxyPort);
            var proxy = new ProxyFilter();
            ProxyCallback = new WaitCallback(proxy.Run);
        }

        private static void Listen()
        {
            while (true)
            {
                var socket = TcpListener.AcceptSocket();
                ThreadPool.SetMinThreads(1, 1);
                ThreadPool.SetMaxThreads(5, 5);
                ThreadPool.QueueUserWorkItem(ProxyCallback, socket);
            }
        }

        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool InternetSetCookie(string lpszUrlName, string lbszCookieName, string lpszCookieData);

        private static void OpenIe(string url, OnLoadEvent evt)
        {
            foreach (Cookie cok in Hc.Cookies.GetCookies(new Uri(DpmBaseUrl)))
            {
                InternetSetCookie(DpmBaseUrl, cok.Name, cok.Value + ";expires=Sun,22-Feb-2099 00:00:00 GMT");
            }
            Bw.IE.Navigate(url);
            _bwEvt = evt;
            Bw.IE.DocumentCompleted += BrowserCompolete;
            Bw.ShowDialog();
            Bw.IE.Navigate("about:blank");
        }

        private static void BrowserCompolete(Object sender, EventArgs e)
        {
            Thread.Sleep(500);
            if (_bwEvt != null)
                _bwEvt();
            _bwEvt = null;
            Bw.IE.DocumentCompleted -= BrowserCompolete;
        }

        private static void ExecJs(string code)
        {
            if (Bw.IE.Document == null) return;
            if (Bw.IE.Document.Window == null) return;
            var win = (IHTMLWindow2)Bw.IE.Document.Window.DomWindow;
            win.execScript(code, "Javascript");
        }

        public static void RefManageRmContent(string rmId, bool rdonly)
        {
            if (rdonly)
            {
                OpenIe(RefRmManage + rmId, null);
            }
            else
            {
                OpenIe(RefRmManage + rmId,
                    () =>
                    {
                        ExecJs(
                            "$('#removeSrForm div.qs_gl_bt')[0].innerHTML='<span><a id=\"add_dev\" class=\"add_clsj\" onclick=\"addDev(\\'"+rmId+"\\');\">添加</a></span><span><a class=\"close_glsj\" id=\"remove_dev\" onclick=\"removeSrFromRm();\">撤出</a></span>';");
                    });
            }
        }

        public static void RefSplitDeveloper(string srId)
        {
            OpenIe(RefSrSplitDeveloper + srId,
                () =>
                {
                    ExecJs(
                        "addUsers=function(srId){$.ajax({url:'./saveCd.do',data:{umnos:$('#rightselect').val().toString(),srId:srId},type:'post',cache:false,dataType:'json',success:function(jsonObj){alert('分配成功！');window.external.CloseWindow();}});}");
                });
        }

        public static void Refresh()
        {
            GetSrc(HtmlGetRefresh);
        }

        public static bool Login(string usr, string pwd)
        {
            var param = new MapDict { { "j_username", usr }, { "j_password", pwd } };
            PostSrc(HtmlPostLogin, BuildParam(param));
            return (FindText(LastSrc, "欢迎您, ") != null);
        }

        public static JObject GetSrInfo(string srid)
        {
            var srInfo = PostSrc(JsonPostGetSrInfo, "srId=" + srid);
            return srInfo == null ? null : JObject.Parse(srInfo);
        }

        public static String SearchRmId(string versionId)
        {
            if (Cache.ContainsKey(versionId))
            {
                return Cache[versionId];
            }
            var param = new MapDict {
                {"perPageNum", "10"},
                {"edTime", DateTime.Now.ToString("yyyy-MM-dd")},
                {"searchOperFlag", "listSearch"},
                {"stTime", DateTime.Now.AddMonths(2).ToString("yyyy-MM-dd")},
                {"rmNo", versionId},
                {"rmStateCode", "all"}
            };
            var res = PostSrc(HtmlPostSearchRmInfo, BuildParam(param));
            var hd = new HtmlDocument();
            hd.LoadHtml(res);
            var node = hd.DocumentNode.SelectSingleNode("//table[@id='rmListTable']/tr[2]");
            if (node == null) return null;
            var rmId = node.GetAttributeValue("param1", "#");
            rmId = rmId != "#" ? rmId : null;
            Cache[versionId] = rmId;
            return rmId;
        }

        public static List<MapDict> GetRmContent(string rmid)
        {
            var res = GetSrc(HtmlGetGetRmContent + "?rmId=" + rmid);
            if (res.Contains("查看 <a href=\"javascript:showhide('detail')\">详细错误信息"))
            {
                return null;
            }
            var hd = new HtmlDocument();
            hd.LoadHtml(res);
            var tbl = hd.DocumentNode.SelectSingleNode("//input[@id='changereason']/following::table");
            var trs = tbl.SelectNodes("tr[position()>1]");
            var content = new List<MapDict>();
            if (trs == null) return content;
            foreach (var nd in trs)
            {
                var mp = new MapDict("srId", "srId") {
                    {"srId", nd.SelectSingleNode("td[2]").InnerText.Trim()},
                    {"title", nd.SelectSingleNode("td[3]").InnerHtml.Trim()},
                    {"source", nd.SelectSingleNode("td[5]").InnerHtml.Trim()},
                    {"status", nd.SelectSingleNode("td[4]").InnerHtml.Trim()},
                    {"needId", nd.SelectSingleNode("td[6]").InnerText.Trim()},
                    {"subSys", nd.SelectSingleNode("td[7]").InnerHtml.Trim()},
                    {"commiter", nd.SelectSingleNode("td[8]").InnerHtml.Trim()},
                    {"devs", nd.SelectSingleNode("td[10]").InnerHtml.Trim()}
                };
                content.Add(mp);
            }
            return content;
        }

        public static MapDict GetRmInfo(string rmId)
        {
            string res = GetSrc(HtmlGetGetRmInfo + "?rmId=" + rmId);
            if (res.Contains("查看 <a href=\"javascript:showhide('detail')\">详细错误信息"))
            {
                return null;
            }

            var hd = new HtmlDocument();
            hd.LoadHtml(res);
            var tbl = hd.DocumentNode.SelectSingleNode("//*[@id='basicDivId']/div[1]/table[1]");
            var oe = One(res);
            var rmInfo = new MapDict {
                {"rmCreateDate", tbl.SelectSingleNode("//input[@id='dateBuild']").GetAttributeValue("value", "")},
                {"rmCreator", tbl.SelectSingleNode("//input[@id='buildPerson']").GetAttributeValue("value", "")},
                {"rmStatus", FindText(oe, "版本状态.*?value=\"(.+?)\"", 1)},
                {"rmVersion", FindText(oe, "版本号.*?value=\"(.+?)\"", 1)},
                {"svnPath", FindText(oe, "SVN配置库的分支.*?value=\"(.+?)\"", 1)},
                {"svnBase", FindText(oe, "SVN起点全局版本号.*?value=\"(.+?)\"", 1)}
            };
            Cache[rmId] = "";
            Cache[rmId + "_base"] = rmInfo["svnBase"];
            Cache[rmId + "_path"] = rmInfo["svnPath"];
            rmInfo.Add("rmType", (rmInfo["rmVersion"].EndsWith(".0") ? "" : "非") + "常规版本");

            res = GetSrc(HtmlGetGetRmPlan + "?rmId=" + rmId);
            hd = new HtmlDocument();
            hd.LoadHtml(res);

            tbl = hd.DocumentNode.SelectSingleNode("//table[@id='verplan_newst']");
            if (tbl.InnerText.Contains("版本冻结时间"))
            {
                rmInfo.Add("rmLockStgDate",
                    tbl.SelectSingleNode("tr[1]/td[2]/input[1]").GetAttributeValue("value", "") + " 00:00:00");
                rmInfo.Add("rmStgDate",
                    tbl.SelectSingleNode("tr[2]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
                rmInfo.Add("rmUatDate",
                    tbl.SelectSingleNode("tr[3]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
                rmInfo.Add("rmPrdDate",
                    tbl.SelectSingleNode("tr[4]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
            }
            else
            {
                rmInfo.Add("rmLockStgDate", "");
                rmInfo.Add("rmStgDate",
                    tbl.SelectSingleNode("tr[1]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
                rmInfo.Add("rmUatDate",
                    tbl.SelectSingleNode("tr[2]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
                rmInfo.Add("rmPrdDate",
                    tbl.SelectSingleNode("tr[3]/td[2]/input[1]").GetAttributeValue("value", "") + ":00");
            }
            // TODO
            rmInfo["rmTestEnv"] = "登录页面看吧";

            return rmInfo;
        }

        public static MapDict GetNeedInfo(string ndid)
        {
            var htmlOrg = GetSrc(HtmlGetGetNeedInfo + "?needId=" + ndid);
            var html = One(htmlOrg);
            var map = new MapDict {
                {"needStatus", FindText(html, "状态.*?value=\"(.+?)\"", 1)},
                {"needName", FindText(html, "需求名称.*?value=\"(.+?)\"", 1)},
                {"subSysName", FindText(html, "子系统.*?value=\"(.+?)\"", 1)},
                {"subSys", FindText(html, "id=\"subSys\".*?value=\"(.+?)\"", 1)},
                {"needBenef", FindText(html, ">受益人.*?value=\"([\\d]+?)\"selected>", 1)},
                {"needType", FindText(html, ">业务需求/IT优化需求.*?value=\"([\\w_]+?)\"selected>", 1)},
                {"expectDate", FindText(html, ">期望完成时间.*?value='(.+?)'", 1)},
                {"needCommiter", FindText(html, ">提交人.*?value=\"(.+?)\"", 1)},
                {"commitDate", FindText(html, ">提交时间.*?value=\"(.+?)\"", 1)},
                {"appDate", FindText(html, ">审批完成时间.*?value=\"(.+?)\"", 1)},
                {"needDesc", FindText(htmlOrg.Replace("\n", "$#$"), ">需求描述.*?title=\"(.+?)\"", 1).Replace("$#$", "\n")}
            };
            map.Add("needBenefName", FindText(html, ">受益人.*?" + map["needBenef"] + ".*?selected>(.+?)</option>", 1));
            return map;
        }

        public static List<MapDict> GetNeedSr(string ndid)
        {
            var result = new List<MapDict>();
            var html = One(GetSrc(HtmlGetGetNeedSr + "?needId=" + ndid));
            html = FindText(html, "开发任务名称.*?</table>");
            var trs = html.Replace("</tr>", "\n").Split('\n');
            for (var i = 1; i < trs.Length - 1; i++)
            {
                var mc = Regex.Matches(trs[i], ">([^<>]*?)<", RegexOptions.IgnoreCase);
                var map = new MapDict("srId") {
                    {"srId", mc[4].Groups[1].Value.Trim()},
                    {"srDesc", mc[7].Groups[1].Value.Trim()},
                    {"srState", mc[9].Groups[1].Value.Trim()},
                    {"srRm", mc[17].Groups[1].Value.Trim()}
                };
                result.Add(map);
            }
            return result;
        }

        public static List<MapDict> GetSrCd(string srid)
        {
            var result = new List<MapDict>();
            var html = One(GetSrc(HtmlGetGetSrCds + "?srId=" + srid));
            var tr = FindText(html, "<tr.*(?=</table>)");
            var trs = tr.Replace("</tr>", "\n").Split('\n');
            for (var i = 1; i < trs.Length - 1; i++)
            {
                var tds = trs[i].Replace("</td>", "\n").Split('\n');
                var map = new MapDict("id") {
                    {"srId", srid},
                    {"id", FindText(tds[1], "(?<=>).*$").Trim()},
                    {"um", FindText(tds[2], "(?<=>).*$").Trim()},
                    {"name", FindText(tds[3], "(?<=>).*$").Trim()},
                    {"state", FindText(tds[4], "(?<=>).*$").Trim()}
                };
                result.Add(map);
            }
            return result;
        }

        public static List<MapDict> GetSvnChanges(string rmId, string srId, string userId)
        {
            var list = new List<MapDict>();
            if (!Cache.ContainsKey(rmId))
            {
                var rmInfo = GetRmInfo(rmId);
                if (rmInfo == null)
                {
                    return list;
                }
            }

            var outStr = RunSvnLog(Cache[rmId + "_base"], Cache[rmId + "_path"], srId.Substring(2));
            Dbg(outStr);
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outStr);
            var root = xmlDoc.DocumentElement;
            if (root == null) return list;
            var specChanges = root.SelectNodes("/log/logentry");
            if (specChanges == null) return list;
            foreach (XmlNode logEnc in specChanges)
            {
                if (logEnc["author"].InnerText.ToUpper() != userId.ToUpper()) continue;
                if (!logEnc["msg"].InnerText.ToUpper().StartsWith(srId)) continue;
                var cg = new MapDict {
                    {"revision", logEnc.Attributes["revision"].InnerText},
                    {"author", logEnc["author"].InnerText},
                    {"comment", logEnc["msg"].InnerText},
                    {"date", logEnc["date"].InnerText.Substring(0, 19).Replace("T", " ")},
                    {"basePath", Cache[rmId + "_path"]}
                };
                var files = logEnc.SelectNodes("paths/path[@kind='file']");
                foreach (XmlNode fl in files)
                {
                    var fg = new MapDict {
                        {"srId", srId},
                        {"revision", cg["revision"]},
                        {"author", cg["author"]},
                        {"comment", cg["comment"]},
                        {"date", cg["date"]},
                        {"path", GetRelativePath(fl.InnerText)},
                        {"action", fl.Attributes["action"].InnerText},
                        {"basePath", cg["basePath"]}
                    };
                    list.Add(fg);
                }
            }
            return list;
        }

        private static string GetRelativePath(string path)
        {
            var phs = path.Split('/');
            List<string> phl = new List<string>(phs);
            phl.RemoveRange(1, path.StartsWith("/branches") ? 2 : 1);
            return string.Join("/", phl.ToArray());
        }

        private static string RunSvnLog(string svnBase, string svnPath, string searchId)
        {
            var zp = new Process
            {
                StartInfo =
                {
                    FileName = "svn",
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    Arguments = "log -v -r " + svnBase + ":head " + svnPath + " --xml --search " + searchId
                }
            };
            zp.Start();
            var output = new StreamReader(zp.StandardOutput.BaseStream, Encoding.UTF8).ReadToEnd();
            if (output.Length < 10)
            {
                throw new Exception(zp.StandardError.ReadToEnd());
            }
            return output;
        }

        public static JObject FinishCd(string srid, string cdid)
        {
            var json = PostSrc(JsonPostFinishCd, "srId=" + srid + "&cdId=" + cdid);
            return json == null ? null : JObject.Parse(json);
        }

        public static JObject FinishSr(string srid, string rmid)
        {
            var param = new MapDict {
                {"completeDesc", "已完成"},
                {"isFinalDealPro", "Y"},
                {"isShowFinalDealProFlag", "Y"},
                {"rmId", rmid},
                {"srId", srid},
                {"riskScore", ""},
                {"riskAnalyse", ""}
            };
            var json = PostSrc(JsonPostFinishSr, BuildParam(param));
            return json == null ? null : JObject.Parse(json);
        }

        #region ASSIST

        public static string Join(List<string> array, string splitStr)
        {
            var sb = new StringBuilder();
            foreach (var info in array)
            {
                sb.AppendFormat("{0}{1}", info, splitStr);
            }
            return sb.ToString().Substring(0, sb.Length - splitStr.Length);
        }

        public static string GetSrc(string url)
        {
            var src = Hc.GetSrc(url, Encode, out _hcMsg);
            LastSrc = src;
            if (_hcMsg != string.Empty)
            {
                Console.WriteLine(_hcMsg);
                return null;
            }
            Dbg(src);
            return src;
        }

        public static string PostSrc(string url, string data)
        {
            var src = Hc.PostData(url, data, Encode, Encode, out _hcMsg);
            LastSrc = src;
            if (_hcMsg != string.Empty)
            {
                Console.WriteLine(_hcMsg);
                return null;
            }
            Dbg(src);
            return src;
        }

        public static string BuildParam(MapDict prm)
        {
            var param = "";
            foreach (var i in prm)
            {
                param = param + "&" + i.Key + "=" + i.Value;
            }
            return param.Substring(1);
        }

        public static string One(string html)
        {
            return html.Replace("\t", "").Replace("\r", "").Replace("\n", "");
        }

        public static void Dbg(string txt)
        {
#if DEBUG
            File.WriteAllText("xdebug.log", txt);
#endif
        }

        public static string FindText(string src, string reg)
        {
            if (string.IsNullOrEmpty(src)) return null;
            var mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            return mc.Count < 1 ? null : mc[0].Value.Replace("&amp;", "&");
        }

        public static string FindText(string src, string reg, int group)
        {
            if (string.IsNullOrEmpty(src)) return null;
            var mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            return mc.Count < 1 ? null : mc[0].Groups[@group].Value.Replace("&amp;", "&");
        }

        #endregion
    }

    public class SubsysBean {
        public string Id { get; set; }
        public string Name { get; set; }

        public SubsysBean(string name, string id)
        {
            Id = id;
            Name = name;
        }
    }

    public class MapDict : Dictionary<string, string>, IComparable {
        public string DispKey { get; set; }
        public string SortKey { get; set; }

        public string Name
        {
            get { return ToString(); }
        }

        public MapDict Value
        {
            get { return this; }
        }

        public MapDict()
        {
            DispKey = null;
            SortKey = null;
        }

        public MapDict(string disp)
        {
            DispKey = disp;
            SortKey = disp;
        }

        public MapDict(string disp, string sort)
        {
            DispKey = disp;
            SortKey = sort;
        }

        int IComparable.CompareTo(object obj)
        {
            return String.Compare(this[SortKey], ((MapDict)obj)[SortKey], StringComparison.Ordinal);
        }

        public override string ToString()
        {
            return this[DispKey];
        }
    }
}