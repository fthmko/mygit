﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper {
    public partial class Home : Form {
        public Home() {
            InitializeComponent();
            Icon = Resources.ico;
            CheckForIllegalCrossThreadCalls = false;
            Common.FrmPool = new List<Form>();
        }

        private void Home_Load(object sender, EventArgs e) {
            Text += "1.5";
            if (new Main().ShowDialog() != DialogResult.OK) {
                Close();
            }
            //this.Text += " - " + Common.FindText(Common.lastSrc, "欢迎您, ([\u4E00-\u9FA5]+)", 1);
            tmrTrim.Enabled = true;
        }

        private void btnSch_Click(object sender, EventArgs e) {
            var id = txtId.Text.Trim().ToUpper();
            if (id.Length < 5) {}
            else if (id.StartsWith("SR_")) {
                ViewSr(id);
            }
            else if (id.StartsWith("NEED_")) {
                ViewNeed(id);
            }
            else if (id.StartsWith("RM_")) {
                ViewRm(id);
            }
            else {
                var rmId = Common.SearchRmId(id);
                if (rmId != null) {
                    ViewRm(rmId);
                }
                else {
                    Msg("不知道你填的是什么！");
                }
            }
        }

        private static void ViewSr(String id) {
            var frm = new ViewSr();
            frm.ShowView(id);
            Common.FrmPool.Add(frm);
        }

        private static void ViewRm(String id) {
            var frm = new ViewRm();
            frm.ShowView(id);
            Common.FrmPool.Add(frm);
        }

        private static void ViewNeed(String id) {
            var frm = new ViewNeed();
            frm.ShowView(id);
            Common.FrmPool.Add(frm);
        }

        private void Msg(string txt) {
            MessageBox.Show(this, txt);
        }

        private void tmrTrim_Tick(object sender, EventArgs e) {
            Common.Refresh();
            lock (Common.FrmPool) {
                for (var i = Common.FrmPool.Count - 1; i >= 0; i--) {
                    if (Common.FrmPool[i].Visible) continue;
                    Common.FrmPool[i].Close();
                    Common.FrmPool[i].Dispose();
                    Common.FrmPool.RemoveAt(i);
                }
            }
            var a = Process.GetCurrentProcess();
            a.MaxWorkingSet = Process.GetCurrentProcess().MaxWorkingSet;
            a.Dispose();
        }

        private void txtId_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                btnSch_Click(null, null);
            }
        }
    }
}