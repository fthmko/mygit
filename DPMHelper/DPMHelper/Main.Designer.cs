﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DPMHelper {
    partial class Main {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtUsr = new TextBox();
            this.txtPwd = new TextBox();
            this.btnLogin = new Button();
            this.label12 = new Label();
            this.label1 = new Label();
            this.SuspendLayout();
            // 
            // txtUsr
            // 
            this.txtUsr.Location = new Point(53, 10);
            this.txtUsr.Name = "txtUsr";
            this.txtUsr.Size = new Size(163, 21);
            this.txtUsr.TabIndex = 0;
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new Point(53, 43);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '#';
            this.txtPwd.Size = new Size(163, 21);
            this.txtPwd.TabIndex = 1;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new Point(141, 74);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new Size(75, 23);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "登 录";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new EventHandler(this.btnLogin_Click);
            // 
            // label12
            // 
            this.label12.Location = new Point(12, 40);
            this.label12.Name = "label12";
            this.label12.Size = new Size(35, 26);
            this.label12.TabIndex = 23;
            this.label12.Text = "密码";
            this.label12.TextAlign = ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new Size(35, 26);
            this.label1.TabIndex = 23;
            this.label1.Text = "用户";
            this.label1.TextAlign = ContentAlignment.MiddleRight;
            // 
            // Main
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new SizeF(6F, 12F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(232, 106);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.txtUsr);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "UM登录";
            this.Load += new EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox txtUsr;
        private TextBox txtPwd;
        private Button btnLogin;
        private Label label12;
        private Label label1;
    }
}

