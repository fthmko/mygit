﻿using System;
using System.Deployment.Application;
using System.Drawing;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper {
    public partial class Main : Form {
        Storage _config;

        public Main() {
            InitializeComponent();
            Init();
        }

        private void Init() {
            Icon = Resources.ico;
            CheckForIllegalCrossThreadCalls = false;
        }

        private void UpdateApplication() {
            if (ApplicationDeployment.IsNetworkDeployed != true) return;
            var ad = ApplicationDeployment.CurrentDeployment;
            var msg = "当前版本:" + ad.CurrentVersion;
            var checkInfo = ad.CheckForDetailedUpdate();
            if (checkInfo.UpdateAvailable != true) return;
            msg += "\n最新版本:" + checkInfo.AvailableVersion;
            MessageBox.Show(this, msg, @"版本更新");
            ad.Update();
            MessageBox.Show(@"更新完毕，程序将重新启动");
            Application.Restart();
        }

        private void Main_Load(object sender, EventArgs e) {
            UpdateApplication();
            CfgMgr<Storage>.Manager = new CfgMgr<Storage>("save.dat");
            if (CfgMgr<Storage>.Manager.Load()) {
                txtUsr.Text = CfgMgr<Storage>.Manager.Config.User;
                txtPwd.Text = CfgMgr<Storage>.Manager.Config.Pwd;
                btnLogin.Focus();
            } else {
                CfgMgr<Storage>.Manager.Config = new Storage();
            }
            _config = CfgMgr<Storage>.Manager.Config;
        }

        private void btnLogin_Click(object sender, EventArgs e) {
            btnLogin.Enabled = false;
            if (Common.Login(txtUsr.Text, txtPwd.Text)) {
                _config.User = txtUsr.Text;
                _config.Pwd = txtPwd.Text;
                CfgMgr<Storage>.Manager.Save();
            } else {
                Msg("登录失败！");
                btnLogin.Enabled = true;
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        void Msg(string txt) {
            MessageBox.Show(this, txt);
        }

        protected override void OnPaint(PaintEventArgs e) {
            //e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            e.Graphics.DrawString("m", new Font("Arial Black", 20), Brushes.DarkOrange, 19, 64);
            e.Graphics.DrawString("u", new Font("Arial Black", 20), Brushes.ForestGreen, 10, 64);
        }
    }
}
