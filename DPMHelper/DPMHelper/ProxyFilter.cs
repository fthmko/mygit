﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace DPMHelper
{
    class ProxyFilter
    {
        public void Run(object socket)
        {
            _Run((Socket)socket);
        }

        private void _Run(Socket socket)
        {
            var requestDataList = new List<byte>();
            var hostUrl = " ";
            var hostPort = 80;

            var requestSize = ReadMessage(socket, requestDataList);
            if (requestSize == 0)
            {
                return;
            }

            HackRequest(requestDataList);
            var requestData = requestDataList.ToArray();

            var streamReader = new StreamReader(new MemoryStream(requestData));
            var strLine = streamReader.ReadLine();
            if (strLine == null || !strLine.Contains("HTTP/"))
            {
                throw new IOException("not http request!");
            }
            while ((strLine = streamReader.ReadLine()) != "")
            {
                if (!strLine.StartsWith("Host:")) continue;
                hostUrl = strLine.Substring(5).Trim();
                break;
            }
            if (hostUrl == "")
            {
                throw new IOException("no request host!");
            }
            if (hostUrl.Contains(":"))
            {
                hostPort = Convert.ToInt32(hostUrl.Substring(hostUrl.IndexOf(":") + 1));
                hostUrl = hostUrl.Substring(0, hostUrl.IndexOf(":"));
            }

            var hostEntry = Dns.GetHostEntry(hostUrl);
            var hostAddress = hostEntry.AddressList;
            var ipEndpoint = new IPEndPoint(hostAddress[0], hostPort);
            var remoteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            remoteSocket.Connect(ipEndpoint);
            remoteSocket.Send(requestData, requestData.Length, 0);
            var responseDataList = new List<byte>();
            var responseSize = ReadMessage(remoteSocket, responseDataList);
            remoteSocket.Shutdown(SocketShutdown.Both);
            remoteSocket.Close();
            HackResponse(responseDataList);
            SendMessage(socket, responseDataList);
            socket.Close();
        }

        private int ReadMessage(Socket sock, List<byte> dataList)
        {
            var byteBuffer = new byte[4096];
            var bytesCnt = sock.Receive(byteBuffer, byteBuffer.Length, 0);
            for (var i = 0; i < bytesCnt; i++)
            {
                dataList.Add(byteBuffer[i]);
            }

            var byteData = dataList.ToArray();
            var streamReader = new StreamReader(new MemoryStream(byteData));
            string ln;
            var contentLength = 0;
            while ((ln = streamReader.ReadLine()) != "")
            {
                if (ln.StartsWith("Content-Length:"))
                {
                    contentLength = Convert.ToInt32(ln.Substring(ln.IndexOf(":") + 1));
                    break;
                }
                if (ln.StartsWith("Transfer-Encoding: chunked"))
                {
                    contentLength = -1;
                    break;
                }
            }
            var lengthOffset = SearchByteArr(byteData, new byte[] { 13, 10, 13, 10 }, 0) + 4;
            while ((contentLength == -1 && !DetectChunkEnd(dataList)) || (contentLength > -1 && dataList.Count - lengthOffset < contentLength))
            {
                bytesCnt = sock.Receive(byteBuffer, byteBuffer.Length, 0);
                for (var i = 0; i < bytesCnt; i++)
                {
                    dataList.Add(byteBuffer[i]);
                }
            }

            return dataList.Count;
        }

        private static bool DetectChunkEnd(List<byte> dataList)
        {
            return Encoding.ASCII.GetString(dataList.GetRange(dataList.Count - 8, 8).ToArray()) == "0000\r\n\r\n";
        }

        private void SendMessage(Socket sock, List<byte> dataList)
        {
            var dataBytes = dataList.ToArray();
            sock.Send(dataBytes, dataBytes.Length, 0);
        }

        private void HackRequest(List<byte> dataList)
        {

        }

        private void HackResponse(List<byte> dataList)
        {
            var dataBytes = dataList.ToArray();
            var streamReader = new StreamReader(new MemoryStream(dataBytes));
            string strLine;
            var headerLine = new List<string>();
            var contentLength = 0;
            var contentType = "";
            byte[] fullBody = null;
            var hacked = false;
            while ((strLine = streamReader.ReadLine()) != "")
            {
                headerLine.Add(strLine);
                if (strLine.StartsWith("Content-Length:"))
                {
                    contentLength = Convert.ToInt32(strLine.Substring(strLine.IndexOf(":") + 1));
                }
                if (strLine.StartsWith("Transfer-Encoding: chunked"))
                {
                    contentLength = -1;
                }
                if (strLine.StartsWith("Content-Type:"))
                {
                    contentType = strLine.Substring(strLine.IndexOf(":") + 1);
                }
            }

            if (headerLine[0].EndsWith("200 OK"))
            {
                if (contentType.Contains("text/html"))
                {
                    fullBody = GetFullBody(dataList, contentLength);
                    var bodyStr = "";
                    var enc = Encoding.Default;
                    if (contentType.Contains("UTF"))
                    {
                        enc = Encoding.UTF8;
                    }
                    bodyStr = enc.GetString(fullBody);
                    if (!bodyStr.Contains("X-UA-Compatible"))
                    {
                        bodyStr = bodyStr.Insert(bodyStr.IndexOf("<head>") + 6, "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />");
                        fullBody = enc.GetBytes(bodyStr);
                        hacked = true;
                    }
                }
            }
            else if (headerLine[0].EndsWith("302 Moved Temporarily"))
            {
                // nothing
            }

            if (!hacked) return;

            dataList.Clear();
            var lenAdded = false;
            foreach (var s in headerLine)
            {
                if (!lenAdded && (s.StartsWith("Content-Length:") || s.StartsWith("Transfer-Encoding: chunked")))
                {
                    lenAdded = true;
                    dataList.AddRange(Encoding.ASCII.GetBytes("Content-Length: " + fullBody.Length));
                }
                else
                {
                    dataList.AddRange(Encoding.ASCII.GetBytes(s));
                }
                dataList.AddRange(new byte[] { 13, 10 });
            }
            dataList.AddRange(new byte[] { 13, 10 });
            dataList.AddRange(fullBody);
        }

        private byte[] GetFullBody(List<byte> data, int contentLength)
        {
            var dataBytes = data.ToArray();
            var offsetBody = SearchByteArr(dataBytes, new byte[] { 13, 10, 13, 10 }, 0) + 4;
            if (contentLength == 0 || offsetBody <= 0) { return null; }
            if (contentLength > 0)
            {
                return data.GetRange(offsetBody, contentLength).ToArray();
            }
            var body = new List<byte>();
            var offsetLastPartEnd = offsetBody;
            while (true)
            {
                var offsetLenEnd = SearchByteArr(dataBytes, new byte[] { 13, 10 }, offsetLastPartEnd);
                var partLenHex = Encoding.ASCII.GetString(data.GetRange(offsetLastPartEnd, offsetLenEnd - offsetLastPartEnd).ToArray());
                if (partLenHex != "0000")
                {
                    var partLen = Convert.ToInt32(partLenHex, 16);
                    var offsetPartBegin = offsetLenEnd + 2;
                    var offsetPartEnd = offsetPartBegin + partLen;
                    body.AddRange(data.GetRange(offsetPartBegin, offsetPartEnd - offsetPartBegin));

                    offsetLastPartEnd = offsetPartEnd + 2;
                }
                else
                {
                    break;
                }
            }
            return body.ToArray();
        }

        private int SearchByteArr(IList<byte> content, IList<byte> find, int start)
        {
            var end = find.Count - 1;
            var skip = 0;
            for (var index = start; index <= content.Count - find.Count; ++index)
            {
                var matched = true;
                if (find[0] != content[index] || find[end] != content[index + end]) continue;
                skip++;
                if (end > 10)
                    if (find[skip] != content[index + skip] || find[end - skip] != content[index + end - skip])
                        continue;
                    else skip++;
                for (var subIndex = skip; subIndex < find.Count - skip; ++subIndex)
                {
                    if (find[subIndex] == content[index + subIndex]) continue;
                    matched = false;
                    break;
                }
                if (matched)
                {
                    return index;
                }
            }
            return -1;
        }
    }
}
