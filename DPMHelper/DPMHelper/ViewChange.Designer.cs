﻿namespace DPMHelper {
    partial class ViewChange {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgChange = new System.Windows.Forms.DataGridView();
            this.cmsChg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuHis = new System.Windows.Forms.ToolStripMenuItem();
            this.colSR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFullPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBasePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgChange)).BeginInit();
            this.cmsChg.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgChange
            // 
            this.dgChange.AllowUserToAddRows = false;
            this.dgChange.AllowUserToDeleteRows = false;
            this.dgChange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgChange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSR,
            this.colCd,
            this.colVer,
            this.colAct,
            this.colFullPath,
            this.colBasePath});
            this.dgChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgChange.Location = new System.Drawing.Point(0, 0);
            this.dgChange.Name = "dgChange";
            this.dgChange.ReadOnly = true;
            this.dgChange.RowTemplate.Height = 23;
            this.dgChange.Size = new System.Drawing.Size(574, 353);
            this.dgChange.TabIndex = 0;
            this.dgChange.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgChange_CellMouseUp);
            // 
            // cmsChg
            // 
            this.cmsChg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHis});
            this.cmsChg.Name = "cmsChg";
            this.cmsChg.ShowImageMargin = false;
            this.cmsChg.ShowItemToolTips = false;
            this.cmsChg.Size = new System.Drawing.Size(139, 48);
            // 
            // mnuHis
            // 
            this.mnuHis.Name = "mnuHis";
            this.mnuHis.Size = new System.Drawing.Size(149, 22);
            this.mnuHis.Text = "查看文件历史(&H)";
            this.mnuHis.Click += new System.EventHandler(this.mnuHis_Click);
            // 
            // colSR
            // 
            this.colSR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.colSR.HeaderText = "SR";
            this.colSR.MinimumWidth = 100;
            this.colSR.Name = "colSR";
            this.colSR.ReadOnly = true;
            // 
            // colCd
            // 
            this.colCd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.colCd.HeaderText = "UM";
            this.colCd.MinimumWidth = 100;
            this.colCd.Name = "colCd";
            this.colCd.ReadOnly = true;
            // 
            // colVer
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colVer.DefaultCellStyle = dataGridViewCellStyle5;
            this.colVer.HeaderText = "版本";
            this.colVer.MinimumWidth = 80;
            this.colVer.Name = "colVer";
            this.colVer.ReadOnly = true;
            this.colVer.Width = 80;
            // 
            // colAct
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colAct.DefaultCellStyle = dataGridViewCellStyle6;
            this.colAct.HeaderText = "操作";
            this.colAct.MinimumWidth = 70;
            this.colAct.Name = "colAct";
            this.colAct.ReadOnly = true;
            this.colAct.Width = 70;
            // 
            // colFullPath
            // 
            this.colFullPath.HeaderText = "相对路径";
            this.colFullPath.MinimumWidth = 800;
            this.colFullPath.Name = "colFullPath";
            this.colFullPath.ReadOnly = true;
            this.colFullPath.Width = 800;
            // 
            // colBasePath
            // 
            this.colBasePath.HeaderText = "basePath";
            this.colBasePath.Name = "colBasePath";
            this.colBasePath.ReadOnly = true;
            this.colBasePath.Visible = false;
            // 
            // ViewChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 353);
            this.Controls.Add(this.dgChange);
            this.Name = "ViewChange";
            this.Text = "ViewChange";
            this.Shown += new System.EventHandler(this.ViewChange_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgChange)).EndInit();
            this.cmsChg.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgChange;
        private System.Windows.Forms.ContextMenuStrip cmsChg;
        private System.Windows.Forms.ToolStripMenuItem mnuHis;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSR;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAct;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFullPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBasePath;
    }
}