﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper {
    public partial class ViewChange : Form {
        public List<MapDict> List { get; set; }

        public ViewChange()
        {
            InitializeComponent();
            Icon = Resources.ico;
        }

        public void SetList(List<MapDict> list)
        {
            dgChange.Rows.Clear();
            foreach (MapDict map in list)
            {
                dgChange.Rows.Add(map["srId"], map["author"], map["revision"], map["action"], map["path"], map["basePath"]);
            }
        }

        private void dgChange_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex <= -1 || e.Button != MouseButtons.Right) return;
            dgChange.ClearSelection();
            dgChange.Rows[e.RowIndex].Selected = true;
            cmsChg.Tag = e.RowIndex;
            cmsChg.Show(MousePosition.X, MousePosition.Y);
        }

        private static void Shell(string process, string arg, bool wait)
        {
            var psi = new ProcessStartInfo(process) { Arguments = arg };
            var prc = Process.Start(psi);
            if (wait)
            {
                prc.WaitForExit();
            }
        }

        private void Shell(string process, params string[] args)
        {
            Shell(process, string.Join(" ", args), false);
        }

        private void mnuHis_Click(object sender, EventArgs e)
        {
            var index = (int)cmsChg.Tag;
            Shell("TortoiseProc.exe", "/command:log", "/strict", "/path:" + dgChange[5, index].Value + dgChange[4, index].Value);
        }

        private void ViewChange_Shown(object sender, EventArgs e)
        {
            SetList(List);
        }

        public void SetTitle(string title) {
            Text = title;
        }
    }
}
