﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DPMHelper {
    partial class ViewNeed {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCmtTime = new System.Windows.Forms.TextBox();
            this.txtWishTime = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPasTime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbBenef = new System.Windows.Forms.ComboBox();
            this.cmbSubsys = new System.Windows.Forms.ComboBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCommiter = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lstAtt = new System.Windows.Forms.ListBox();
            this.dgSR = new System.Windows.Forms.DataGridView();
            this.colSr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSR)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 327);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtCmtTime);
            this.tabPage1.Controls.Add(this.txtWishTime);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtPasTime);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.txtDesc);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.cmbBenef);
            this.tabPage1.Controls.Add(this.cmbSubsys);
            this.tabPage1.Controls.Add(this.txtState);
            this.tabPage1.Controls.Add(this.txtId);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtCommiter);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(407, 301);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(200, 272);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 26);
            this.label11.TabIndex = 39;
            this.label11.Text = "审批完成";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCmtTime
            // 
            this.txtCmtTime.Location = new System.Drawing.Point(74, 275);
            this.txtCmtTime.Name = "txtCmtTime";
            this.txtCmtTime.ReadOnly = true;
            this.txtCmtTime.Size = new System.Drawing.Size(121, 21);
            this.txtCmtTime.TabIndex = 37;
            this.txtCmtTime.Text = "9999-99-99 99:99:99";
            this.txtCmtTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtWishTime
            // 
            this.txtWishTime.Location = new System.Drawing.Point(265, 248);
            this.txtWishTime.Name = "txtWishTime";
            this.txtWishTime.ReadOnly = true;
            this.txtWishTime.Size = new System.Drawing.Size(121, 21);
            this.txtWishTime.TabIndex = 37;
            this.txtWishTime.Text = "9999-99-99 99:99:99";
            this.txtWishTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 26);
            this.label9.TabIndex = 38;
            this.label9.Text = "提交时间";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPasTime
            // 
            this.txtPasTime.Location = new System.Drawing.Point(265, 275);
            this.txtPasTime.Name = "txtPasTime";
            this.txtPasTime.ReadOnly = true;
            this.txtPasTime.Size = new System.Drawing.Size(121, 21);
            this.txtPasTime.TabIndex = 36;
            this.txtPasTime.Text = "9999-99-99 99:99:99";
            this.txtPasTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(201, 245);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 26);
            this.label14.TabIndex = 38;
            this.label14.Text = "期望完成";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(8, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 26);
            this.label10.TabIndex = 35;
            this.label10.Text = "需求描述";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(74, 147);
            this.txtDesc.Multiline = true;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDesc.Size = new System.Drawing.Size(316, 95);
            this.txtDesc.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 26);
            this.label12.TabIndex = 30;
            this.label12.Text = "编号";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbBenef
            // 
            this.cmbBenef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBenef.Enabled = false;
            this.cmbBenef.FormattingEnabled = true;
            this.cmbBenef.Location = new System.Drawing.Point(74, 93);
            this.cmbBenef.Name = "cmbBenef";
            this.cmbBenef.Size = new System.Drawing.Size(182, 20);
            this.cmbBenef.TabIndex = 28;
            // 
            // cmbSubsys
            // 
            this.cmbSubsys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubsys.Enabled = false;
            this.cmbSubsys.FormattingEnabled = true;
            this.cmbSubsys.Location = new System.Drawing.Point(74, 67);
            this.cmbSubsys.Name = "cmbSubsys";
            this.cmbSubsys.Size = new System.Drawing.Size(312, 20);
            this.cmbSubsys.TabIndex = 28;
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(285, 15);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(105, 21);
            this.txtState.TabIndex = 25;
            this.txtState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(74, 15);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(121, 21);
            this.txtId.TabIndex = 24;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 245);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 26);
            this.label8.TabIndex = 27;
            this.label8.Text = "提交人";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 26);
            this.label5.TabIndex = 27;
            this.label5.Text = "受益人";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 26);
            this.label2.TabIndex = 27;
            this.label2.Text = "子系统";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCommiter
            // 
            this.txtCommiter.Location = new System.Drawing.Point(74, 248);
            this.txtCommiter.MaxLength = 200;
            this.txtCommiter.Name = "txtCommiter";
            this.txtCommiter.ReadOnly = true;
            this.txtCommiter.Size = new System.Drawing.Size(121, 21);
            this.txtCommiter.TabIndex = 26;
            this.txtCommiter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(74, 41);
            this.txtName.MaxLength = 200;
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(316, 21);
            this.txtName.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 26);
            this.label1.TabIndex = 23;
            this.label1.Text = "标题";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(219, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 26);
            this.label13.TabIndex = 29;
            this.label13.Text = "状态";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.lstAtt);
            this.tabPage2.Controls.Add(this.dgSR);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(407, 301);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "需求分析";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(8, 178);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 26);
            this.label16.TabIndex = 11;
            this.label16.Text = "附件(双击下载)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(9, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 26);
            this.label15.TabIndex = 11;
            this.label15.Text = "拆分开发任务";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstAtt
            // 
            this.lstAtt.FormattingEnabled = true;
            this.lstAtt.ItemHeight = 12;
            this.lstAtt.Location = new System.Drawing.Point(8, 207);
            this.lstAtt.Name = "lstAtt";
            this.lstAtt.Size = new System.Drawing.Size(389, 88);
            this.lstAtt.TabIndex = 2;
            // 
            // dgSR
            // 
            this.dgSR.AllowUserToAddRows = false;
            this.dgSR.AllowUserToDeleteRows = false;
            this.dgSR.BackgroundColor = System.Drawing.Color.LightGray;
            this.dgSR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSr,
            this.colState,
            this.colVer,
            this.colTitle});
            this.dgSR.Location = new System.Drawing.Point(9, 33);
            this.dgSR.Name = "dgSR";
            this.dgSR.ReadOnly = true;
            this.dgSR.RowHeadersVisible = false;
            this.dgSR.RowTemplate.Height = 20;
            this.dgSR.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSR.Size = new System.Drawing.Size(389, 139);
            this.dgSR.TabIndex = 1;
            this.dgSR.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSR_CellDoubleClick);
            // 
            // colSr
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colSr.DefaultCellStyle = dataGridViewCellStyle4;
            this.colSr.HeaderText = "编号";
            this.colSr.MinimumWidth = 100;
            this.colSr.Name = "colSr";
            this.colSr.ReadOnly = true;
            this.colSr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colState
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colState.DefaultCellStyle = dataGridViewCellStyle5;
            this.colState.HeaderText = "状态";
            this.colState.MinimumWidth = 80;
            this.colState.Name = "colState";
            this.colState.ReadOnly = true;
            this.colState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colState.Width = 80;
            // 
            // colVer
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colVer.DefaultCellStyle = dataGridViewCellStyle6;
            this.colVer.HeaderText = "版本号";
            this.colVer.MinimumWidth = 100;
            this.colVer.Name = "colVer";
            this.colVer.ReadOnly = true;
            this.colVer.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colTitle
            // 
            this.colTitle.HeaderText = "标题";
            this.colTitle.MinimumWidth = 250;
            this.colTitle.Name = "colTitle";
            this.colTitle.ReadOnly = true;
            this.colTitle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTitle.Width = 250;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(325, 333);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = " 确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // ViewNeed
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(415, 365);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewNeed";
            this.Text = "ViewNEED";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label12;
        private ComboBox cmbSubsys;
        private TextBox txtState;
        private TextBox txtId;
        private Label label2;
        private TextBox txtName;
        private Label label1;
        private Label label13;
        private ComboBox cmbBenef;
        private Label label5;
        private Label label10;
        private TextBox txtDesc;
        private Label label8;
        private TextBox txtCommiter;
        private Label label11;
        private TextBox txtCmtTime;
        private TextBox txtWishTime;
        private Label label9;
        private TextBox txtPasTime;
        private Label label14;
        private DataGridView dgSR;
        private ListBox lstAtt;
        private Label label15;
        private Label label16;
        private Button btnOK;
        private DataGridViewTextBoxColumn colSr;
        private DataGridViewTextBoxColumn colState;
        private DataGridViewTextBoxColumn colVer;
        private DataGridViewTextBoxColumn colTitle;
    }
}