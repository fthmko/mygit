﻿using System;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper {
    public partial class ViewNeed : Form {
        public ViewNeed() {
            InitializeComponent();
            Icon = Resources.ico;
        }

        private bool Reload() {
            var jo = Common.GetNeedInfo(txtId.Text);
            if (jo != null) {
                SetInfo(jo);
            }
            return (jo != null);
        }

        public bool SetView(string ndid) {
            txtId.Text = ndid;
            tabControl1.Select();
            return Reload();
        }

        public void ShowView(string ndid) {
            if (SetView(ndid)) {
                Show();
            } else {
                Msg("未找到" + ndid);
            }
        }

        private void SetInfo(MapDict jo) {
            txtName.Text = jo["needName"];
            Text = @"原始需求 - " + txtId.Text + @" - " + txtName.Text;
            txtState.Text = jo["needStatus"];
            cmbSubsys.Items.Add(jo["subSysName"]);
            cmbBenef.Items.Add(jo["needBenefName"]);
            cmbSubsys.SelectedIndex = 0;
            cmbBenef.SelectedIndex = 0;
            txtPasTime.Text = jo["appDate"];
            txtCommiter.Text = jo["needCommiter"];
            txtCmtTime.Text = jo["commitDate"];
            txtWishTime.Text = jo["expectDate"];
            txtDesc.Text = jo["needDesc"];

            var srs = Common.GetNeedSr(txtId.Text);
            dgSR.Rows.Clear();
            foreach (var map in srs) {
                dgSR.Rows.Add(map["srId"], map["srState"], map["srRm"], map["srDesc"]);
            }
        }

        void Msg(string txt) {
            MessageBox.Show(this, txt);
        }

        private void dgSR_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) return;
            dgSR.Rows[e.RowIndex].Selected = true;
            var frm = new ViewSr();
            if (!frm.SetView(dgSR.Rows[e.RowIndex].Cells[0].Value.ToString())) return;
            lock (Common.FrmPool) {
                Common.FrmPool.Add(frm);
            }
            frm.Show();
        }

        private void btnOK_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
