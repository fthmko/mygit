﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DPMHelper {
    partial class ViewRm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCreateDate = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtLock = new System.Windows.Forms.TextBox();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUAT = new System.Windows.Forms.TextBox();
            this.txtPrd = new System.Windows.Forms.TextBox();
            this.txtStg = new System.Windows.Forms.TextBox();
            this.txtTestEnv = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblRmType = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgContent = new System.Windows.Forms.DataGridView();
            this.colCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDevs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnMgrRm = new System.Windows.Forms.Button();
            this.txtSvnPath = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContent)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 327);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtCreateDate);
            this.tabPage1.Controls.Add(this.txtState);
            this.tabPage1.Controls.Add(this.txtLock);
            this.tabPage1.Controls.Add(this.txtCreator);
            this.tabPage1.Controls.Add(this.txtId);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtUAT);
            this.tabPage1.Controls.Add(this.txtPrd);
            this.tabPage1.Controls.Add(this.txtStg);
            this.tabPage1.Controls.Add(this.txtTestEnv);
            this.tabPage1.Controls.Add(this.txtSvnPath);
            this.tabPage1.Controls.Add(this.txtVersion);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.lblRmType);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(407, 301);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(203, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 26);
            this.label11.TabIndex = 25;
            this.label11.Text = "移交时间";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(8, 241);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 26);
            this.label10.TabIndex = 27;
            this.label10.Text = "测试环境";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(219, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 26);
            this.label6.TabIndex = 22;
            this.label6.Text = "状态";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(203, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 26);
            this.label5.TabIndex = 22;
            this.label5.Text = "创建时间";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 26);
            this.label4.TabIndex = 22;
            this.label4.Text = "冻结时间";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 26);
            this.label3.TabIndex = 22;
            this.label3.Text = "创建人";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 26);
            this.label12.TabIndex = 22;
            this.label12.Text = "编号";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCreateDate
            // 
            this.txtCreateDate.Location = new System.Drawing.Point(269, 93);
            this.txtCreateDate.Name = "txtCreateDate";
            this.txtCreateDate.ReadOnly = true;
            this.txtCreateDate.Size = new System.Drawing.Size(121, 21);
            this.txtCreateDate.TabIndex = 5;
            this.txtCreateDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(285, 15);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(105, 21);
            this.txtState.TabIndex = 1;
            this.txtState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLock
            // 
            this.txtLock.Location = new System.Drawing.Point(74, 154);
            this.txtLock.Name = "txtLock";
            this.txtLock.ReadOnly = true;
            this.txtLock.Size = new System.Drawing.Size(121, 21);
            this.txtLock.TabIndex = 6;
            this.txtLock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCreator
            // 
            this.txtCreator.Location = new System.Drawing.Point(74, 93);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(121, 21);
            this.txtCreator.TabIndex = 4;
            this.txtCreator.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(74, 15);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(121, 21);
            this.txtId.TabIndex = 0;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 26);
            this.label2.TabIndex = 16;
            this.label2.Text = "SVN地址";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUAT
            // 
            this.txtUAT.Location = new System.Drawing.Point(74, 181);
            this.txtUAT.Name = "txtUAT";
            this.txtUAT.ReadOnly = true;
            this.txtUAT.Size = new System.Drawing.Size(121, 21);
            this.txtUAT.TabIndex = 8;
            this.txtUAT.Text = "9999-99-99 99:99:99";
            this.txtUAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPrd
            // 
            this.txtPrd.Location = new System.Drawing.Point(269, 181);
            this.txtPrd.Name = "txtPrd";
            this.txtPrd.ReadOnly = true;
            this.txtPrd.Size = new System.Drawing.Size(121, 21);
            this.txtPrd.TabIndex = 9;
            this.txtPrd.Text = "9999-99-99 99:99:99";
            this.txtPrd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtStg
            // 
            this.txtStg.Location = new System.Drawing.Point(269, 154);
            this.txtStg.Name = "txtStg";
            this.txtStg.ReadOnly = true;
            this.txtStg.Size = new System.Drawing.Size(121, 21);
            this.txtStg.TabIndex = 7;
            this.txtStg.Text = "9999-99-99 99:99:99";
            this.txtStg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTestEnv
            // 
            this.txtTestEnv.Location = new System.Drawing.Point(74, 243);
            this.txtTestEnv.Multiline = true;
            this.txtTestEnv.Name = "txtTestEnv";
            this.txtTestEnv.ReadOnly = true;
            this.txtTestEnv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTestEnv.Size = new System.Drawing.Size(316, 41);
            this.txtTestEnv.TabIndex = 10;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(74, 41);
            this.txtVersion.MaxLength = 200;
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.ReadOnly = true;
            this.txtVersion.Size = new System.Drawing.Size(191, 21);
            this.txtVersion.TabIndex = 2;
            this.txtVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 26);
            this.label7.TabIndex = 25;
            this.label7.Text = "UAT时间";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 26);
            this.label1.TabIndex = 11;
            this.label1.Text = "版本号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(203, 178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 26);
            this.label14.TabIndex = 25;
            this.label14.Text = "发布时间";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(15, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 26);
            this.label9.TabIndex = 22;
            this.label9.Text = "测试环境信息";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(15, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 26);
            this.label8.TabIndex = 22;
            this.label8.Text = "版本发布计划";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRmType
            // 
            this.lblRmType.Location = new System.Drawing.Point(285, 38);
            this.lblRmType.Name = "lblRmType";
            this.lblRmType.Size = new System.Drawing.Size(105, 26);
            this.lblRmType.TabIndex = 22;
            this.lblRmType.Text = "常规版本";
            this.lblRmType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgContent);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(407, 301);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "版本内容";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgContent
            // 
            this.dgContent.AllowUserToAddRows = false;
            this.dgContent.AllowUserToDeleteRows = false;
            this.dgContent.BackgroundColor = System.Drawing.Color.LightGray;
            this.dgContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgContent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCd,
            this.colTitle,
            this.colStatus,
            this.colNeed,
            this.colDevs});
            this.dgContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgContent.Location = new System.Drawing.Point(3, 3);
            this.dgContent.Name = "dgContent";
            this.dgContent.ReadOnly = true;
            this.dgContent.RowHeadersVisible = false;
            this.dgContent.RowTemplate.Height = 20;
            this.dgContent.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgContent.Size = new System.Drawing.Size(401, 295);
            this.dgContent.TabIndex = 0;
            this.dgContent.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContent_CellDoubleClick);
            // 
            // colCd
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colCd.DefaultCellStyle = dataGridViewCellStyle2;
            this.colCd.Frozen = true;
            this.colCd.HeaderText = "编号";
            this.colCd.MinimumWidth = 100;
            this.colCd.Name = "colCd";
            this.colCd.ReadOnly = true;
            this.colCd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colTitle
            // 
            this.colTitle.HeaderText = "标题";
            this.colTitle.MinimumWidth = 240;
            this.colTitle.Name = "colTitle";
            this.colTitle.ReadOnly = true;
            this.colTitle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTitle.Width = 240;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "状态";
            this.colStatus.MinimumWidth = 90;
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colStatus.Width = 90;
            // 
            // colNeed
            // 
            this.colNeed.HeaderText = "原始需求";
            this.colNeed.MinimumWidth = 100;
            this.colNeed.Name = "colNeed";
            this.colNeed.ReadOnly = true;
            this.colNeed.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colDevs
            // 
            this.colDevs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colDevs.HeaderText = "开发人员";
            this.colDevs.MinimumWidth = 60;
            this.colDevs.Name = "colDevs";
            this.colDevs.ReadOnly = true;
            this.colDevs.Width = 76;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(328, 333);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = " 确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnMgrRm
            // 
            this.btnMgrRm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMgrRm.Location = new System.Drawing.Point(4, 333);
            this.btnMgrRm.Name = "btnMgrRm";
            this.btnMgrRm.Size = new System.Drawing.Size(75, 23);
            this.btnMgrRm.TabIndex = 3;
            this.btnMgrRm.Text = "版本管理";
            this.btnMgrRm.UseVisualStyleBackColor = true;
            this.btnMgrRm.Click += new System.EventHandler(this.btnMgrRm_Click);
            // 
            // txtSvnPath
            // 
            this.txtSvnPath.Location = new System.Drawing.Point(74, 68);
            this.txtSvnPath.MaxLength = 200;
            this.txtSvnPath.Name = "txtSvnPath";
            this.txtSvnPath.ReadOnly = true;
            this.txtSvnPath.Size = new System.Drawing.Size(316, 21);
            this.txtSvnPath.TabIndex = 2;
            // 
            // ViewRm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(415, 365);
            this.Controls.Add(this.btnMgrRm);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewRm";
            this.Text = "ViewRM";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgContent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private Label label11;
        private Label label10;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label12;
        private TextBox txtCreateDate;
        private TextBox txtState;
        private TextBox txtLock;
        private TextBox txtCreator;
        private TextBox txtId;
        private Label label2;
        private TextBox txtPrd;
        private TextBox txtStg;
        private TextBox txtTestEnv;
        private TextBox txtVersion;
        private Label label1;
        private Label lblRmType;
        private Label label14;
        private TabPage tabPage2;
        private DataGridView dgContent;
        private Label label6;
        private TextBox txtUAT;
        private Label label7;
        private Label label9;
        private Label label8;
        private Button btnOK;
        private DataGridViewTextBoxColumn colCd;
        private DataGridViewTextBoxColumn colTitle;
        private DataGridViewTextBoxColumn colStatus;
        private DataGridViewTextBoxColumn colNeed;
        private DataGridViewTextBoxColumn colDevs;
        private Button btnMgrRm;
        private TextBox txtSvnPath;
    }
}