﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DPMHelper.Properties;

namespace DPMHelper {
    public partial class ViewRm : Form {
        public ViewRm() {
            InitializeComponent();
            Icon = Resources.ico;
            dgContent.AlternatingRowsDefaultCellStyle.BackColor = Color.Honeydew;
        }

        public bool SetView(string id) {
            txtId.Text = id;
            tabControl1.Select();

            var rmInfo = Common.GetRmInfo(id);
            if (rmInfo == null) return false;

            var contentList = Common.GetRmContent(id);
            txtState.Text = rmInfo["rmStatus"];
            txtSvnPath.Text = rmInfo["svnPath"];
            txtVersion.Text = rmInfo["rmVersion"];
            txtCreator.Text = rmInfo["rmCreator"];
            txtCreateDate.Text = rmInfo["rmCreateDate"];
            txtLock.Text = rmInfo["rmLockStgDate"];
            txtStg.Text = rmInfo["rmStgDate"];
            txtUAT.Text = rmInfo["rmUatDate"];
            lblRmType.Text = rmInfo["rmType"];
            txtPrd.Text = rmInfo["rmPrdDate"];
            txtTestEnv.Text = rmInfo["rmTestEnv"];
            Text = @"版本 - " + txtId.Text + @" - " + txtVersion.Text;

            dgContent.Rows.Clear();
            foreach (var map in contentList) {
                dgContent.Rows.Add(map["srId"], map["title"], map["status"], map["needId"], map["devs"]);
            }

            return true;
        }

        public void ShowView(string id) {
            if (SetView(id)) {
                Show();
            } else {
                Msg("未找到" + id);
            }
        }

        private void btnOK_Click(object sender, EventArgs e) {
            Close();
        }

        void Msg(string txt) {
            MessageBox.Show(this, txt);
        }

        private void dgContent_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) return;
            dgContent.Rows[e.RowIndex].Selected = true;
            var frm = new ViewSr();
            if (!frm.SetView(dgContent.Rows[e.RowIndex].Cells[0].Value.ToString())) return;
            lock (Common.FrmPool) {
                Common.FrmPool.Add(frm);
            }
            frm.Show();
        }

        private void btnMgrRm_Click(object sender, EventArgs e) {
            Common.RefManageRmContent(txtId.Text, txtState.Text != @"新建" && txtState.Text != @"已首移");
        }
    }
}
