﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DPMHelper {
    partial class ViewSr {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtWork = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.txtUp = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtPrd = new System.Windows.Forms.TextBox();
            this.txtStg = new System.Windows.Forms.TextBox();
            this.txtPlan = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblCdCnt = new System.Windows.Forms.Label();
            this.btnSplit = new System.Windows.Forms.Button();
            this.dgCd = new System.Windows.Forms.DataGridView();
            this.colCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.mnuCD = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuFinish = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuChg = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnFinish = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCd)).BeginInit();
            this.mnuCD.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 327);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtWork);
            this.tabPage1.Controls.Add(this.txtState);
            this.tabPage1.Controls.Add(this.txtVersion);
            this.tabPage1.Controls.Add(this.txtUp);
            this.tabPage1.Controls.Add(this.txtId);
            this.tabPage1.Controls.Add(this.txtPrd);
            this.tabPage1.Controls.Add(this.txtStg);
            this.tabPage1.Controls.Add(this.txtPlan);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(407, 301);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 267);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 26);
            this.label11.TabIndex = 25;
            this.label11.Text = "移交时间";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(8, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 26);
            this.label10.TabIndex = 27;
            this.label10.Text = "分析方案";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(205, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 26);
            this.label5.TabIndex = 22;
            this.label5.Text = "工作量";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 26);
            this.label4.TabIndex = 22;
            this.label4.Text = "版本号";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 26);
            this.label3.TabIndex = 22;
            this.label3.Text = "上游编号";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 26);
            this.label12.TabIndex = 22;
            this.label12.Text = "编号";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWork
            // 
            this.txtWork.Location = new System.Drawing.Point(269, 93);
            this.txtWork.Name = "txtWork";
            this.txtWork.ReadOnly = true;
            this.txtWork.Size = new System.Drawing.Size(121, 21);
            this.txtWork.TabIndex = 5;
            this.txtWork.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtWork.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtUp_MouseDoubleClick);
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(285, 15);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(105, 21);
            this.txtState.TabIndex = 1;
            this.txtState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(74, 121);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.ReadOnly = true;
            this.txtVersion.Size = new System.Drawing.Size(191, 21);
            this.txtVersion.TabIndex = 6;
            this.txtVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVersion.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtVersion_MouseDoubleClick);
            // 
            // txtUp
            // 
            this.txtUp.ForeColor = System.Drawing.Color.MediumBlue;
            this.txtUp.Location = new System.Drawing.Point(74, 93);
            this.txtUp.Name = "txtUp";
            this.txtUp.ReadOnly = true;
            this.txtUp.Size = new System.Drawing.Size(121, 21);
            this.txtUp.TabIndex = 4;
            this.txtUp.Text = "NEED";
            this.txtUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUp.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtUp_MouseDoubleClick);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(74, 15);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(121, 21);
            this.txtId.TabIndex = 0;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPrd
            // 
            this.txtPrd.Location = new System.Drawing.Point(269, 270);
            this.txtPrd.Name = "txtPrd";
            this.txtPrd.ReadOnly = true;
            this.txtPrd.Size = new System.Drawing.Size(121, 21);
            this.txtPrd.TabIndex = 9;
            this.txtPrd.Text = "9999-99-99 99:99:99";
            this.txtPrd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtStg
            // 
            this.txtStg.Location = new System.Drawing.Point(74, 270);
            this.txtStg.Name = "txtStg";
            this.txtStg.ReadOnly = true;
            this.txtStg.Size = new System.Drawing.Size(121, 21);
            this.txtStg.TabIndex = 8;
            this.txtStg.Text = "9999-99-99 99:99:99";
            this.txtStg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPlan
            // 
            this.txtPlan.Location = new System.Drawing.Point(74, 150);
            this.txtPlan.Multiline = true;
            this.txtPlan.Name = "txtPlan";
            this.txtPlan.ReadOnly = true;
            this.txtPlan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPlan.Size = new System.Drawing.Size(316, 111);
            this.txtPlan.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(74, 41);
            this.txtName.MaxLength = 200;
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(316, 21);
            this.txtName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 26);
            this.label1.TabIndex = 11;
            this.label1.Text = "标题";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(219, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 26);
            this.label13.TabIndex = 22;
            this.label13.Text = "状态";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(205, 267);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 26);
            this.label14.TabIndex = 25;
            this.label14.Text = "发布时间";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblCdCnt);
            this.tabPage2.Controls.Add(this.btnSplit);
            this.tabPage2.Controls.Add(this.dgCd);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(407, 301);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "编码任务";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblCdCnt
            // 
            this.lblCdCnt.AutoSize = true;
            this.lblCdCnt.Location = new System.Drawing.Point(117, 13);
            this.lblCdCnt.Name = "lblCdCnt";
            this.lblCdCnt.Size = new System.Drawing.Size(11, 12);
            this.lblCdCnt.TabIndex = 14;
            this.lblCdCnt.Text = "0";
            // 
            // btnSplit
            // 
            this.btnSplit.Location = new System.Drawing.Point(323, 5);
            this.btnSplit.Name = "btnSplit";
            this.btnSplit.Size = new System.Drawing.Size(75, 23);
            this.btnSplit.TabIndex = 1;
            this.btnSplit.Text = "拆分";
            this.btnSplit.UseVisualStyleBackColor = true;
            this.btnSplit.Click += new System.EventHandler(this.btnSplit_Click);
            // 
            // dgCd
            // 
            this.dgCd.AllowUserToAddRows = false;
            this.dgCd.AllowUserToDeleteRows = false;
            this.dgCd.BackgroundColor = System.Drawing.Color.LightGray;
            this.dgCd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCd,
            this.colDev,
            this.colUM,
            this.colState});
            this.dgCd.Location = new System.Drawing.Point(9, 33);
            this.dgCd.Name = "dgCd";
            this.dgCd.ReadOnly = true;
            this.dgCd.RowHeadersVisible = false;
            this.dgCd.RowTemplate.Height = 20;
            this.dgCd.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCd.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgCd.Size = new System.Drawing.Size(389, 262);
            this.dgCd.TabIndex = 0;
            this.dgCd.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgCd_CellMouseUp);
            // 
            // colCd
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colCd.DefaultCellStyle = dataGridViewCellStyle1;
            this.colCd.Frozen = true;
            this.colCd.HeaderText = "编号";
            this.colCd.MinimumWidth = 100;
            this.colCd.Name = "colCd";
            this.colCd.ReadOnly = true;
            this.colCd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colDev
            // 
            this.colDev.Frozen = true;
            this.colDev.HeaderText = "开发";
            this.colDev.MinimumWidth = 60;
            this.colDev.Name = "colDev";
            this.colDev.ReadOnly = true;
            this.colDev.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colDev.Width = 60;
            // 
            // colUM
            // 
            this.colUM.Frozen = true;
            this.colUM.HeaderText = "UM账号";
            this.colUM.MinimumWidth = 120;
            this.colUM.Name = "colUM";
            this.colUM.ReadOnly = true;
            this.colUM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colUM.Width = 120;
            // 
            // colState
            // 
            this.colState.Frozen = true;
            this.colState.HeaderText = "状态";
            this.colState.MinimumWidth = 90;
            this.colState.Name = "colState";
            this.colState.ReadOnly = true;
            this.colState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colState.Width = 90;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(8, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 26);
            this.label16.TabIndex = 10;
            this.label16.Text = "已拆分的编码任务";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuCD
            // 
            this.mnuCD.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFinish,
            this.mnuClose,
            this.mnuChg});
            this.mnuCD.Name = "mnuCD";
            this.mnuCD.ShowImageMargin = false;
            this.mnuCD.ShowItemToolTips = false;
            this.mnuCD.Size = new System.Drawing.Size(103, 70);
            // 
            // mnuFinish
            // 
            this.mnuFinish.Name = "mnuFinish";
            this.mnuFinish.ShowShortcutKeys = false;
            this.mnuFinish.Size = new System.Drawing.Size(102, 22);
            this.mnuFinish.Text = "完成编码";
            this.mnuFinish.Click += new System.EventHandler(this.mnuFinish_Click);
            // 
            // mnuClose
            // 
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.ShowShortcutKeys = false;
            this.mnuClose.Size = new System.Drawing.Size(102, 22);
            this.mnuClose.Text = "异常关闭";
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            // 
            // mnuChg
            // 
            this.mnuChg.Name = "mnuChg";
            this.mnuChg.ShowShortcutKeys = false;
            this.mnuChg.Size = new System.Drawing.Size(102, 22);
            this.mnuChg.Text = "查看变更集";
            this.mnuChg.Click += new System.EventHandler(this.mnuChg_Click);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(325, 333);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = " 确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(14, 332);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(75, 23);
            this.btnFinish.TabIndex = 1;
            this.btnFinish.Text = "完成编码";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // ViewSr
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(415, 365);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewSr";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCd)).EndInit();
            this.mnuCD.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Button btnOK;
        private Label label10;
        private TextBox txtName;
        private Label label1;
        private Label label13;
        private Label label12;
        private TextBox txtState;
        private TextBox txtId;
        private Label label14;
        private Label label11;
        private DataGridView dgCd;
        private Label label16;
        private Button btnSplit;
        private ContextMenuStrip mnuCD;
        private ToolStripMenuItem mnuFinish;
        private ToolStripMenuItem mnuClose;
        private ToolStripMenuItem mnuChg;
        private TextBox txtPrd;
        private TextBox txtStg;
        private DataGridViewTextBoxColumn colCd;
        private DataGridViewTextBoxColumn colDev;
        private DataGridViewTextBoxColumn colUM;
        private DataGridViewTextBoxColumn colState;
        private Label lblCdCnt;
        private Label label3;
        private TextBox txtUp;
        private Label label5;
        private Label label4;
        private TextBox txtWork;
        private TextBox txtVersion;
        private TextBox txtPlan;
        private Button btnFinish;
    }
}