﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DPMHelper.Properties;
using Newtonsoft.Json.Linq;

namespace DPMHelper
{
    public partial class ViewSr : Form
    {
        JObject _srInfo;

        public ViewSr()
        {
            InitializeComponent();
            Icon = Resources.ico;
            dgCd.AlternatingRowsDefaultCellStyle.BackColor = Color.Honeydew;
        }

        private bool Reload()
        {
            var jo = Common.GetSrInfo(txtId.Text);
            if (jo != null)
            {
                SetInfo(jo);
            }
            return (jo != null);
        }

        public bool SetView(string srid)
        {
            txtId.Text = srid;
            tabControl1.Select();
            return Reload();
        }

        public void ShowView(string srid)
        {
            if (SetView(srid))
            {
                Show();
            }
            else
            {
                Msg("未找到" + srid);
            }
        }

        private void SetInfo(JObject info)
        {
            _srInfo = info;
            txtState.Text = info["srDTO"]["srState"].ToString().Trim();
            btnFinish.Enabled = (txtState.Text == @"新建" && info["srDTO"]["rmId"].ToString().Length > 3);

            txtName.Text = info["srDTO"]["srName"].ToString().Trim();
            txtUp.Text = info["srDTO"]["sourceId"].ToString().Trim();
            Text = @"开发任务 - " + txtId.Text + @" - " + txtName.Text;
            txtWork.Text = info["srDTO"]["expCost"].ToString().Trim() + @"人日";
            txtPlan.Text = info["srDTO"]["analysePlan"].ToString().Trim();
            //"srStateCode" : "RELEASED",
            txtStg.Text = "";
            txtPrd.Text = "";
            if (info["srDTO"]["rmId"] != null && info["srDTO"]["rmId"].ToString().Length > 3)
            {
                Dictionary<string, string> rmInfo = Common.GetRmInfo(info["srDTO"]["rmId"].ToString());
                txtVersion.Text = rmInfo["rmVersion"];

                txtStg.Text = rmInfo["rmStgDate"];
                txtPrd.Text = rmInfo["rmPrdDate"];
            }
            btnSplit.Enabled = (txtState.Text == @"新建");
            RefreshCd();
        }

        private void RefreshCd()
        {
            var cds = Common.GetSrCd(txtId.Text);
            dgCd.Rows.Clear();
            foreach (var map in cds)
            {
                dgCd.Rows.Add(map["id"], map["name"], map["um"], map["state"]);
            }
            lblCdCnt.Text = "" + cds.Count;
        }
        private void dgCd_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.Button == MouseButtons.Right)
            {
                dgCd.ClearSelection();
                dgCd.Rows[e.RowIndex].Selected = true;
                mnuFinish.Enabled = false;
                mnuClose.Enabled = false;
                if (dgCd.Rows[e.RowIndex].Cells[3].Value.ToString() == "开发中")
                {
                    mnuFinish.Enabled = true;
                    mnuClose.Enabled = true;
                }
                if (dgCd.Rows[e.RowIndex].Cells[3].Value.ToString() == "已完成编码")
                {
                    mnuClose.Enabled = true;
                }
                mnuCD.Tag = dgCd.Rows[e.RowIndex].Cells[2].Value;
                mnuCD.Show(MousePosition.X, MousePosition.Y);
            }
        }

        void Msg(string txt)
        {
            MessageBox.Show(this, txt);
        }

        private void txtUp_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (txtUp.Text.StartsWith("NEED_"))
            {
                var frm = new ViewNeed();
                if (!frm.SetView(txtUp.Text)) return;
                lock (Common.FrmPool)
                {
                    Common.FrmPool.Add(frm);
                }
                frm.Show();
            }
            else
            {
                // TODO PRO/SPRO
                Msg("暂不支持");
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            var res = Common.FinishSr(txtId.Text, _srInfo["srDTO"]["rmId"].ToString().Trim());
            Msg((res != null && res["isSucc"].ToString() == "Y") ? "操作成功" : "操作失败");
            Reload();
        }

        private void mnuChg_Click(object sender, EventArgs e)
        {
            try
            {
                var frm = new ViewChange();
                var list = Common.GetSvnChanges(_srInfo["srDTO"]["rmId"].ToString(), txtId.Text, mnuCD.Tag.ToString());
                frm.List = list;
                frm.SetTitle("查看变更集 - " + txtId.Text + " - " + mnuCD.Tag);
                lock (Common.FrmPool)
                {
                    Common.FrmPool.Add(frm);
                }
                frm.Show();
            }
            catch (Exception x)
            {
                Msg(x.Message);
            }
        }

        private void btnSplit_Click(object sender, EventArgs e)
        {
            Common.RefSplitDeveloper(txtId.Text);
            RefreshCd();
        }

        private void mnuFinish_Click(object sender, EventArgs e)
        {
            var result = Common.FinishCd(txtId.Text, mnuCD.Tag.ToString());
            Msg(result != null ? result["resultMsg"].ToString() : "未知错误！");
            RefreshCd();
        }

        private void mnuClose_Click(object sender, EventArgs e)
        {
            Msg("TODO");
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtVersion_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (txtVersion.Text.Length > 3)
            {
                var rmId = Common.SearchRmId(txtVersion.Text);
                if (rmId != null)
                {
                    var frm = new ViewRm();
                    frm.SetView(rmId);
                    lock (Common.FrmPool)
                    {
                        Common.FrmPool.Add(frm);
                    }
                    frm.Show();
                }
            }
        }
    }
}
