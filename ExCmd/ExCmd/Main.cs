﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace ExCmd
{
    public partial class Main : Form
    {
        System.Diagnostics.Process prc;
        public Main()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void appendText(String txt, Color cr)
        {
            txtConsole.Select(txtConsole.TextLength, 0);
            txtConsole.SelectionColor = cr;
            txtConsole.AppendText(txt + Environment.NewLine);
            txtConsole.Select(txtConsole.TextLength, 0);
            txtConsole.ScrollToCaret();
            txtConsole.Refresh();
        }
        private string runCmd(string strFile, string args, string dir)
        {
            prc = new System.Diagnostics.Process();
            prc.StartInfo = new System.Diagnostics.ProcessStartInfo();
            prc.StartInfo.WorkingDirectory = dir;
            prc.StartInfo.FileName = strFile;
            prc.StartInfo.Arguments = args;
            prc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            prc.StartInfo.RedirectStandardOutput = true;
            prc.StartInfo.RedirectStandardError = true;
            prc.StartInfo.UseShellExecute = false;
            prc.StartInfo.CreateNoWindow = true;
            prc.Start();
            string ot = prc.StandardOutput.ReadToEnd();
            appendText(prc.StandardError.ReadToEnd(), Color.Red);
            prc.WaitForExit();
            //appendText(ot, Color.White);
            return ot;
        }

        string findText(string src, string reg)
        {
            if (string.IsNullOrEmpty(src)) return "";
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return "";
            return mc[0].Value.Replace("&amp;", "&");
        }

        private void fillTable(string str, string id)
        {
            TableRow tr = new TableRow();
            tr.owner = findText(str, "(?<=owner: PAICDOM\\\\).+(?=\n)");
            tr.title = findText(str, "(?<=title: ).+(?=\n)");
            tr.stage = findText(str, "(?<=State: ).+$");
            tr.id = id;

            if (tr.owner == "")
            {
                dgDetail.Rows.Add(new object[] { "查找失败", id, "", "", "", "", "" });
            }
            else
            {
                string[] rows = str.Split('\n');
                string rw;
                List<TableRow> gridRows = new List<TableRow>();
                foreach (string row in rows)
                {
                    rw = row.Trim();
                    if (rw.StartsWith(txtFolder.Text))
                    {
                        tr = new TableRow();
                        tr.owner = findText(str, "(?<=owner: PAICDOM\\\\).+(?=\n)");
                        tr.title = findText(str, "(?<=title: ).+(?=\n)");
                        tr.stage = findText(str, "(?<=State: ).+$");
                        tr.id = id;
                        tr.version = findText(rw, "(?<=\\\\)\\d+$");
                        tr.file = findText(rw, "^.+(?=@@)");
                        if (tr.file.Contains("\\dml\\"))
                        {
                            if (!tr.file.ToUpper().EndsWith(".SQL"))
                            {
                                continue;
                            }
                            tr.type = "elis_pos_3v_dml";
                            if (tr.file.Contains("\\lbsbranch\\"))
                            {
                                tr.db = "机构库";
                            }
                            else
                            {
                                tr.db = "中心库";
                            }
                            tr.file = findText(tr.file, "(?<=\\\\)[^\\\\]+$");
                        }
                        else if (tr.file.Contains("\\elis_pos_3v_j2ee\\"))
                        {
                            tr.type = "elis_pos_3v_j2ee";
                            tr.file = "elispos.ear";
                        }
                        else
                        {
                            if (!tr.file.ToUpper().EndsWith(".SQL"))
                            {
                                continue;
                            }
                            tr.type = "elis_pos_3v_pkg";
                            if (tr.file.Contains("\\lbsbranch\\"))
                            {
                                tr.db = "机构库";
                            }
                            else
                            {
                                tr.db = "中心库";
                            }
                            tr.file = findText(tr.file, "(?<=\\\\)[^\\\\]+$");
                        }
                        gridRows.Add(tr);
                    }
                }

                if (gridRows.Count == 0)
                {
                    tr.file = "无";
                    dgDetail.Rows.Add(tr.getRow());
                }
                else
                {
                    gridRows.Sort();
                    for (int i = gridRows.Count - 1; i > 0; i--)
                    {
                        if (gridRows[i].id == gridRows[i - 1].id && gridRows[i].db == gridRows[i - 1].db && gridRows[i].file == gridRows[i - 1].file)
                        {
                            if (gridRows[i - 1].version != gridRows[i].version)
                            {
                                gridRows[i - 1].version += "," + gridRows[i].version;
                            }
                            gridRows.RemoveAt(i);
                        }
                    }
                    foreach (var gr in gridRows)
                    {
                        dgDetail.Rows.Add(gr.getRow());
                    }
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            dgDetail.Rows.Clear();
            txtConsole.Clear();
            string[] cds = txtCd.Text.Split('\n');
            foreach (string lcd in cds)
            {
                string cd = lcd.Replace("CD_", "").Trim();
                fillTable(runCmd("cleartool", "lsact -l " + cd, txtFolder.Text), cd);
            }
        }

    }

    class TableRow : IComparable<TableRow>
    {
        public string id = "", owner = "", title = "", stage = "", file = "", type = "", version = "", db = "";
        public object[] getRow()
        {
            return new object[] { owner, stage, id, type, file, version, db, title };
        }

        #region IComparable 成员

        public int CompareTo(TableRow obj)
        {
            int r = cmp(obj);
            Console.WriteLine(r);
            return r;
        }

        public int cmp(TableRow obj)
        {
            int ret = id.CompareTo(obj.id);
            if (ret != 0) return ret;
            ret = stage.CompareTo(obj.stage);
            if (ret != 0) return ret;
            //ret = type == "elis_pos_3v_j2ee" ? -1 : (obj.type == "elis_pos_3v_j2ee" ? 1 : type.CompareTo(obj.type));
            if (ret != 0) return ret;
            ret = db.CompareTo(obj.db);
            if (ret != 0) return ret;
            if (ret != 0) return ret;
            return version.CompareTo(obj.version);
        }

        #endregion
    }

    class LineItem
    {
        public Color Color { get; set; }
        public String Text { get; set; }
        public LineItem(String txt, Color cr)
        {
            Text = txt;
            Color = cr;
        }
    }

    /// <summary>
    /// 模拟VB的InputBox
    /// </summary>
    public class InputBox : System.Windows.Forms.Form
    {
        private System.Windows.Forms.TextBox txtData;
        private System.ComponentModel.Container components = null;

        private InputBox()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }

            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {

            this.txtData = new System.Windows.Forms.TextBox();
            this.SuspendLayout();

            this.txtData.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular,
                                                        System.Drawing.GraphicsUnit.Point, ((System.Byte)(134)));
            this.txtData.Location = new System.Drawing.Point(10, 8);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(330, 23);
            this.txtData.TabIndex = 0;
            this.txtData.Text = "";
            this.txtData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtData_KeyDown);

            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(350, 39);
            this.ControlBox = false;
            this.Controls.Add(this.txtData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "InputBox";
            this.Text = "InputBox";
            this.ResumeLayout(false);
            this.StartPosition = FormStartPosition.CenterParent;
        }

        //对键盘进行响应
        private void txtData_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Close();
            }

            else if (e.KeyCode == Keys.Escape)
            {
                txtData.Text = string.Empty;
                this.Close();
            }
        }

        /// <summary>
        /// 显示InputBox
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="defText">默认文字</param>
        /// <returns>输入文字</returns>
        public static string Show(string title, string defText)
        {
            InputBox inputbox = new InputBox();
            inputbox.Text = title;
            inputbox.txtData.Text = defText;
            inputbox.ShowDialog();
            return inputbox.txtData.Text;
        }
    }
}
