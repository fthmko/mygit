﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Fedora
{
    public abstract class Formula
    {
        public abstract String Name { get; }
        public abstract double Power { get; }
        public abstract double Angle { get; }
        public abstract Color Color { get; }
        public abstract void calc(double dist, double wind);
        protected virtual Color InvalidColor { get { return Color.FromArgb(64, 64, 64); } }
        public virtual bool isValid()
        {
            return Power > 5 && Angle > 0;
        }
        public virtual double getValue(double[] data, double val)
        {
            if (val < 0 || val > 20) return -1;
            int l = (int)Math.Floor(val);
            int r = (int)Math.Ceiling(val);
            return data[l] + (val - l) * (data[r] - data[l]);
        }
    }

    public class Formula30 : Formula
    {
        double[] POW_ARR = { 1, 14.0, 20.0, 24.7, 28.7, 32.3, 35.7, 38.8, 41.8, 44.7, 47.5, 50.2, 52.8, 55.3, 57.9, 60.3, 62.7, 65.7, 67.5, 69.8, 72.1 };
        double power = 99, angle = 30;
        Color _color = Color.Red;
        public override String Name
        {
            get { return "30度"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 30 + wind;
            power = getValue(POW_ARR, dist);
            //power = 0.0044 * dist * dist * dist - 0.1971 * dist * dist + 5.339 * dist + 9.7442;
        }
    }
    public class Formula50 : Formula
    {
        double[] POW_ARR = { 1, 14.1, 20.1, 24.8, 28.8, 32.5, 35.9, 39.0, 42.0, 44.9, 48.3, 50.5, 53.0, 55.5, 58.0, 60.5, 63.0, 65.5, 68.0, 70.0, 72.5 };
        double power = 99, angle = 50;
        Color _color = Color.RoyalBlue;
        public override String Name
        {
            get { return "50度"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 50 + wind * 2;
            power = getValue(POW_ARR, dist);
            //power = 0.0047 * dist * dist * dist - 0.2079 * dist * dist + 5.4534 * dist + 9.64;
        }
    }
    public class Formula65 : Formula
    {
        double[] POW_ARR = { 1, 13.0, 21.0, 26.0, 31.5, 37.0, 41.0, 44.0, 48.5, 53.0, 56.0, 58.0, 61.0, 64.0, 67.0, 70.0, 73.0, 76.0, 79.0, 82.0, 85.0 };
        double power = 99, angle = 65;
        Color _color = Color.Green;
        public override String Name
        {
            get { return "65度"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 65 + wind * 2;
            power = getValue(POW_ARR, dist);
            //power = 13.406 * Math.Pow(dist, 0.6144);
        }
    }
    public class FormulaHalf : Formula
    {
        double power = 60, angle = 80;
        Color _color = Color.Cyan;
        public override String Name
        {
            get { return "半抛"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = Math.Round(90 - dist * 2 + wind * 2);
            power = 60;
        }
    }
    public class FormulaFull : Formula
    {
        double power = 95, angle = 90;
        Color _color = Color.GreenYellow;
        public override String Name
        {
            get { return "高抛"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = Math.Round(90 - dist + wind * 2);
            power = 95;
        }
    }
    public class Formula50Fix : Formula
    {
        double[] POW_ARR1 = { 0, -0.355555556, -0.511111111, -0.622222222, -0.733333333, -0.822222222, -0.911111111, -0.977777778, -1.066666667, -1.133333333, -1.2, -1.266666667, -1.333333333, -1.4, -1.466666667, -1.533333333, -1.577777778, -1.644444444, -1.711111111, -1.755555556, -1.822222222, -1.866666667 };
        double[] POW_ARR2 = { 0, 14.1, 20.1, 24.8, 28.8, 32.5, 35.9, 39.0, 42.0, 44.9, 47.7, 50.4, 53.0, 55.6, 58.1, 60.6, 63.0, 65.4, 67.8, 70.2, 72.5, 74.8 };
        double power = 99, angle = 50;
        Color _color = Color.Yellow;
        public override String Name
        {
            get { return "50度定角"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 50;
            double pa = getValue(POW_ARR1, dist);
            double pw = getValue(POW_ARR2, dist);
            power = pa * wind + pw;
            //power = 0.0047 * dist * dist * dist - 0.2079 * dist * dist + 5.4534 * dist + 9.64 - wind;
        }
    }
    public class Formula45Fix : Formula
    {
        double power = 99, angle = 45;
        Color _color = Color.SpringGreen;
        public override String Name
        {
            get { return "45度定角"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 45;
            //power = getValue(POW_ARR, dist) - wind;
            power = 0.0046 * dist * dist * dist - 0.2141 * dist * dist + 5.4625 * dist + 7.1474 - wind;
        }
    }
    public class Formula30Fix : Formula
    {
        double[] POW_ARR = { 0, 14, 20, 24.7, 28.7, 32.3, 35.7, 38.8, 41.8, 44.7, 47.5, 50.2, 52.8, 55.3, 57.9, 60.3, 62.7, 65.7, 67.5, 69.8, 72.1 };
        double power = 99, angle = 30;
        Color _color = Color.LawnGreen;
        public override String Name
        {
            get { return "30度定角"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 30;
            power = getValue(POW_ARR, dist) - wind;
            //power = 0.0044 * dist * dist * dist - 0.1971 * dist * dist + 5.339 * dist + 9.7442 - wind;
        }
    }
    public class Formula20Fix : Formula
    {
        double[] POW_ARR = { 0, 14, 20, 24.7, 28.7, 32.3, 35.7, 38.8, 41.8, 44.7, 47.5, 50.2, 52.8, 55.3, 57.9, 60.3, 62.7, 65.7, 67.5, 69.8, 72.1 };
        double power = 99, angle = 20;
        Color _color = Color.HotPink;
        public override String Name
        {
            get { return "20度定角"; }
        }
        public override Color Color { get { return isValid() ? _color : InvalidColor; } }
        public override double Power { get { return power > 100 ? -1 : power; } }
        public override double Angle { get { return angle; } }
        public override void calc(double dist, double wind)
        {
            angle = 20;
            power = getValue(POW_ARR, dist) - wind;
            //power = 0.0066 * dist * dist * dist - 0.3206 * dist * dist + 7.6054 * dist + 4.2047 - wind / 2;
        }
    }
}
