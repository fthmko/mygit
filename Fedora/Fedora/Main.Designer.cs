﻿namespace Fedora
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalc = new System.Windows.Forms.Button();
            this.numWind = new System.Windows.Forms.NumericUpDown();
            this.numDist = new System.Windows.Forms.NumericUpDown();
            this.lstResult = new System.Windows.Forms.ListBox();
            this.cmList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pnlDisp = new System.Windows.Forms.Panel();
            this.pbBar = new System.Windows.Forms.PictureBox();
            this.cmBar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cbAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.cbLine = new System.Windows.Forms.ToolStripMenuItem();
            this.cbImg = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numWind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDist)).BeginInit();
            this.pnlDisp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBar)).BeginInit();
            this.cmBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(29, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "风速";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(123, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "距离";
            // 
            // btnCalc
            // 
            this.btnCalc.BackColor = System.Drawing.Color.Green;
            this.btnCalc.Location = new System.Drawing.Point(212, 1);
            this.btnCalc.Margin = new System.Windows.Forms.Padding(2);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(26, 26);
            this.btnCalc.TabIndex = 2;
            this.btnCalc.UseVisualStyleBackColor = false;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // numWind
            // 
            this.numWind.BackColor = System.Drawing.Color.Honeydew;
            this.numWind.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numWind.DecimalPlaces = 1;
            this.numWind.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numWind.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numWind.Location = new System.Drawing.Point(66, 3);
            this.numWind.Margin = new System.Windows.Forms.Padding(4);
            this.numWind.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numWind.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numWind.Name = "numWind";
            this.numWind.Size = new System.Drawing.Size(51, 23);
            this.numWind.TabIndex = 0;
            this.numWind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numWind.Enter += new System.EventHandler(this.numWind_Enter);
            this.numWind.Leave += new System.EventHandler(this.numWind_Leave);
            // 
            // numDist
            // 
            this.numDist.BackColor = System.Drawing.Color.Honeydew;
            this.numDist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numDist.DecimalPlaces = 1;
            this.numDist.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numDist.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDist.Location = new System.Drawing.Point(160, 3);
            this.numDist.Margin = new System.Windows.Forms.Padding(4);
            this.numDist.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.numDist.Name = "numDist";
            this.numDist.Size = new System.Drawing.Size(51, 23);
            this.numDist.TabIndex = 1;
            this.numDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numDist.Enter += new System.EventHandler(this.numWind_Enter);
            this.numDist.Leave += new System.EventHandler(this.numWind_Leave);
            // 
            // lstResult
            // 
            this.lstResult.AllowDrop = true;
            this.lstResult.BackColor = System.Drawing.Color.Black;
            this.lstResult.ContextMenuStrip = this.cmList;
            this.lstResult.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.lstResult.ForeColor = System.Drawing.Color.Gold;
            this.lstResult.FormattingEnabled = true;
            this.lstResult.Location = new System.Drawing.Point(3, 224);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(60, 240);
            this.lstResult.TabIndex = 0;
            this.lstResult.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstResult_DrawItem);
            this.lstResult.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.lstResult_MeasureItem);
            this.lstResult.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstResult_KeyDown);
            this.lstResult.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstResult_MouseDown);
            this.lstResult.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lstResult_MouseMove);
            this.lstResult.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lstResult_MouseUp);
            // 
            // cmList
            // 
            this.cmList.Name = "cmList";
            this.cmList.Size = new System.Drawing.Size(61, 4);
            // 
            // pnlDisp
            // 
            this.pnlDisp.BackColor = System.Drawing.Color.Black;
            this.pnlDisp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDisp.Controls.Add(this.pbBar);
            this.pnlDisp.Controls.Add(this.numWind);
            this.pnlDisp.Controls.Add(this.numDist);
            this.pnlDisp.Controls.Add(this.btnCalc);
            this.pnlDisp.Controls.Add(this.label1);
            this.pnlDisp.Controls.Add(this.label2);
            this.pnlDisp.Location = new System.Drawing.Point(363, 237);
            this.pnlDisp.Name = "pnlDisp";
            this.pnlDisp.Size = new System.Drawing.Size(243, 30);
            this.pnlDisp.TabIndex = 11;
            // 
            // pbBar
            // 
            this.pbBar.ContextMenuStrip = this.cmBar;
            this.pbBar.Image = global::Fedora.Properties.Resources.Annotation_New;
            this.pbBar.Location = new System.Drawing.Point(-2, -2);
            this.pbBar.Margin = new System.Windows.Forms.Padding(0);
            this.pbBar.Name = "pbBar";
            this.pbBar.Size = new System.Drawing.Size(32, 32);
            this.pbBar.TabIndex = 12;
            this.pbBar.TabStop = false;
            this.pbBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbBar_MouseDown);
            this.pbBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbBar_MouseMove);
            this.pbBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbBar_MouseUp);
            // 
            // cmBar
            // 
            this.cmBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbAcc,
            this.cbLine,
            this.cbImg,
            this.toolStripSeparator1,
            this.btnHelp,
            this.btnExit});
            this.cmBar.Name = "cmBar";
            this.cmBar.ShowCheckMargin = true;
            this.cmBar.ShowImageMargin = false;
            this.cmBar.Size = new System.Drawing.Size(153, 120);
            // 
            // cbAcc
            // 
            this.cbAcc.Checked = true;
            this.cbAcc.CheckOnClick = true;
            this.cbAcc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAcc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cbAcc.Name = "cbAcc";
            this.cbAcc.ShowShortcutKeys = false;
            this.cbAcc.Size = new System.Drawing.Size(152, 22);
            this.cbAcc.Text = "计算高度差";
            this.cbAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            // 
            // cbLine
            // 
            this.cbLine.Checked = true;
            this.cbLine.CheckOnClick = true;
            this.cbLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cbLine.Name = "cbLine";
            this.cbLine.ShowShortcutKeys = false;
            this.cbLine.Size = new System.Drawing.Size(152, 22);
            this.cbLine.Text = "画出抛物线";
            this.cbLine.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            // 
            // cbImg
            // 
            this.cbImg.Checked = true;
            this.cbImg.CheckOnClick = true;
            this.cbImg.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbImg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cbImg.Name = "cbImg";
            this.cbImg.ShowShortcutKeys = false;
            this.cbImg.Size = new System.Drawing.Size(152, 22);
            this.cbImg.Text = "使用截图量距离";
            this.cbImg.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.ForeColor = System.Drawing.Color.Black;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.ShowShortcutKeys = false;
            this.btnHelp.Size = new System.Drawing.Size(152, 22);
            this.btnHelp.Text = "帮助...";
            this.btnHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnExit.Name = "btnExit";
            this.btnExit.ShowShortcutKeys = false;
            this.btnExit.Size = new System.Drawing.Size(152, 22);
            this.btnExit.Text = "退出";
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Main
            // 
            this.AcceptButton = this.btnCalc;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Fuchsia;
            this.ClientSize = new System.Drawing.Size(770, 465);
            this.Controls.Add(this.lstResult);
            this.Controls.Add(this.pnlDisp);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(770, 392);
            this.Name = "Main";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "弹弹堂计算器";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Magenta;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.form_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.numWind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDist)).EndInit();
            this.pnlDisp.ResumeLayout(false);
            this.pnlDisp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBar)).EndInit();
            this.cmBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.NumericUpDown numWind;
        private System.Windows.Forms.NumericUpDown numDist;
        private System.Windows.Forms.ListBox lstResult;
        private System.Windows.Forms.ContextMenuStrip cmList;
        private System.Windows.Forms.Panel pnlDisp;
        private System.Windows.Forms.PictureBox pbBar;
        private System.Windows.Forms.ContextMenuStrip cmBar;
        private System.Windows.Forms.ToolStripMenuItem cbAcc;
        private System.Windows.Forms.ToolStripMenuItem cbLine;
        private System.Windows.Forms.ToolStripMenuItem cbImg;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem btnHelp;
        private System.Windows.Forms.ToolStripMenuItem btnExit;
    }
}

