﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace Fedora
{
    public partial class Main : Form
    {
        double unitWidth = 100;
        //double unitWidth = 6.4;
        Graphics gc;
        bool dist = true;
        Point me, he;
        Image lastImg;

        String savePath = System.Windows.Forms.Application.StartupPath + @"\fedora.dat";
        CfgMgr<Config> cfg;
        Assembly asm;

        bool dg1, dg2;
        Point dp1, dp2;

        IntPtr wd;

        public static int WM_KEYDOWN = 0x0100;
        public static int WM_KEYUP = 0x0101;
        public static int WM_CHAR = 0x0102;

        [DllImport("user32.dll")]
        public static extern IntPtr WindowFromPoint(int xPoint, int yPoint);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, object lParam);

        public Main()
        {
            InitializeComponent();
            // 定位模式
            Hotkey.Regist(this.Handle, HotkeyModifiers.NONE, Keys.Escape, new Hotkey.HotKeyCallBackHanlder(distMode));
            // +10距离
            Hotkey.Regist(this.Handle, HotkeyModifiers.NONE, Keys.Add, new Hotkey.HotKeyCallBackHanlder(delegate()
            {
                if (numDist.Value > 30) numDist.Value = 30;
                numDist.Value = numDist.Value + 10;
                he.X = he.X > me.X ? (me.X + (int)((double)numDist.Value * unitWidth)) : (me.X - (int)((double)numDist.Value * unitWidth));
                calcOnly();
            }));
            // 风速变向
            Hotkey.Regist(this.Handle, HotkeyModifiers.NONE, Keys.Multiply, new Hotkey.HotKeyCallBackHanlder(delegate()
            {
                numWind.Value = 0 - numWind.Value;
                calcOnly();
            }));
            // 自动力度
            Hotkey.Regist(this.Handle, HotkeyModifiers.NONE, Keys.Home, new Hotkey.HotKeyCallBackHanlder(delegate()
            {
                if (lstResult.SelectedIndex < 0) return;
                wd = WindowFromPoint(Cursor.Position.X, Cursor.Position.Y);
                Formula sf = (Formula)lstResult.SelectedItem;
                if (sf.Power <= 5) return;
                int tm = (int)(sf.Power / 0.02533 + 52.7122);
                SendMessage(wd, WM_KEYDOWN, new IntPtr(0x20), 0x1e0001);
                Thread.Sleep(tm);
                SendMessage(wd, WM_KEYUP, new IntPtr(0x20), 0xc01e0001);
            }));
            Hotkey.Regist(this.Handle, HotkeyModifiers.NONE, Keys.End, new Hotkey.HotKeyCallBackHanlder(delegate()
            {
                SendMessage(wd, WM_KEYUP, new IntPtr(0x20), 0xc01e0001);
            }));
            loadConfig();
            resizeList();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            Hotkey.ProcessHotKey(m);
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            he.X = he.X > me.X ? (me.X + (int)((double)numDist.Value * unitWidth)) : (me.X - (int)((double)numDist.Value * unitWidth));
            calcOnly();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1、按下Esc，输入风力(向右正，向左负)\n"
                + "2、在中间区域用鼠标左键/右键标出自己/敌人位置\n"
                + "3、再次按下Esc计算结果\n"
                + "4、如果手动输入距离，必须按计算按钮(回车)计算结果\n"
                + "5、小键盘+号距离加10，*号变换风向\n"
                + "6、选中一个算法，使用U/D键上下移动\n   右键菜单选择是否显示\n"
                + "7、选中一个算法，按Home自动发射", "By Choso");
        }

        #region 计算与绘制
        /// <summary>
        /// 计算
        /// </summary>
        private void calcAll()
        {
            this.SuspendLayout();
            gc = this.CreateGraphics();
            if (!cbImg.Checked)
            {
                gc.Clear(Color.Khaki);
            }
            else
            {
                gc.DrawImage(lastImg, 0, 0);
            }
            gc.FillRectangle(Brushes.Red, 0, this.Height - 50, this.Width, this.Height - 50);
            gc.DrawRectangle(Pens.Green, me.X - 20, me.Y - 20, 40, 40);
            gc.DrawRectangle(Pens.Green, me.X - 21, me.Y - 21, 42, 42);
            gc.DrawRectangle(Pens.Green, me.X - 22, me.Y - 22, 44, 44);
            gc.DrawRectangle(Pens.Red, he.X - 20, he.Y - 20, 40, 40);
            gc.DrawRectangle(Pens.Red, he.X - 21, he.Y - 21, 42, 42);
            gc.DrawRectangle(Pens.Red, he.X - 22, he.Y - 22, 44, 44);

            numDist.Value = (decimal)(Math.Abs(he.X - me.X) / unitWidth);
            drawPoly();
            lstResult.Refresh();
            this.ResumeLayout();
        }

        /// <summary>
        /// 计算角度和绘制抛物线
        /// </summary>
        private void drawPoly()
        {
            if (me.X == 0 || he.X == 0) return;
            List<Point> points = new List<Point>();

            for (int i = 0; i < lstResult.Items.Count; i++)
            {
                Formula fm = (Formula)lstResult.Items[i];

                double dispWidth = calcCore(fm) * unitWidth;
                if (cbLine.Checked && fm.isValid())
                {
                    int y, dx;
                    dx = Math.Abs(he.X - me.X);
                    bool t = he.X > me.X;
                    double angle = fm.Angle;
                    double radian = angle * Math.PI / 180;
                    double pa = -Math.Tan(radian) / dispWidth;
                    double pb = Math.Tan(radian);
                    points.Clear();
                    for (int x = 0; x <= dx; x++)
                    {
                        y = (int)(pa * x * x + pb * x);
                        if (y < 2000 && y > -2000)
                        {
                            points.Add(new Point(t ? (me.X + x) : (me.X - x), me.Y - y));
                        }
                    }
                    if (points.Count > 3)
                    {
                        gc.DrawLines(new Pen(fm.Color, 2), points.ToArray());
                    }
                }
            }
        }

        private void calcOnly()
        {
            for (int i = 0; i < lstResult.Items.Count; i++)
            {
                Formula fm = (Formula)lstResult.Items[i];
                calcCore(fm);
            }
            lstResult.Refresh();
        }

        /// <summary>
        /// 寻找最佳角度
        /// </summary>
        /// <param name="fm"></param>
        /// <returns></returns>
        private double calcCore(Formula fm)
        {
            double virtualDist = (double)numDist.Value;
            double wind = (double)numWind.Value;
            double extr = he.Y < me.Y ? 1 : -1;
            double delt = 100;
            wind = he.X > me.X ? wind : 0 - wind;
            int max = 1000;
            do
            {
                fm.calc(virtualDist, wind);
                if (!cbAcc.Checked)
                {
                    break;
                }
                delt = calcHdel(fm, virtualDist);
                extr = delt > 0 ? -0.1 : 0.1;
                virtualDist += extr;
                max--;
            }
            while (Math.Abs(delt) > Math.Tan(fm.Angle * Math.PI / 180) && max > 0);
            // 使用正切控制精度
            return virtualDist;
        }

        /// <summary>
        /// 计算抛物线与目标点高度差
        /// </summary>
        /// <param name="fm"></param>
        /// <param name="dist"></param>
        /// <returns></returns>
        private double calcHdel(Formula fm, double dist)
        {
            dist *= unitWidth;
            double angle = fm.Angle;
            double radian = angle * Math.PI / 180;
            double pa = -Math.Tan(radian) / dist;
            double pb = Math.Tan(radian);
            double x = Math.Abs(he.X - me.X);
            return (pa * x * x + pb * x) - (me.Y - he.Y);
        }

        /// <summary>
        /// unused
        /// </summary>
        /// <param name="degree"></param>
        /// <param name="cr"></param>
        /// <returns></returns>
        private double drawPath(double degree, Color cr)
        {
            int funcY, destX, flatX;
            double radian = degree * Math.PI / 180;
            bool AD = he.X > me.X;
            destX = Math.Abs(he.X - me.X);
            flatX = (int)(Math.Tan(radian) * destX * destX / (Math.Tan(radian) * destX + he.Y - me.Y));
            double paramA = -Math.Tan(radian) / flatX;
            double paramB = Math.Tan(radian);
            double topY = -paramB * paramB / paramA / 4;
            double v = Math.Sqrt(2 * topY / Math.Sin(radian) / Math.Sin(radian));
            double power = 1;//getPower(v);
            List<Point> xpoints = new List<Point>();
            for (int x = 0; x <= destX; x++)
            {
                funcY = (int)(paramA * x * x + paramB * x);
                if (funcY < 2000 && funcY > -2000)
                {
                    xpoints.Add(new Point(AD ? (me.X + x) : (me.X - x), me.Y - funcY));
                }
            }
            gc.DrawLines(new Pen(cr, 2), xpoints.ToArray());
            return (power > 100 || power <= 0) ? -1 : power;
        }

        /// <summary>
        /// 进/出定位模式
        /// </summary>
        private void distMode()
        {
            if (dist)
            {
                if (!cbImg.Checked)
                {
                    this.TransparencyKey = Color.Khaki;
                }
                else
                {
                    Point p1 = this.PointToScreen(this.Location);
                    lastImg = new Bitmap(this.Width, this.Height);
                    gc = Graphics.FromImage(lastImg);
                    gc.CopyFromScreen(p1.X, p1.Y, 0, 0, this.Size);
                }
                this.Activate();
                numWind.Focus();
                calcAll();
            }
            else
            {
                calcAll();
                this.TransparencyKey = Color.Magenta;
                gc = this.CreateGraphics();
                gc.Clear(Color.Magenta);
                btnCalc.Focus();
            }
            dist = !dist;
            cbImg.Enabled = dist;
        }

        private void form_MouseClick(object sender, MouseEventArgs e)
        {
            if (!dist)
            {
                if (e.Button == MouseButtons.Left) me = e.Location;
                else he = e.Location;
                calcAll();
            }
        }
        #endregion

        #region 算法列表的显示和排序
        private void lstResult_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)this.Font.Size * 2 + 15;
        }

        private void lstResult_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (e.Index >= 0)
            {
                Formula fml = (Formula)lstResult.Items[e.Index];
                Font ft = new Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                e.Graphics.DrawString(String.Format("角{0:N1}", fml.Angle), ft, new SolidBrush(fml.Color), e.Bounds.Left, e.Bounds.Top);
                e.Graphics.DrawString(String.Format("力{0:N1}", fml.Power), ft, new SolidBrush(fml.Color), e.Bounds.Left, e.Bounds.Top + this.Font.Size + 3);
            }
        }
        private void resizeList()
        {
            lstResult.Height = (int)(this.Font.Size * 2 + 15) * lstResult.Items.Count + 5;
        }
        /// <summary>
        /// 方向键改变算法显示顺序
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstResult_KeyDown(object sender, KeyEventArgs e)
        {
            int idx = lstResult.SelectedIndex;
            if (idx < 0) return;
            if (e.KeyCode == Keys.U && idx > 0)
            {
                Object t = lstResult.Items[idx];
                lstResult.Items[idx] = lstResult.Items[idx - 1];
                lstResult.Items[idx - 1] = t;
                lstResult.SelectedIndex--;
            }
            else if (e.KeyCode == Keys.D && idx < lstResult.Items.Count - 1)
            {
                Object t = lstResult.Items[idx];
                lstResult.Items[idx] = lstResult.Items[idx + 1];
                lstResult.Items[idx + 1] = t;
                lstResult.SelectedIndex++;
            }
        }
        #endregion

        #region 读取/保存
        /// <summary>
        /// 退出时保存设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            cfg.Config = new Fedora.Config();
            cfg.Config.accu = cbAcc.Checked;
            cfg.Config.line = cbLine.Checked;
            cfg.Config.img = cbImg.Checked;
            cfg.Config.bar = pnlDisp.Location;
            cfg.Config.lst = lstResult.Location;
            cfg.Config.selected = lstResult.SelectedItem.GetType().FullName;
            foreach (Object obj in lstResult.Items)
            {
                cfg.Config.formula.Add(obj.GetType().FullName);
            }
            cfg.Save();
        }

        /// <summary>
        /// 读取设置文件
        /// </summary>
        private void loadConfig()
        {
            cfg = new CfgMgr<Config>(savePath);
            asm = Assembly.GetCallingAssembly();
            List<string> fms = getFormulas();

            foreach (string fmn in fms)
            {
                Formula ff = (Formula)asm.CreateInstance(fmn);
                ToolStripMenuItem tsm = new ToolStripMenuItem(ff.Name);
                tsm.ForeColor = ff.Color;
                tsm.BackColor = Color.Black;
                tsm.CheckOnClick = true;
                tsm.Tag = fmn;
                tsm.Click += new EventHandler(tsm_Click);
                cmList.Items.Add(tsm);
            }

            if (cfg.Load())
            {
                cbAcc.Checked = cfg.Config.accu;
                cbLine.Checked = cfg.Config.line;
                cbImg.Checked = cfg.Config.img;

                foreach (string fn in cfg.Config.formula)
                {
                    lstResult.Items.Add(asm.CreateInstance(fn));
                    ((ToolStripMenuItem)cmList.Items[fms.IndexOf(fn)]).Checked = true;
                    if (cfg.Config.selected == fn)
                    {
                        lstResult.SelectedIndex = lstResult.Items.Count - 1;
                    }
                }

                pnlDisp.Location = cfg.Config.bar;
                lstResult.Location = cfg.Config.lst;
            }
            else
            {
                for (int i = 0; i < fms.Count; i++)
                {
                    lstResult.Items.Add(asm.CreateInstance(fms[i]));
                    ((ToolStripMenuItem)cmList.Items[i]).Checked = true;
                }
                lstResult.SelectedIndex = 0;
            }
        }

        void tsm_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsm = (ToolStripMenuItem)sender;
            string fn = (string)tsm.Tag;
            if (tsm.Checked)
            {
                lstResult.Items.Add(asm.CreateInstance(fn));
            }
            else
            {
                for (int i = 0; i < lstResult.Items.Count; i++)
                {
                    if (lstResult.Items[i].GetType().FullName == fn)
                    {
                        lstResult.Items.RemoveAt(i);
                        break;
                    }
                }
            }
            resizeList();
        }

        /// <summary>
        /// 使用反射获取所有的算法类
        /// </summary>
        /// <returns></returns>
        private List<string> getFormulas()
        {
            List<string> fms = new List<string>();
            Type[] types = asm.GetTypes();
            Type formulaType = typeof(Formula);
            foreach (Type tp in types)
            {
                if (tp.IsSubclassOf(formulaType))
                {
                    fms.Add(tp.FullName);
                }
            }
            return fms;
        }
        #endregion

        #region 输入区变色
        private void numWind_Enter(object sender, EventArgs e)
        {
            NumericUpDown nu = (NumericUpDown)sender;
            nu.BackColor = Color.GreenYellow;
            nu.Select(0, 4);
        }

        private void numWind_Leave(object sender, EventArgs e)
        {
            NumericUpDown nu = (NumericUpDown)sender;
            nu.BackColor = Color.Honeydew;
        }
        #endregion

        #region 拖动事件
        private void pbBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (dg1)
            {
                pnlDisp.Location = new Point(Cursor.Position.X - dp1.X, Cursor.Position.Y - dp1.Y);
            }
        }

        private void pbBar_MouseDown(object sender, MouseEventArgs e)
        {
            dg1 = true;
            dp1 = new Point(Cursor.Position.X - pnlDisp.Location.X, Cursor.Position.Y - pnlDisp.Location.Y);

        }

        private void pbBar_MouseUp(object sender, MouseEventArgs e)
        {
            dg1 = false;
        }

        private void lstResult_MouseUp(object sender, MouseEventArgs e)
        {
            dg2 = false;
        }

        private void lstResult_MouseDown(object sender, MouseEventArgs e)
        {
            dg2 = true;
            dp2 = new Point(Cursor.Position.X - lstResult.Location.X, Cursor.Position.Y - lstResult.Location.Y);
        }

        private void lstResult_MouseMove(object sender, MouseEventArgs e)
        {
            if (dg2)
            {
                lstResult.Location = new Point(Cursor.Position.X - dp2.X, Cursor.Position.Y - dp2.Y);
            }
        }
        #endregion
    }

    [Serializable]
    class Config
    {
        public bool accu;
        public bool line;
        public bool img;
        public List<string> formula;
        public Point bar;
        public Point lst;
        public string selected;
        public Config()
        {
            formula = new List<string>();
        }
    }
}
