﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Grid
{
    public partial class ServerForm : Form
    {
        WebServer webServ;
        GridTaskServer server;
        Bitmap bmp = new Bitmap(400, 300);
        Brush drawBrush;
        int rat;

        public ServerForm()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            this.Icon = Grid.Properties.Resources.Server;
        }

        private void ServerForm_Load(object sender, EventArgs e)
        {
            server = new GridTaskServer();
            server.AddNodeEvent += new HandleAddNode(server_AddNodeEvent);
            server.RemoveNodeEvent += new HandleRemoveNode(server_RemoveNodeEvent);
            server.TaskChangeEvent += new HandleTaskChange(server_TaskChangeEvent);
            server.NewMessageEvent += new HandleNewMessage(server_NewMessageEvent);
            txtPort.Text = "" + GridTool.ServerPort;
        }

        void server_NewMessageEvent(string message)
        {
            log(message);
        }

        void server_TaskChangeEvent(string node, GridTask task)
        {
            if (task.TaskState == 2)
            {
                refreshStatus();
                drawOne(task);
            }
        }

        void server_RemoveNodeEvent(string node)
        {
            if (lstClient.Items.Contains(node))
            {
                lstClient.Items.Remove(node);
                refreshStatus();
            }
        }

        void server_AddNodeEvent(string node)
        {
            if (!lstClient.Items.Contains(node))
            {
                lstClient.Items.Add(node);
                refreshStatus();
            }
        }

        public void log(string txt)
        {
            string fulltxt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + txt;
            txtLog.Text = fulltxt + "\r\n" + txtLog.Text;
            // LOG FILE
            if (txtLog.Text.Length > 32000)
            {
                txtLog.Text = txtLog.Text.Substring(0, 30000);
            }
        }

        private void drawTaskMap()
        {
            Graphics gc = Graphics.FromImage(bmp);
            gc.Clear(Color.White);
            GridTask[] taskList = (from entry in server.getTaskMap()
                                   orderby entry.Value.TaskIndex ascending
                                   select entry.Value).ToArray<GridTask>();
            rat = server.getTotalTask() / 1200;
            if (server.getTotalTask() % 1200 > 0)
            {
                rat++;
            }
            drawBrush = new SolidBrush(Color.FromArgb(255 / rat, 0, 255, 0));
            foreach (GridTask task in taskList)
            {
                int x = (task.TaskIndex / rat) % 40;
                int y = (task.TaskIndex / rat) / 40;
                gc.DrawRectangle(Pens.Black, x * 10, y * 10, 8, 8);
                if (task.TaskState == 2)
                {
                    gc.FillRectangle(drawBrush, x * 10, y * 10, 9, 9);
                }
            }
            gc.Dispose();
            pbGraph.Image = bmp;
        }

        private void drawOne(GridTask task)
        {
            Graphics gc = Graphics.FromImage(bmp);
            int x = (task.TaskIndex / rat) % 40;
            int y = (task.TaskIndex / rat) / 40;
            gc.FillRectangle(drawBrush, x * 10, y * 10, 9, 9);
            gc.Dispose();
            pbGraph.Image = bmp;
        }

        private void startHttpListen()
        {
            webServ = new WebServer(Convert.ToInt32(txtPort.Text));
            webServ.OnRequestEvent += new WebServer.HandleRequest(webServ_OnRequestEvent);
            webServ.OnMessageEvent += new HandleNewMessage(webServ_OnMessageEvent);
            webServ.Start();
        }

        void webServ_OnMessageEvent(string message)
        {
            log(message);
        }

        byte[] webServ_OnRequestEvent(byte[] param)
        {
            if (param != null && param.Length > 7)
            {
                try
                {
                    GridParam p = (GridParam)GridTool.UnSerialize(param);
                    if (p != null)
                    {
                        server.handleParam(p);
                        return GridTool.Serialize(p);
                    }
                }
                catch (Exception e)
                {
                    log("WEB-EX:" + e.Message);
                }
            }
            return null;
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (server.isFinished())
            {
                tmr.Enabled = false;
                log("任务全部结束");
            }
            else
            {
                server.refreshClient();
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            txtPort.Enabled = false;
            btnStart.Enabled = false;
            btnStart.Text = "运行中";
            server.init();
            Thread srvThd = new Thread(startHttpListen);
            srvThd.IsBackground = true;
            srvThd.Start();
            pbGraph.Image = bmp;
            drawTaskMap();
            tmr.Enabled = true;
            refreshStatus();
        }

        private void refreshStatus()
        {
            ts2.Text = "" + lstClient.Items.Count;
            ts4.Text = server.getFinishTask() + "/" + server.getTotalTask();
            ts5.Text = server.getAveragTime() + "s";
            if (server.getAveragTime() > 0 && lstClient.Items.Count > 0)
            {
                long hours = (server.getTotalTask() - server.getFinishTask()) * server.getAveragTime() / 3600 / lstClient.Items.Count;
                ts7.Text = (hours >= 24 ? (hours / 24 + "天") : "") + (hours % 24 > 0 ? (hours % 24 + "小时") : "");
            }
            else
            {
                ts7.Text = "0";
            }
        }
    }
}
