﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.Net.Sockets;

namespace Grid
{
    public partial class NodeForm : Form
    {
        string myName;

        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        public GridTaskNode TaskNode;

        public NodeForm()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            this.Icon = Grid.Properties.Resources.Grid;
        }

        private void NodeForm_Load(object sender, EventArgs e)
        {
            myName = Dns.GetHostName() + ":" + Process.GetCurrentProcess().Id;
            txtPort.Text = "" + GridTool.ServerPort;
            txtServer.Text = GridTool.ServerIP;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            btnLogin.Enabled = false;
            txtPort.Enabled = false;
            txtServer.Enabled = false;
            pg.Visible = true;
            TaskNode = new GridTaskNode(myName);
            TaskNode.NewMessageEvent += new HandleNewMessage(TaskNode_NewMessageEvent);
            TaskNode.TaskChangeEvent += new HandleTaskChange(TaskNode_TaskChangeEvent);
            var t = new Thread(new ThreadStart(initTaskNode));
            t.IsBackground = true;
            t.Start();
        }

        void TaskNode_TaskChangeEvent(string node, GridTask task)
        {
            GridParam pm = new GridParam();
            pm.Method = "finishTask";
            pm.Param = new List<object>();
            pm.Param.Add(myName);
            pm.Param.Add(TaskNode.NowTask);
            SendData(pm);
            startNewTask();
        }

        void TaskNode_NewMessageEvent(string message)
        {
            log(message);
        }

        void initTaskNode()
        {
            GridParam pm = new GridParam();
            pm.Method = "Login";
            pm.Param = new List<object>();
            pm.Param.Add(myName);
            log("登录服务器...");
            SendData(pm);
            startNewTask();
        }

        void startNewTask()
        {
            var t = new Thread(new ThreadStart(startNewTaskCore));
            t.IsBackground = true;
            t.Start();
        }

        void startNewTaskCore()
        {
            GridParam pm = new GridParam();
            pm.Method = "getTask";
            pm.Param = new List<object>();
            pm.Param.Add(myName);
            GridParam result = SendData(pm);
            if (result != null && result.Result != null)
            {
                TaskNode.setTask((GridTask)result.Result);
                TaskNode.run();
            }
            else
            {
                pg.Visible = false;
                tmr.Enabled = false;
                log("任务已结束");
            }
        }

        private void log(string txt)
        {
            txtLog.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n  " + txt + "\r\n" + txtLog.Text;
            if (txtLog.Text.Length > 32000)
            {
                txtLog.Text = txtLog.Text.Substring(0, 30000);
            }
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (TaskNode != null && TaskNode.NowTask != null)
            {
                GridParam pm = new GridParam();
                pm.Method = "report";
                pm.Param = new List<object>();
                pm.Param.Add(TaskNode.NowTask);
                SendData(pm);
            }
        }

        private GridParam SendData(GridParam pm)
        {
            TcpClient tcpClient = new TcpClient();
            try
            {
                tcpClient.Connect(IPAddress.Parse(txtServer.Text), Convert.ToInt32(txtPort.Text));
                NetworkStream ns = tcpClient.GetStream();
                byte[] send = GridTool.Serialize(pm);
                try
                {
                    ns.Write(send, 0, send.Length);
                }
                catch (Exception e)
                {
                    Console.WriteLine("tcp-client-exp1:" + e.Message);
                }
                byte[] bytes = new byte[102400];
                int recv = 0;
                try
                {
                    if ((recv = ns.Read(bytes, 0, 102400)) == 0)
                    {
                        Console.WriteLine("disconnected");
                    }
                    return (GridParam)GridTool.UnSerialize(bytes);
                }
                catch (Exception e)
                {
                    Console.WriteLine("tcp-client-exp2:" + e.Message);
                }
                ns.Close();
                tcpClient.Close();
            }
            catch (Exception e)
            {
                log("EXP:" + e.Message);
            }
            return null;
        }
    }
}
