﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Grid
{
    public class GridTaskNode
    {
        public event HandleTaskChange TaskChangeEvent;
        public event HandleNewMessage NewMessageEvent;

        const string EX_SSE = "ops_sse2.exe";
        const string EX_AVX = "ops_avx.exe";
        const string FL_INP = "inp.txt";
        const string FL_RET = "ret.txt";

        public GridTask NowTask;
        public string NodeName;
        string password;

        int allCalc;
        int currCalc;

        System.Diagnostics.Process prc;

        public GridTaskNode(string nodeName)
        {
            NodeName = nodeName;
        }

        public void setTask(GridTask task)
        {

            NowTask = task;
            password = null;
            currCalc = 0;
            allCalc = (int)Math.Pow(36, (int)task.ExtData["calclen"]);
            NowTask.Node = NodeName;
            NowTask.TaskProgress = 0;
            NowTask.TaskState = 1;
        }
        public void run()
        {
            Stopwatch watcher = new Stopwatch();
            NewMessageEvent(String.Format("开始任务片 {0}({1})", NowTask.TaskId, allCalc));
            watcher.Start();
            try
            {
                runCore((NowTask.ExtData["pre"] as string), (int)NowTask.ExtData["calclen"]);
            }
            catch (Exception e)
            {
                NewMessageEvent("RUN-EXP" + e.Message);
                return;
            }
            watcher.Stop();
            NewMessageEvent(String.Format("完成任务片 {0} 用时{1}s", NowTask.TaskId, (watcher.ElapsedMilliseconds / 1000)));
            currCalc = allCalc;

            if (password != null)
            {
                NowTask.ExtData["bingo"] = "Y";
                NowTask.ExtData["password"] = password;
            }

            NowTask.Node = NodeName;
            NowTask.TaskProgress = 100;
            NowTask.TaskState = 2;
            TaskChangeEvent(NodeName, NowTask);
        }
        private void runCore(string pre, int len)
        {
            string user = (string)NowTask.ExtData["user"];
            string inpFile = Environment.CurrentDirectory + "\\" + FL_INP;
            string retFile = Environment.CurrentDirectory + "\\" + FL_RET;
            if (File.Exists(inpFile)) File.Delete(inpFile);
            if (File.Exists(retFile)) File.Delete(retFile);
            File.WriteAllText(inpFile, user + pre.ToUpper() + ":" + NowTask.ExtData["hash"] + ":GridTask");
            prc = new Process();
            prc.StartInfo = new ProcessStartInfo();
            prc.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
            prc.StartInfo.FileName = Environment.CurrentDirectory + "\\" + EX_SSE;
            prc.StartInfo.Arguments = String.Format("--hashlist={0} --min={1} --max={2} --results={3} --first_symbol_charset={4} --charset={4}", FL_INP, 1, Math.Max(2, len), FL_RET, GridTaskServer.ALL_CHAR);
            prc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            prc.StartInfo.RedirectStandardInput = true;
            prc.StartInfo.RedirectStandardOutput = true;
            prc.StartInfo.RedirectStandardError = true;
            prc.StartInfo.UseShellExecute = false;
            prc.StartInfo.CreateNoWindow = true;
            prc.Start();
            prc.WaitForExit();
            string[] ret = File.ReadAllText(retFile).Split(':');
            if (ret[1] != "?")
            {
                password = pre + ret[1];
                NewMessageEvent(String.Format("找到密码 【{0}】", password));
            }
        }

        public int getAllCalc()
        {
            return allCalc;
        }
        public int getCurrCalc()
        {
            return currCalc;
        }
    }
}
