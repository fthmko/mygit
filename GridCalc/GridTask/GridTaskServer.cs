﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Grid
{
    public class GridTaskServer
    {
        public const string ALL_CHAR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.$#/";

        private object tasklock = new object();
        private object clientlock = new object();
        private object loglock = new object();

        List<string> Clients;
        string TaskName = "AVX";
        string GS_USER = "";
        string GS_HASH = "";
        int GS_PASLEN = 8;
        int GS_TASKSIZE = 3;

        Queue<GridTask> tasks;
        Queue<GridTask> vipTasks;
        MapDict<string, GridTask> taskMap = null;
        MapDict<string, int> clientMap = null;
        bool isFinish = false;
        int TotalTasks;
        int FinishedTasks;
        long allTime;

        public event HandleAddNode AddNodeEvent;
        public event HandleRemoveNode RemoveNodeEvent;
        public event HandleTaskChange TaskChangeEvent;
        public event HandleNewMessage NewMessageEvent;

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(string section, string key, int def, string filePath);

        public void handleParam(GridParam param)
        {
            Type tp = this.GetType();
            MethodInfo mi = tp.GetMethod(param.Method);
            if (mi != null)
            {
                object ret = mi.Invoke(this, param.Param.ToArray());
                param.Result = ret;
            }
        }

        public void Login(string s)
        {
            lock (clientlock)
            {
                log(String.Format("节点 {0} 连接了", s));
                if (!Clients.Contains(s))
                {
                    Clients.Add(s);
                    clientMap[s] = 0;
                    AddNodeEvent(s);
                }
            }
        }

        public void Logout(string s)
        {
            lock (clientlock)
            {
                if (Clients.Contains(s))
                {
                    log(String.Format("节点 {0} 断开了", s));
                    Clients.Remove(s);
                    clientMap.Remove(s);

                    var ltask = (from entry in taskMap
                                 where entry.Value.Node == s && entry.Value.TaskState != 2
                                 select entry.Value);

                    if (ltask.Count() > 0)
                    {
                        var rtask = ltask.First();
                        log(String.Format("任务片 {0} 中断", rtask.TaskId));
                        vipTasks.Enqueue(rtask);
                        rtask.Node = null;
                        rtask.TaskProgress = 0;
                        rtask.TaskState = 0;
                    }
                    RemoveNodeEvent(s);
                }
            }
        }

        public void SaveResult(string content)
        {
            File.WriteAllText(Environment.CurrentDirectory + "\\" + TaskName + "_result.txt", content, Encoding.UTF8);
        }

        private void saveState()
        {
            string stgName = Environment.CurrentDirectory + "\\" + TaskName + "_save.bin";
            try
            {
                if (File.Exists(stgName + ".bak")) File.Delete(stgName + ".bak");
                if (File.Exists(stgName)) File.Move(stgName, stgName + ".bak");
                FileStream fs = new FileStream(stgName, FileMode.CreateNew);
                IFormatter saver = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                saver.Serialize(fs, taskMap);
                fs.Close();
            }
            catch
            {
            }
        }

        private void loadState()
        {
            string stgName = Environment.CurrentDirectory + "\\" + TaskName + "_save.bin";
            try
            {
                if (!File.Exists(stgName)) return;
                FileStream fs = new FileStream(stgName, FileMode.Open);
                IFormatter loader = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                taskMap = (MapDict<string, GridTask>)loader.Deserialize(fs);
                fs.Close();
            }
            catch (Exception ex)
            {
                log("读取存档异常：" + ex.Message);
            }
        }

        public int getTotalTask()
        {
            return TotalTasks;
        }

        public int getFinishTask()
        {
            return FinishedTasks;
        }

        public long getAveragTime()
        {
            return FinishedTasks > 0 ? (allTime / FinishedTasks) : 0;
        }

        private string readConfig(string Key, string def = "")
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString("Default", Key, def, temp, 255, Environment.CurrentDirectory + "\\GridTask.ini");
            return temp.ToString();
        }

        private int readConfig(string Key, int def)
        {
            StringBuilder temp = new StringBuilder(255);
            return GetPrivateProfileInt("Default", Key, def, Environment.CurrentDirectory + "\\GridTask.ini");
        }

        public void init()
        {
            lock (tasklock)
            {
                GS_USER = readConfig("user").ToUpper();
                GS_HASH = readConfig("hash").ToUpper();
                GS_PASLEN = readConfig("password_max_len", 8);
                GS_TASKSIZE = readConfig("task_split", 3);
                allTime = 0;
                FinishedTasks = 0;
                log(String.Format("任务: {0}/{1}, {2}位", GS_USER, GS_HASH, GS_PASLEN));

                loadState();
                tasks = new Queue<GridTask>(10000);
                vipTasks = new Queue<GridTask>();
                Clients = new List<string>();
                clientMap = new MapDict<string, int>();
                if (taskMap == null)
                {
                    taskMap = new MapDict<string, GridTask>(10000);
                    splitTask("", GS_TASKSIZE);
                    // FIX
                    //GridTask tmp = tasks.Dequeue();
                    //while (tmp.TaskId.CompareTo("PR0") < 0)
                    //{
                    //    tmp.TaskState = 2;
                    //    tmp.TimeFinish = tmp.TimeStart.AddSeconds(50);
                    //    tmp.TaskProgress = 100;
                    //    FinishedTasks++;
                    //    tmp = tasks.Dequeue();
                    //}
                    //saveState();
                    // FIX
                }
                else
                {
                    log("读取存档成功");
                    string[] keys = taskMap.Keys.ToArray();
                    foreach (string key in keys)
                    {
                        taskMap[key].Node = null;
                        if (taskMap[key].TaskState != 2)
                        {
                            taskMap[key].TaskState = 0;
                            taskMap[key].TaskProgress = 0;
                            tasks.Enqueue(taskMap[key]);
                        }
                        else
                        {
                            allTime += (long)(taskMap[key].TimeFinish - taskMap[key].TimeStart).TotalSeconds;
                            FinishedTasks++;
                        }
                    }
                }
                TotalTasks = taskMap.Count;
                log(String.Format("剩余任务片 {0}, 每片 {1}", TotalTasks, Math.Pow(ALL_CHAR.Length, GS_PASLEN - GS_TASKSIZE)));
                log(String.Format("任务进度 {0}/{1}{2}", FinishedTasks, taskMap.Count, (FinishedTasks > 0 ? (" (平均时间" + (allTime / FinishedTasks) + "s)") : "")));
            }
        }

        private void log(string txt)
        {
            lock (loglock)
            {
                NewMessageEvent(txt);
            }
        }

        private void splitTask(string pre, int count)
        {
            if (count == 1)
            {
                foreach (char c in ALL_CHAR.ToCharArray())
                {
                    GridTask task = new GridTask();
                    task.TaskId = pre + c;
                    task.TaskIndex = tasks.Count;
                    task.TaskName = TaskName;
                    tasks.Enqueue(task);
                    taskMap[task.TaskId] = task;
                }
            }
            else
            {
                foreach (char c in ALL_CHAR.ToCharArray())
                {
                    splitTask(pre + c, count - 1);
                }
            }
        }

        public string getTaskName()
        {
            return TaskName;
        }

        public GridTask getTask(string node)
        {
            try
            {
                lock (tasklock)
                {
                    if (Clients.Contains(node))
                    {
                        GridTask task = null;
                        if (vipTasks.Count > 0)
                        {
                            task = vipTasks.Dequeue();
                        }
                        else if (tasks.Count > 0)
                        {
                            task = tasks.Dequeue();
                        }
                        if (task != null)
                        {
                            task.ExtData["pre"] = task.TaskId;
                            task.ExtData["user"] = GS_USER;
                            task.ExtData["hash"] = GS_HASH;
                            task.ExtData["pwdlen"] = GS_PASLEN;
                            task.ExtData["calclen"] = GS_PASLEN - task.TaskId.Length;
                            task.TaskState = 1;
                            task.Node = node;
                            task.TimeStart = DateTime.Now;
                            log(String.Format("节点 {0} 领取任务片 {1} ", node, task.TaskId));
                            clientMap[node] = 0;
                            return task;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log("getTask-EXP：" + node + ex.Message);
            }
            log("getTask：" + node + ":no more task!");
            return null;
        }

        public void finishTask(string node, GridTask task)
        {
            lock (tasklock)
            {
                GridTask srvTask = taskMap[task.TaskId];
                srvTask.TaskProgress = 100;
                srvTask.TaskState = 2;
                srvTask.TimeFinish = DateTime.Now;
                long useTime = (long)(srvTask.TimeFinish - srvTask.TimeStart).TotalSeconds;
                allTime += useTime;
                FinishedTasks++;
                log(String.Format("节点 {0} 完成任务片 {1} 用时{2}s", node, task.TaskId, useTime));
                TaskChangeEvent(node, task);
                if (task.ExtData.ContainsKey("bingo"))
                {
                    string pwd = (string)task.ExtData["password"];
                    log(String.Format("节点 {0} 在任务片 {1} 找到密码 【{2}】", node, task.TaskId, pwd));
                    SaveResult("密码:【" + pwd + "】");
                    isFinish = true;
                    FinishedTasks = TotalTasks;
                    tasks.Clear();
                    vipTasks.Clear();
                }
                else
                {
                    if (FinishedTasks % 10 == 0)
                    {
                        saveState();
                    }
                }
            }
        }

        public void report(GridTask task)
        {
            lock (clientlock)
            {
                //log("Receive Report:" + task.Node);
                clientMap[task.Node] = 0;
                taskMap[task.TaskId].TaskProgress = task.TaskProgress;
            }
        }

        public void refreshClient()
        {
            lock (clientlock)
            {
                string[] clients = Clients.ToArray();
                foreach (string node in clients)
                {
                    if (clientMap.ContainsKey(node))
                    {
                        clientMap[node] += 1;
                        if (clientMap[node] > 3)
                        {
                            Logout(node);
                        }
                    }
                }
            }
        }

        public bool isFinished()
        {
            return isFinish;
        }
        public MapDict<string, GridTask> getTaskMap()
        {
            return taskMap;
        }

    }
}
