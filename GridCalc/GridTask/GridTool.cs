﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Net;
using System.Threading;

namespace Grid
{
    public class GridTool
    {
        public const int ServerPort = 80;
        public const string ServerIP = "10.13.34.145";

        public static object UnSerialize(byte[] src)
        {
            object obj = null;
            var sr = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                MemoryStream ms = new MemoryStream(src);
                obj = sr.Deserialize(ms);
            }
            catch (Exception e)
            {

            }
            return obj;
        }

        public static byte[] Serialize(object obj)
        {
            var sr = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                MemoryStream ms = new MemoryStream();
                sr.Serialize(ms, obj);
                return ms.ToArray();
            }
            catch (Exception e)
            {
            }
            return null;
        }

        public static string findText(string src, string reg, int group)
        {
            if (string.IsNullOrEmpty(src)) return null;
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return null;
            return mc[0].Groups[group].Value;
        }
    }

    [Serializable]
    public class GridParam
    {
        public string Method { get; set; }
        public List<object> Param { get; set; }
        public object Result { get; set; }
        public GridTask Task { get; set; }
    }

    [Serializable]
    public class GridTask
    {
        public string TaskId = null;
        public string TaskName = null;
        public string Node = null;
        public int TaskIndex = -1;
        public int TaskProgress = 0;
        public int TaskState = 0;
        public DateTime TimeStart = DateTime.Now;
        public DateTime TimeFinish = DateTime.Now;
        public MapDict<string, object> ExtData = new MapDict<string, object>();
    }

    [Serializable]
    public class MapDict<K, V> : Dictionary<K, V>
    {
        public new V this[K key]
        {
            get
            {
                if (this.ContainsKey(key))
                {
                    return base[key];
                }
                return default(V);
            }
            set
            {
                if (this.ContainsKey(key))
                {
                    base[key] = value;
                }
                else
                {
                    this.Add(key, value);
                }
            }
        }

        public MapDict()
        {
        }
        public MapDict(int capacity)
            : base(capacity)
        {
        }
        protected MapDict(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public MapDict(IDictionary<K, V> dictionary) : base(dictionary) { }
        public MapDict(IDictionary<K, V> dictionary, IEqualityComparer<K> comparer) : base(dictionary, comparer) { }
        public MapDict(IEqualityComparer<K> comparer) : base(comparer) { }
        public MapDict(int capacity, IEqualityComparer<K> comparer) : base(capacity, comparer) { }
    }


    public class WebServer
    {
        public delegate byte[] HandleRequest(byte[] param);
        public event HandleRequest OnRequestEvent;
        public event HandleNewMessage OnMessageEvent;
        TcpListener listener;

        public WebServer(int port)
        {
            IPAddress ipAddress = IPAddress.Any;
            listener = new TcpListener(ipAddress, port);

            ThreadPool.SetMaxThreads(50, 1000);
            ThreadPool.SetMinThreads(50, 50);
        }

        public void Start()
        {
            listener.Start();
            while (true)
            {
                try
                {
                    TcpClient tcpClient = listener.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(ProcessRequest, tcpClient);
                }
                catch (Exception ex)
                {
                    OnMessageEvent("TCP-EXP:" + ex.Message);
                    break;
                }
            }
        }

        public void Stop()
        {
            this.listener.Stop();
        }

        void ProcessRequest(object listenerContext)
        {
            var context = (TcpClient)listenerContext;
            NetworkStream ns = context.GetStream();
            int recv;
            byte[] bytes = new byte[102400];
            try
            {
                if ((recv = ns.Read(bytes, 0, 102400)) == 0)
                {
                    OnMessageEvent("disconnected");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("tcp-client-exp1:" + e.Message);
            }
            byte[] resp = OnRequestEvent(bytes);
            try
            {
                ns.Write(resp, 0, resp.Length);
            }
            catch (Exception e)
            {
                OnMessageEvent("tcp-client-exp2:" + e.Message);
            }
            ns.Close();
            context.Close();
        }
    }

    public delegate void HandleAddNode(string node);
    public delegate void HandleRemoveNode(string node);
    public delegate void HandleTaskChange(string node, GridTask task);
    public delegate void HandleNewMessage(string message);
}
