﻿namespace MergeQuery
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lstStatus = new System.Windows.Forms.CheckedListBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.txtSql = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgData = new System.Windows.Forms.DataGridView();
            this.txtCon = new System.Windows.Forms.TextBox();
            this.tmrTrans = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Execute = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Stop = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Commit = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Rollback = new System.Windows.Forms.ToolStripButton();
            this.btnExport = new System.Windows.Forms.ToolStripSplitButton();
            this.cbKo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cmbLimit = new System.Windows.Forms.ToolStripComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lstStatus);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(871, 538);
            this.splitContainer1.SplitterDistance = 117;
            this.splitContainer1.TabIndex = 1;
            // 
            // lstStatus
            // 
            this.lstStatus.BackColor = System.Drawing.Color.Silver;
            this.lstStatus.CausesValidation = false;
            this.lstStatus.CheckOnClick = true;
            this.lstStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstStatus.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lstStatus.ForeColor = System.Drawing.Color.Black;
            this.lstStatus.FormattingEnabled = true;
            this.lstStatus.Location = new System.Drawing.Point(0, 0);
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.Size = new System.Drawing.Size(117, 538);
            this.lstStatus.TabIndex = 0;
            this.lstStatus.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstStatus_ItemCheck);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.txtSql);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer3.Size = new System.Drawing.Size(750, 538);
            this.splitContainer3.SplitterDistance = 232;
            this.splitContainer3.TabIndex = 0;
            // 
            // txtSql
            // 
            this.txtSql.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSql.Font = new System.Drawing.Font("Consolas", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSql.HideSelection = false;
            this.txtSql.Location = new System.Drawing.Point(0, 0);
            this.txtSql.MaxLength = 327670;
            this.txtSql.Multiline = true;
            this.txtSql.Name = "txtSql";
            this.txtSql.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSql.Size = new System.Drawing.Size(750, 232);
            this.txtSql.TabIndex = 0;
            this.txtSql.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtSql_MouseDoubleClick);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgData);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtCon);
            this.splitContainer2.Size = new System.Drawing.Size(750, 302);
            this.splitContainer2.SplitterDistance = 215;
            this.splitContainer2.TabIndex = 0;
            // 
            // dgData
            // 
            this.dgData.AllowUserToAddRows = false;
            this.dgData.AllowUserToDeleteRows = false;
            this.dgData.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Honeydew;
            this.dgData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgData.Location = new System.Drawing.Point(0, 0);
            this.dgData.Name = "dgData";
            this.dgData.ReadOnly = true;
            this.dgData.RowHeadersWidth = 40;
            this.dgData.RowTemplate.Height = 18;
            this.dgData.Size = new System.Drawing.Size(750, 215);
            this.dgData.TabIndex = 0;
            this.dgData.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgData_RowPostPaint);
            // 
            // txtCon
            // 
            this.txtCon.BackColor = System.Drawing.Color.DimGray;
            this.txtCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCon.ForeColor = System.Drawing.Color.Gainsboro;
            this.txtCon.Location = new System.Drawing.Point(0, 0);
            this.txtCon.MaxLength = 100000;
            this.txtCon.Multiline = true;
            this.txtCon.Name = "txtCon";
            this.txtCon.ReadOnly = true;
            this.txtCon.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCon.Size = new System.Drawing.Size(750, 83);
            this.txtCon.TabIndex = 0;
            // 
            // tmrTrans
            // 
            this.tmrTrans.Enabled = true;
            this.tmrTrans.Interval = 50;
            this.tmrTrans.Tick += new System.EventHandler(this.tmrTrans_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Execute,
            this.toolStripButton_Stop,
            this.toolStripButton_Commit,
            this.toolStripButton_Rollback,
            this.btnExport,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.toolStripButton1,
            this.toolStripLabel1,
            this.cmbLimit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(871, 25);
            this.toolStrip1.TabIndex = 2;
            // 
            // toolStripButton_Execute
            // 
            this.toolStripButton_Execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Execute.Image = global::MergeQuery.Properties.Resources.FormRunHS;
            this.toolStripButton_Execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Execute.Name = "toolStripButton_Execute";
            this.toolStripButton_Execute.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Execute.Text = "执行";
            this.toolStripButton_Execute.Click += new System.EventHandler(this.toolStripButton_Execute_Click);
            // 
            // toolStripButton_Stop
            // 
            this.toolStripButton_Stop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Stop.Enabled = false;
            this.toolStripButton_Stop.Image = global::MergeQuery.Properties.Resources.cancel;
            this.toolStripButton_Stop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Stop.Name = "toolStripButton_Stop";
            this.toolStripButton_Stop.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Stop.Text = "中断";
            this.toolStripButton_Stop.Click += new System.EventHandler(this.toolStripButton_Stop_Click);
            // 
            // toolStripButton_Commit
            // 
            this.toolStripButton_Commit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Commit.Enabled = false;
            this.toolStripButton_Commit.Image = global::MergeQuery.Properties.Resources.commit;
            this.toolStripButton_Commit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Commit.Name = "toolStripButton_Commit";
            this.toolStripButton_Commit.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Commit.Text = "提交";
            this.toolStripButton_Commit.Click += new System.EventHandler(this.toolStripButton_Commit_Click);
            // 
            // toolStripButton_Rollback
            // 
            this.toolStripButton_Rollback.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Rollback.Enabled = false;
            this.toolStripButton_Rollback.Image = global::MergeQuery.Properties.Resources.rollback;
            this.toolStripButton_Rollback.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Rollback.Name = "toolStripButton_Rollback";
            this.toolStripButton_Rollback.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Rollback.Text = "回滚";
            this.toolStripButton_Rollback.Click += new System.EventHandler(this.toolStripButton_Rollback_Click);
            // 
            // btnExport
            // 
            this.btnExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbKo});
            this.btnExport.Image = global::MergeQuery.Properties.Resources.MSExcel;
            this.btnExport.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(235)))), ((int)(((byte)(7)))));
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(32, 22);
            this.btnExport.Text = "输出查询结果";
            this.btnExport.ButtonClick += new System.EventHandler(this.toolStripButton_Export_Click);
            // 
            // cbKo
            // 
            this.cbKo.Checked = true;
            this.cbKo.CheckOnClick = true;
            this.cbKo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbKo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cbKo.Name = "cbKo";
            this.cbKo.ShowShortcutKeys = false;
            this.cbKo.Size = new System.Drawing.Size(112, 22);
            this.cbKo.Text = "字段加[]";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::MergeQuery.Properties.Resources.clearlog;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "清除日志";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton_Clearlog_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::MergeQuery.Properties.Resources.db_disconnect;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "关闭连接";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton_Disconnect_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::MergeQuery.Properties.Resources.FontDialogHS;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "字体...";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton_Font_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::MergeQuery.Properties.Resources.ActionRequired_03;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Trim";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.ForeColor = System.Drawing.Color.Maroon;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(99, 22);
            this.toolStripLabel1.Text = " 每库数据量限制:";
            // 
            // cmbLimit
            // 
            this.cmbLimit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLimit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbLimit.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLimit.Items.AddRange(new object[] {
            "1000",
            "10000",
            "100000"});
            this.cmbLimit.MaxLength = 6;
            this.cmbLimit.Name = "cmbLimit";
            this.cmbLimit.Size = new System.Drawing.Size(75, 25);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.csv";
            this.saveFileDialog1.Filter = "CSV 文件|*.csv";
            this.saveFileDialog1.Title = "保存文件";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 566);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.KeyPreview = true;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MergeQuery Ver1.3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Shown += new System.EventHandler(this.Main_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Main_KeyUp);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dgData;
        private System.Windows.Forms.Timer tmrTrans;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Execute;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripSplitButton btnExport;
        private System.Windows.Forms.ToolStripMenuItem cbKo;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox txtCon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton_Commit;
        private System.Windows.Forms.ToolStripButton toolStripButton_Rollback;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Stop;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.CheckedListBox lstStatus;
        private System.Windows.Forms.TextBox txtSql;
        private System.Windows.Forms.ToolStripComboBox cmbLimit;
    }
}

