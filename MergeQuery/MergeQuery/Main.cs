﻿#define dbg

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;
using System.Threading;
using System.Collections;
using System.IO;
using System.Diagnostics;

namespace MergeQuery
{
    public partial class Main : Form
    {
        const int ONCE_DBMS_FETCH_COUNT = 8;
        const string EXT_HEAD = "DB#";

        Queue dataQuery;
        Queue logQuery;
        bool working;
        bool exportFlg;
        delegate void LogDelegate(string txt);
        static string cfgPath = Environment.CurrentDirectory + "\\config.txt";
        static string sqlPath = Environment.CurrentDirectory + "\\recent.sql";
        static string hisPath = Environment.CurrentDirectory + "\\history.log";
        List<char> spChar = new List<char>() { ' ', '|', '.', '/', '\\', '\r', '\n', '\t', '\'', '"', ',', ';', '(', ')', '{', '}', '[', ']', '^', '+', '=', '*', '-', ':', '<', '>' };

        List<TnsBean> lstTnsBean;

        static string exlogpath = Environment.CurrentDirectory + "\\exlog.log";

        string buttonLock = "btnLock";
        string transLock = "transLock";

        CfgMgr<Font> cm = new CfgMgr<Font>(Environment.CurrentDirectory + "\\font.cfg");

        [Conditional("dbg")]
        public static void exlog(string txt)
        {
            lock (exlogpath)
            {
                File.AppendAllText(exlogpath, txt + "\r\n");
            }
        }

        public Main()
        {
            InitializeComponent();
            this.Icon = global::MergeQuery.Properties.Resources.extension;
            Control.CheckForIllegalCrossThreadCalls = false;
            dataQuery = Queue.Synchronized(new Queue());
            logQuery = Queue.Synchronized(new Queue());
            lstTnsBean = new List<TnsBean>();
            working = false;
            exportFlg = false;
            cmbLimit.SelectedIndex = 0;

            init();
            dgData.SortCompare += new DataGridViewSortCompareEventHandler((object sender, DataGridViewSortCompareEventArgs e) =>
            {
                if (e.CellValue1 == System.DBNull.Value)
                {
                    e.SortResult = e.CellValue2 == System.DBNull.Value ? 0 : -1;
                    e.Handled = true;
                }
                else if (e.CellValue2 == System.DBNull.Value)
                {
                    e.SortResult = 1;
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            });
        }

        private void init()
        {
            if (!File.Exists(cfgPath))
            {
                var sw = File.CreateText(cfgPath);
                sw.WriteLine("1,DEV-上海,lifeman/lifetest@ldsh0");
                sw.WriteLine("0,DEV-广州,lifeman/lifetest@ld33gz.world");
                sw.Flush();
                sw.Close();
            }
            var sr = File.OpenText(cfgPath);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                TnsBean t = TnsBean.load(line);
                if (t != null)
                {
                    lstStatus.Items.Add(t.Name);
                    lstTnsBean.Add(t);
                    t.Index = lstStatus.Items.Count - 1;
                    t.RelList = lstStatus;
                    lstStatus.SetItemChecked(t.Index, t.Valid && t.Checked);
                    exlog("add:" + t.Name + " idx:" + t.Index + " total:" + lstStatus.Items.Count);
                }
            }
            sr.Close();

            if (cm.Load())
            {
                txtSql.Font = cm.Config;
            }
        }

        private void doQuery()
        {
            exlog("query thread started");
            lstStatus.SelectedIndex = -1;
            string sql = "";

            if (txtSql.SelectionLength > 1)
            {
                sql = txtSql.SelectedText.Trim();
                File.AppendAllText(hisPath, "\r\n【" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "】\r\n" + sql + "\r\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
            }
            sql = sql.Replace("\r", " ").Replace("\n", " ").Trim();
            if (sql.ToUpper().StartsWith("SELECT")
                || sql.ToUpper().StartsWith("UPDATE")
                || sql.ToUpper().StartsWith("INSERT")
                || sql.ToUpper().StartsWith("INSERT"))
            {
                sql = sql.TrimEnd(';');
            }
            if (sql.ToUpper().StartsWith("SELECT"))
            {
                sql = "select * from (" + sql + ") ttxx where rownum <= " + cmbLimit.SelectedItem.ToString();
            }
            txtCon.Text = "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆\r\n" + txtCon.Text;
            for (int i = 0; i < lstTnsBean.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                exlog("idx:" + tb.Index + " status:" + tb.Checked + " name:" + tb.Name);
                if (tb.Checked)
                {
                    Thread t1 = new Thread(new ThreadStart(delegate()
                    {
                        exlog("START-DOQUERY-" + tb._Name);
                        working = true;
                        setButtunStatus();
                        if (tb.Status == 0 || tb.Connection == null || tb.Connection.State == ConnectionState.Closed || tb.Connection.State == ConnectionState.Broken)
                        {
                            tb.Status = 3;
                            exlog("try connect " + tb.Name);
                            if (!tb.Connect())
                            {
                                exlog("connect fail " + tb.Name);
                                logQuery.Enqueue(tb.Msg);
                                RefreshStatus();
                                return;
                            }
                            exlog("connect success" + tb.Name);
                        }
                        if (tb.Connection == null || tb.Status != 1 || tb.Connection.State != ConnectionState.Open)
                        {
                            exlog("db down " + tb.Name);
                            tb.Status = -1;
                            logQuery.Enqueue("数据库不可用:" + tb.Name);
                            RefreshStatus();
                            return;
                        }
                        tb.Status = 3;
                        RefreshStatus();
                        tb.Cancle = false;
                        OracleCommand oc = tb.Connection.CreateCommand();
                        oc.CommandText = sql;
                        oc.CommandType = CommandType.Text;
                        tb.Command = oc;
                        if (tb.Trans == null)
                        {
                            exlog("begin transaction " + tb.Name);
                            tb.Trans = tb.Connection.BeginTransaction();
                        }
                        oc.Transaction = tb.Trans;
                        string txt = "数据库:" + tb._Name;
                        tb.StartTime = DateTime.Now;
                        if (sql.Trim().ToLower().StartsWith("select"))
                        {
                            OracleDataAdapter oda = new OracleDataAdapter(oc);
                            DataTable dt = new DataTable();
                            try
                            {
                                exlog("do fill " + tb.Name);
                                oda.Fill(dt);
                                exlog("fill ok " + tb.Name);
                                txt += "\r\n数量:" + dt.Rows.Count;
                                txt += "\r\n执行时间:" + (DateTime.Now - tb.StartTime).TotalMilliseconds + "ms";
                                if (dt.Rows.Count <= Convert.ToInt32(cmbLimit.SelectedItem.ToString()))
                                {
                                    dt.Columns.Add(EXT_HEAD);
                                    dt.Columns[EXT_HEAD].SetOrdinal(0);
                                    for (int j = 0; j < dt.Rows.Count; j++)
                                    {
                                        dt.Rows[j][EXT_HEAD] = tb._Name;
                                        dataQuery.Enqueue(dt.Rows[j]);
                                        if (dataQuery.Count > 1000)
                                        {
                                            Thread.Sleep(100);
                                        }
                                    }
                                }
                                else
                                {
                                    txt += "\r\n超过每库数据量限制!";
                                }
                            }
                            catch (Exception ex)
                            {
                                txt += "\r\n查询出错!\r\n" + ex.Message;
                            }
                        }
                        else
                        {
                            try
                            {
                                oc.CommandText = "begin dbms_output.enable;end;";
                                oc.ExecuteNonQuery();
                                oc.CommandText = sql;
                                oc.Transaction = tb.Trans;
                                int cnt = oc.ExecuteNonQuery();
                                txt += "\r\n执行时间:" + (DateTime.Now - tb.StartTime).TotalMilliseconds + "ms";
                                txt += "\r\n修改数量:" + cnt;
                                if (cnt > 0)
                                {
                                    toolStripButton_Rollback.Enabled = true;
                                    toolStripButton_Commit.Enabled = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                txt += "\r\n执行出错!\r\n" + ex.Message;
                            }
                            getConsoleOutput(ref txt, oc);
                        }
                        logQuery.Enqueue(txt);
                        tb.Status = 1;
                        exlog("END-DOQUERY-" + tb._Name);
                        RefreshStatus();
                    }));
                    t1.Name = tb.Name;
                    t1.Start();
                }
            }
        }

        private void log(String txt)
        {
            txtCon.Text = "【" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "】\r\n" + txt + "\r\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\r\n" + txtCon.Text;
        }

        private void RefreshStatus()
        {
            foreach (TnsBean tb in lstTnsBean)
            {
                if (!tb.isFinish())
                {
                    working = true;
                    setButtunStatus();
                    return;
                }
            }
            working = false;
            setButtunStatus();
        }

        private void setButtunStatus()
        {
            lock (buttonLock)
            {
                toolStripButton_Execute.Enabled = !working;
                btnExport.Enabled = !working;
                toolStripButton_Stop.Enabled = working;
                lstStatus.Enabled = !working;
            }
        }

        private void tmrTrans_Tick(object sender, EventArgs e)
        {
            lock (transLock)
            {
                while (dataQuery.Count > 0 && !exportFlg)
                {
                    DataRow dr = dataQuery.Dequeue() as DataRow;
                    if (dgData.RowCount == 0)
                    {
                        DataGridViewTextBoxColumn dh;
                        for (int x = 0; x < dr.Table.Columns.Count; x++)
                        {
                            dh = new DataGridViewTextBoxColumn();
                            dh.HeaderText = dr.Table.Columns[x].ColumnName;
                            dh.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                            dgData.Columns.Add(dh);
                        }
                    }
                    dgData.Rows.Add(dr.ItemArray);
                }
                while (logQuery.Count > 0)
                {
                    log(logQuery.Dequeue() as string);
                }
            }
        }

        private void ReduceMemory()
        {
            System.Diagnostics.Process A = System.Diagnostics.Process.GetCurrentProcess();
            A.MaxWorkingSet = System.Diagnostics.Process.GetCurrentProcess().MaxWorkingSet;
            A.Dispose();
        }

        private void Main_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                if (toolStripButton_Execute.Enabled)
                {
                    toolStripButton_Execute_Click(null, null);
                }
                else
                {
                    log("处理中...");
                }
            }
        }

        private void toolStripButton_Execute_Click(object sender, EventArgs e)
        {
            saveSql();
            bool chk = false;
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb.Checked)
                {
                    chk = true;
                }
            }
            if (!chk)
            {
                logQuery.Enqueue("未选择服务器");
                return;
            }
            if (!working)
            {
                tmrTrans.Enabled = false;
                dataQuery.Clear();
                logQuery.Clear();
                dgData.Rows.Clear();
                dgData.Columns.Clear();
                tmrTrans.Enabled = true;
                exlog("create query thread");
                var t1 = new Thread(new ThreadStart(doQuery));
                t1.Name = "doQuery";
                exlog("start query thread");
                t1.Start();
                toolStripButton_Execute.Enabled = false;
                btnExport.Enabled = false;
                toolStripButton_Stop.Enabled = true;
            }
        }

        private void toolStripButton_Clearlog_Click(object sender, EventArgs e)
        {
            txtCon.Text = "";
        }

        private void toolStripButton_Disconnect_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb.Connection != null)
                {
                    try
                    {
                        tb.Command.Cancel();
                        if (tb.Trans != null) tb.Trans.Rollback();
                        tb.Connection.Close();
                        tb.Connection.Dispose();
                    }
                    catch { }
                    tb.Status = 0;
                    RefreshStatus();
                    tb.Connection = null;
                }
            }
        }

        private void getConsoleOutput(ref string txt, OracleCommand cmd)
        {
            string t1 = "";
            int fetchCount = ONCE_DBMS_FETCH_COUNT;
            int fetchedCount = 0;
            OracleParameter[] px = new OracleParameter[ONCE_DBMS_FETCH_COUNT];
            OracleParameter p_count = new OracleParameter();

            p_count.OracleType = OracleType.Number;
            p_count.Direction = ParameterDirection.InputOutput;
            p_count.ParameterName = "cnt";
            p_count.Value = fetchCount;
            cmd.Parameters.Add(p_count);

            for (int i = 0; i < fetchCount; i++)
            {
                t1 += "IF vc > " + i + " THEN :p" + i + " := vo(" + (i + 1) + "); END IF;";
                px[i] = new OracleParameter();
                px[i].OracleType = OracleType.VarChar;
                px[i].Direction = ParameterDirection.Output;
                px[i].Size = 32767;
                px[i].ParameterName = "p" + i;
                cmd.Parameters.Add(px[i]);
            }
            string anonymous_block = "DECLARE vo dbms_output.chararr; vc NUMBER; BEGIN vc := :cnt; dbms_output.get_lines(vo, vc); :cnt := vc; " + t1 + " END; ";

            cmd.CommandText = anonymous_block;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            fetchCount = Convert.ToInt32(p_count.Value.ToString());

            while (fetchCount > 0 && fetchedCount < Convert.ToInt32(cmbLimit.SelectedItem.ToString()))
            {
                fetchedCount += fetchCount;
                for (int i = 0; i < fetchCount; i++)
                {
                    txt += "\r\n" + px[i].Value.ToString();
                }
                cmd.ExecuteNonQuery();

                fetchCount = Convert.ToInt32(p_count.Value.ToString());
            }

            cmd.Dispose();
        }

        private void toolStripButton_Export_Click(object sender, EventArgs e)
        {
            bool chk = false;
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb.Checked)
                {
                    chk = true;
                }
            }
            if (!chk)
            {
                logQuery.Enqueue("未选择服务器");
                return;
            }

            if (cbKo.CheckState == CheckState.Checked)
            {
                saveFileDialog1.DefaultExt = "*.csv";
                saveFileDialog1.Filter = "CSV 文件|*.csv";
            }
            else
            {
                saveFileDialog1.DefaultExt = "*.txt";
                saveFileDialog1.Filter = "文本文件|*.txt";
            }

            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            working = true;
            setButtunStatus();
            var t1 = new Thread(new ThreadStart(export));
            t1.Name = "export";
            t1.Start();
        }

        private void export()
        {
            exlog("START-EXPORT");
            exportFlg = true;
            StreamWriter sw = new StreamWriter(File.Create(saveFileDialog1.FileName), Encoding.Default);
            bool head = false;
            long rows = 0;
            var t1 = new Thread(new ThreadStart(doQuery));
            t1.Name = "export-doQuery";
            t1.Start();
            Thread.Sleep(1000);
            while (working)
            {
                Thread.Sleep(500);
            }
            working = true;
            setButtunStatus();
            while (dataQuery.Count > 0)
            {
                DataRow dr = dataQuery.Dequeue() as DataRow;
                if (!head)
                {
                    for (int i = 0; i < dr.Table.Columns.Count; i++)
                    {
                        sw.Write("\"" + dr.Table.Columns[i].Caption + "\"");
                        if (i == dr.Table.Columns.Count - 1)
                        {
                            sw.Write("\r\n");
                        }
                        else
                        {
                            sw.Write(",");
                        }
                    }
                    head = true;
                }
                // add data line
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (cbKo.CheckState == CheckState.Checked && ("" + dr[i]).Length > 0)
                    {
                        sw.Write("\"[" + dr[i] + "]\"");
                    }
                    else
                    {
                        sw.Write("\"" + dr[i] + "\"");
                    }
                    if (i == dr.ItemArray.Length - 1)
                    {
                        sw.Write("\r\n");
                    }
                    else
                    {
                        sw.Write(",");
                    }
                }
                rows++;
            }

            working = false;
            exportFlg = false;
            setButtunStatus();
            sw.Flush();
            sw.Close();
            logQuery.Enqueue("导出查询结果" + rows + "行");
            exlog("END-EXPORT");
            while (logQuery.Count > 0)
            {
                log(logQuery.Dequeue() as string);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            toolStripButton_Disconnect_Click(null, null);
            saveSql();
            cm.Config = txtSql.Font;
            cm.Save();
        }

        private void saveSql()
        {
            var sw = File.CreateText(cfgPath);
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                sw.WriteLine(tb.save());
            }
            sw.Flush();
            sw.Close();
            sw = File.CreateText(sqlPath);
            sw.Write(txtSql.Text);
            sw.Flush();
            sw.Close();
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            if (File.Exists(sqlPath))
            {
                var sr = File.OpenText(sqlPath);
                txtSql.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        private void toolStripButton_Rollback_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb != null && tb.Connection != null && tb.Command != null)
                {
                    tb.Trans.Rollback();
                    tb.Trans = null;
                }
            }
            toolStripButton_Rollback.Enabled = false;
            toolStripButton_Commit.Enabled = false;
        }

        private void toolStripButton_Commit_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb != null && tb.Connection != null && tb.Command != null)
                {
                    tb.Trans.Commit();
                    tb.Trans = null;
                }
            }
            toolStripButton_Rollback.Enabled = false;
            toolStripButton_Commit.Enabled = false;
        }

        private void toolStripButton_Font_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = txtSql.Font;
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSql.Font = fontDialog1.Font;
            }
        }

        private void toolStripButton_Stop_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstStatus.Items.Count; i++)
            {
                TnsBean tb = lstTnsBean[i];
                if (tb.Status == 3)
                {
                    var t1 = new Thread(() =>
                    {
                        exlog("START-STOP-" + tb._Name);
                        try
                        {
                            tb.Command.Cancel();
                        }
                        catch { }
                        tb.Status = 1;
                        exlog("END-STOP-" + tb._Name);
                    });
                    t1.Name = "STOP-" + tb.Name;
                    t1.Start();
                }
            }
        }

        private void dgData_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
    e.RowBounds.Location.Y,
    dgData.RowHeadersWidth - 4,
    e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dgData.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dgData.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ReduceMemory();
        }

        private void lstStatus_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (lstTnsBean[e.Index].Valid)
            {
                lstTnsBean[e.Index].Checked = (e.NewValue == CheckState.Checked);
            }
            else
            {
                e.NewValue = CheckState.Unchecked;
            }
        }

        private void txtSql_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int clickPos = txtSql.GetCharIndexFromPosition(e.Location);
            char[] ca = txtSql.Text.ToCharArray();
            int start = clickPos;
            int end = clickPos;
            char me = ca[clickPos];

            if (spChar.Contains(me))
            {
                while (start > 0)
                {
                    if (me == ca[start - 1])
                    {
                        start--;
                    }
                    else
                    {
                        break;
                    }
                }
                while (end < ca.Length)
                {
                    if (me == ca[end])
                    {
                        end++;
                    }
                    else
                    {
                        break;
                    }
                }
                txtSql.Select(start, end - start);
            }
            else
            {
                while (start > 0)
                {
                    if (!spChar.Contains(ca[start - 1]))
                    {
                        start--;
                    }
                    else
                    {
                        break;
                    }
                }
                while (end < ca.Length)
                {
                    if (!spChar.Contains(ca[end]))
                    {
                        end++;
                    }
                    else
                    {
                        break;
                    }
                }
                txtSql.Select(start, end - start);
            }
        }
    }

    public class TnsBean
    {
        public DateTime StartTime { get; set; }
        public OracleCommand Command { get; set; }
        public OracleTransaction Trans { get; set; }
        public string _Name;
        public string Name { get { return StsDesp + _Name; } set { _Name = value; } }
        public string Tns { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public bool Checked { get; set; }
        public String Msg { get; set; }
        public OracleConnection Connection { get; set; }
        private Color _lastColor;
        private int _status;
        public bool Valid;
        public int Status
        {
            get { return _status; }
            set
            {
                _status = value;
                lock (RelList)
                {
                    RelList.Items[Index] = Name;
                }
            }
        }
        public int Index { get; set; }
        public CheckedListBox RelList { get; set; }
        public string StsDesp
        {
            get
            {
                if (Status == 0)
                {
                    return "　";
                }
                else if (Status == 1)
                {
                    return "■";
                }
                else if (Status == 3)
                {
                    return "□";
                }
                else
                {
                    return "Ｘ";
                }
            }
        }
        public bool Cancle { get; set; }
        public Color StatusColor
        {
            get
            {
                _lastColor = Color.Gray;
                switch (Status)
                {
                    case 0:
                        _lastColor = Color.Gray;
                        break;
                    case 1:
                        _lastColor = Color.Lime;
                        break;
                    case -1:
                        _lastColor = Color.Red;
                        break;
                    case 3:
                        _lastColor = Color.Cyan;
                        break;
                }
                return _lastColor;
            }
        }
        public bool Connect()
        {
            try
            {
                Connection = new OracleConnection("Data Source=" + Tns + ";User Id=" + User + ";Password=" + Password + ";");
                Connection.Open();
                Msg = "";
                Status = 1;
                Trans = null;
                return true;
            }
            catch (OracleException e)
            {
                Status = -1;
                Connection = null;
                Msg = "连接失败：" + Tns + "\r\n" + e.Message;
            }
            return false;
        }
        public bool isFinish()
        {
            if (Connection != null)
            {
                if (Connection.State == ConnectionState.Broken)
                {
                    Status = -1;
                }
            }
            if (Status != 3)
            {
                return true;
            }
            return false;
        }
        public TnsBean(String name, String tns, String user, String pwd)
        {
            _Name = name;
            Tns = tns;
            User = user;
            Password = pwd;
        }
        public static TnsBean load(String cfg)
        {
            bool spl = false;
            TnsBean tb = null;
            if (cfg == null || cfg.Length < 5)
            {
                return null;
            }
            var s1 = cfg.Split(',');
            if (s1.Length != 3)
            {
                if (s1.Length == 2)
                {
                    spl = true;
                    tb = new TnsBean(s1[1], "", "", "");
                    tb.Checked = false;
                    tb.Valid = false;
                    return tb;
                }
                else
                {
                    return null;
                }
            }
            if (!spl)
            {
                var s2 = s1[2].Split('/');
                if (s2.Length != 2)
                {
                    return null;
                }
                var s3 = s2[1].Split('@');
                if (s3.Length != 2)
                {
                    return null;
                }
                tb = new TnsBean(s1[1], s3[1], s2[0], s3[0]);
                tb.Checked = (s1[0] == "1");
                tb.Valid = true;
                tb.Checked = tb.Checked;
            }
            return tb;
        }
        public string save()
        {
            return (Checked ? "1," : "0,") + _Name + (Valid ? ("," + User + "/" + Password + "@" + Tns) : "");
        }
    }

}