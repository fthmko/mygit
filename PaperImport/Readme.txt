1、用户名&密码你懂的
2、JDBC连接：按照“IP:端口:SID”格式填入，例如“10.25.18.34:1573:ld33gz”
3、数据文件支持CSV-逗号分割/TSV-TAB分割，例如：
    CSV文件1：
        POLNO,PLAN_CODE
        P001,123
        P002,456
        P003,789
    CSV文件2：
        C0001,张三
        C0002,李四
    CSV文件3：
        "C0001","1000"
        "C0002","2340"
    TSV文件：
        P0001	985	C0001
        P0002	541	C0002
        P0003	994	C0002
4、SQL模板。根据数据文件的列数，以问号代替参数，问号数必须等于列数
    例如对于上面的TSV文件，SQL可以是
      insert into temp_table(polno,plan_code,clientno) values(?,?,?)
