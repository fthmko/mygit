/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paperimport;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JTextArea;

/**
 *
 * @author Blum
 */
public class Core {

    public static JTextArea log;
    public static final String CONTENT_CSV = "csv";
    public static final String CONTENT_TSV = "tsv";
    private static SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss");

    public static void Log(String txt) {
        log.append(fmt.format(Calendar.getInstance().getTime()) + " " + txt + "\n");
        log.setCaretPosition(log.getText().length());
    }

    public static Connection getConnection(String url, String user, String pwd) {
        try {
            Class.forName("oracle.jdbc.OracleDriver").newInstance();
        } catch (Exception ex) {
            Log("SQLException: " + ex.getMessage());
            //ex.printStackTrace();
            return null;
        }
        String connstr = "jdbc:oracle:thin:@" + url; //orcl为数据库的SID 
        try {
            return DriverManager.getConnection(connstr, user, pwd);
        } catch (Exception ex) {
            Log("Exception: " + ex.getMessage());
            //ex.printStackTrace();
        }
        return null;
    }

    public static int ImportData(Connection connOut, String fileName, String format, int commitSize, String contentType, boolean hasHeader, boolean dup) {
        long timeBegin = System.currentTimeMillis();
        boolean isExcep = false;
        Log("开始导入数据...");
        PreparedStatement psOut;
        try {
            psOut = connOut.prepareStatement(format);
        } catch (SQLException ex) {
            Log("SQLException: " + ex.getMessage());
            //ex.printStackTrace();
            return -1;
        }

        int cnt = 0;
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(fileName));
            String data = br.readLine();
            String ldata = "!";
            String[] cols;

            int paramCnt = 0;
            char[] chars = format.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] == '?') {
                    paramCnt++;
                }
            }

            while (data != null && data.length() > 0) {
                cnt++;
                if (cnt > 1 || !hasHeader) {
                    if (dup && data.equals(ldata)) {
                        // skip dup line
                        cnt--;
                    } else {
                        if (CONTENT_CSV.equals(contentType)) {
                            cols = data.split(",", -1);
                        } else if (CONTENT_TSV.equals(contentType)) {
                            cols = data.split("	", -1);
                        } else {
                            throw new IOException("不支持的数据格式！");
                        }
                        if (paramCnt != cols.length) {
                            throw new IOException("参数数量不符: SQL=" + paramCnt + " DATA=" + cols.length);
                        }
                        for (int i = 0; i < cols.length; i++) {
                            psOut.setString(i + 1, trimSpec(cols[i], '"'));
                        }
                        psOut.addBatch();
                        if (cnt % commitSize == 0) {
                            psOut.executeBatch();
                            psOut.close();
                            psOut = connOut.prepareStatement(format);
                            Log("提交: " + cnt);
                        }
                    }
                }
                ldata = data;
                data = br.readLine();
            }
            psOut.executeBatch();
        } catch (FileNotFoundException ex) {
            isExcep = true;
            Log("FileNotFoundException: " + ex.getMessage());
            //ex.printStackTrace();
        } catch (IOException ex) {
            isExcep = true;
            Log("IOException: " + ex.getMessage());
            //ex.printStackTrace();
        } catch (SQLException ex) {
            isExcep = true;
            Log("SQLException: " + ex.getMessage());
            //ex.printStackTrace();
        }
        long timeEnd = System.currentTimeMillis();
        if (isExcep) {
            Log("异常结束！");
        } else {
            Log("数据量：" + cnt + " 用时: " + (timeEnd - timeBegin) / 1000 + "秒");
            Log("导入数据完成");
        }
        return cnt;
    }

    public static String trimSpec(String source, char c) {
        String beTrim = String.valueOf(c);

        if (source == null || source.length() == 0) {
            return source;
        }

        String beginChar = source.substring(0, 1);
        while (beginChar.equalsIgnoreCase(beTrim)) {
            source = source.substring(1, source.length());
            if (source.length() == 0) {
                break;
            }
            beginChar = source.substring(0, 1);
        }

        if (source.length() > 0) {
            String endChar = source.substring(source.length() - 1, source.length());
            while (endChar.equalsIgnoreCase(beTrim)) {
                source = source.substring(0, source.length() - 1);
                if (source.length() == 0) {
                    break;
                }
                endChar = source.substring(source.length() - 1, source.length());
            }
        }
        return source;
    }
}
