﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace Poster {
    public abstract class BaseProc {
        public const string UA_FIREFOX5 = "Mozilla/5.0 (Windows NT 6.1; rv:5.0) Gecko/20100101 Firefox/5.0";
        public const string UA_IE8 = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)";
        public const string UA_CHROME17 = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11";
        public const string UA_IPAD = "Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10";
        private const string DEF_ENCODE = "GBK";

        protected HttpClient hc;
        protected string hcMsg = "";
        protected string lastSrc = "";
        public string Name;

        protected abstract void process();

        public void exec() {
            hc = new HttpClient();
            try {
                process();
            } catch (Exception e) {
                setFile("exception_" + DateTime.Now.ToString("yyyyMMddhh24mmss") + ".log", e.StackTrace);
                throw e;
            }
        }

        /// <summary>
        /// 设置UserAgent
        /// 可用值：
        ///     UA_IE8      IE8,默认
        ///     UA_FIREFOX5 Firefox5.0
        ///     UA_CHROME17 Chrome17
        ///     UA_IPAD     IPad
        /// </summary>
        /// <param name="userAgent">UserAgent</param>
        protected void setUA(string userAgent) {
            hc.userAgent = userAgent;
        }

        /// <summary>
        /// 弹出输入文本框
        /// </summary>
        /// <param name="text">提示信息</param>
        /// <param name="def">默认值</param>
        /// <returns>录入值(取消时返回NULL)</returns>
        protected string input(string text, string def) {
            return InputBox.Input("请输入", text, def);
        }

        /// <summary>
        /// 弹出消息框
        /// </summary>
        /// <param name="txt">内容</param>
        /// <param name="title">标题</param>
        protected void msg(string txt, string title) {
            MessageBox.Show(txt, title);
        }

        /// <summary>
        /// 弹出消息框
        /// </summary>
        /// <param name="txt">内容</param>
        protected void msg(string txt) {
            MessageBox.Show(txt);
        }

        /// <summary>
        /// 读取文本文件内容
        /// </summary>
        /// <param name="fileName">文件名(不含路径)<</param>
        /// <returns>文件内容</returns>
        protected string getFile(string fileName) {
            if (File.Exists(Environment.CurrentDirectory + "\\mods\\" + Name + "\\" + fileName)) {
                return System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\mods\\" + Name + "\\" + fileName);
            } else {
                return null;
            }
        }

        /// <summary>
        /// 写入文本文件
        /// </summary>
        /// <param name="fileName">文件名(不含路径)<</param>
        /// <param name="content">文件内容</param>
        protected void setFile(string fileName, string content) {
            if (!Directory.Exists(Environment.CurrentDirectory + "\\mods\\" + Name)) {
                Directory.CreateDirectory(Environment.CurrentDirectory + "\\mods\\" + Name);
            }
            File.WriteAllText(Environment.CurrentDirectory + "\\mods\\" + Name + "\\" + fileName, content);
        }

        /// <summary>
        /// 使用正则提取字符串
        /// </summary>
        /// <param name="src">原始内容</param>
        /// <param name="reg">正则表达式</param>
        /// <returns>提取内容</returns>
        protected string findText(string src, string reg) {
            if (string.IsNullOrEmpty(src)) return "";
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return "";
            return mc[0].Value.Replace("&amp;", "&");
        }

        /// <summary>
        /// 使用正则提取字符串列表
        /// </summary>
        /// <param name="src">原始内容</param>
        /// <param name="reg">正则表达式</param>
        /// <returns>提取列表</returns>
        protected List<List<string>> findList(string src, string reg) {
            MatchCollection mcs = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            List<List<string>> list = new List<List<string>>();
            foreach (Match mc in mcs) {
                List<string> ls = new List<string>();
                for (int i = 1; i < mc.Groups.Count; i++) {
                    ls.Add(mc.Groups[i].Value);
                }
                list.Add(ls);
            }
            return list;
        }

        /// <summary>
        /// 以GET方式发起WEB请求
        /// </summary>
        /// <param name="url">URL地址</param>
        /// <param name="encode">编码</param>
        /// <returns>服务器返回内容</returns>
        protected string getSrc(string url, string encode) {
            string src = hc.GetSrc(url, encode, out hcMsg);
            lastSrc = src;
            dbg(src, "getSrc.txt");
            return src;
        }

        /// <summary>
        /// 以GET方式发起WEB请求
        /// </summary>
        /// <param name="url">URL地址</param>
        /// <param name="encode">编码</param>
        /// <returns>服务器返回内容</returns>
        protected string getSrc(string url) {
            return getSrc(url, DEF_ENCODE);
        }

        /// <summary>
        /// 以POST方式发起WEB请求
        /// </summary>
        /// <param name="url">URL地址</param>
        /// <param name="data">POST数据</param>
        /// <param name="encode">编码</param>
        /// <returns>服务器返回内容</returns>
        protected string postSrc(string url, string data, string encode) {
            string src = hc.PostData(url, data, encode, encode, out hcMsg);
            lastSrc = src;
            dbg(src, "postSrc.txt");
            return src;
        }

        /// <summary>
        /// 以POST方式发起WEB请求
        /// </summary>
        /// <param name="url">URL地址</param>
        /// <param name="data">POST数据</param>
        /// <returns>服务器返回内容</returns>
        protected string postSrc(string url, string data) {
            return postSrc(url, data, DEF_ENCODE);
        }

        /// <summary>
        /// 将字符串整理为一行
        /// </summary>
        /// <param name="src">字符串</param>
        /// <param name="rep">换行替换值</param>
        /// <returns>整理后字符串</returns>
        protected string toSingle(string src, string rep) {
            return src.Replace("\t", "").Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", rep).Replace("\n", rep);
        }


        /// <summary>
        /// 将字符串整理为一行
        /// </summary>
        /// <param name="src">字符串</param>
        /// <returns>整理后字符串</returns>
        protected string toSingle(string src) {
            return toSingle(src, "");
        }

        /// <summary>
        /// 生成debug文件，仅在DEBUG编译下有效
        /// </summary>
        /// <param name="txt">文件内容</param>
        /// <param name="file">文件名(不含路径)</param>
        protected void dbg(string txt, string file) {
#if DEBUG
            if (!Directory.Exists(Environment.CurrentDirectory + "\\mods\\" + Name)) {
                Directory.CreateDirectory(Environment.CurrentDirectory + "\\mods\\" + Name);
            }
            System.IO.File.WriteAllText(Environment.CurrentDirectory + "\\mods\\" + Name + "\\" + file, txt);
#endif
        }
    }
}
