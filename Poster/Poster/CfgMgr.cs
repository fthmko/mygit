﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;
using System.CodeDom.Compiler;
using System.Reflection;
using Microsoft.CSharp;
using System.Xml.Serialization;

namespace Poster {
    public class CfgMgr<T> {
        public String FileName {
            get { return nm; }
        }
        public T Config {
            get { return real; }
            set { real = value; }
        }
        public bool Status {
            get;
            set;
        }
        private T real;
        private String nm;


        public CfgMgr(String fileName) {
            nm = fileName;
        }

        public bool Save() {
            Status = false;
            try {
                if (File.Exists(nm)) File.Delete(nm);
                FileStream fs = new FileStream(nm, FileMode.CreateNew);
                IFormatter saver = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                saver.Serialize(fs, real);
                fs.Close();
                Status = true;
            } catch {
            }
            return Status;
        }

        public bool Load() {
            Status = false;
            try {
                if (!File.Exists(nm)) return false;
                FileStream fs = new FileStream(nm, FileMode.Open);
                IFormatter loader = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                real = (T)loader.Deserialize(fs);
                fs.Close();
                Status = true;
            } catch {
            }
            return Status;
        }
    }

    [Serializable]
    public class XtemData {
        const string CLASS_NAME = "GenClass";
        public string Id { get; set; }
        public string Desc { get; set; }
        public string Code { get; set; }
        public XtemData() {
            Id = Guid.NewGuid().ToString();
        }
        private string BuildMessage(CompilerResults result) {
            string ErrorMessage = "";
            if (result.Errors.HasErrors) {
                foreach (CompilerError err in result.Errors) {
                    ErrorMessage += err.ErrorText + Environment.NewLine;
                }
            }
            return ErrorMessage;
        }
        public string TestCompile() {
            CompilerResults result = Compile();
            return BuildMessage(result);
        }
        private CompilerResults Compile() {
            IDictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("CompilerVersion", "v2.0");
            CSharpCodeProvider objCSharpCodePrivoder = new CSharpCodeProvider(dic);
            CompilerParameters paras = new CompilerParameters();

            // compile to dll
            paras.GenerateExecutable = false;

            // reference
            paras.ReferencedAssemblies.Add("System.dll");
            paras.ReferencedAssemblies.Add("System.Data.dll");
            paras.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            paras.ReferencedAssemblies.Add(Environment.GetCommandLineArgs()[0].Replace(".vshost", ""));

            // write to memory
            paras.GenerateInMemory = true;

            // save path
            //paras.OutputAssembly = "d:\\test\\test.dll";

            string source = getSource();

            if (!Directory.Exists(Environment.CurrentDirectory + "\\mods\\" + Id)) {
                Directory.CreateDirectory(Environment.CurrentDirectory + "\\mods\\" + Id);
            }
            System.IO.File.WriteAllText(Environment.CurrentDirectory + "\\mods\\" + Id + "\\GenClass.cs", source);

            return objCSharpCodePrivoder.CompileAssemblyFromSource(paras, source);
        }
        public Exception Run() {
            Exception ret = null;
            CompilerResults result = Compile();

            if (result.Errors.HasErrors) {
                return new Exception(BuildMessage(result));
            }

            // get complied assembly
            Assembly assembly = result.CompiledAssembly;

            BaseProc prc = assembly.CreateInstance(this.GetType().Namespace + "." + CLASS_NAME) as BaseProc;
            prc.Name = this.Id;
            try {
                prc.exec();
            } catch (Exception e) {
                ret = e;
            }
            GC.Collect();
            return ret;
        }
        public string getSource() {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("using System;");
            sb.AppendLine("using System.Windows.Forms;");
            sb.AppendLine("using System.Text;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using System.Data;");
            sb.AppendLine("namespace " + this.GetType().Namespace);
            sb.AppendLine("{");
            sb.AppendLine("  public class " + CLASS_NAME + ":BaseProc");
            sb.AppendLine("  {");
            sb.AppendLine("    protected override void process() {");
            sb.AppendLine("//======================USER CODE START======================");
            sb.AppendLine(this.Code);
            sb.AppendLine("//=======================USER CODE END=======================");
            sb.AppendLine("    }");
            sb.AppendLine("  }");
            sb.AppendLine("}");

            return sb.ToString();
        }
    }

    [Serializable]
    public class Story {
        public List<XtemData> xtems;
        public Story() {
            xtems = new List<XtemData>();
        }
    }
}