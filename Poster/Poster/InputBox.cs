﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace Poster {
    class InputBox : Form {
        private Label labelText;
        private TextBox textboxValue;
        private Button buttonOK;
        private Button btnCancel;
        private bool onlyNumeric;
        public InputBox() {
            InitializeComponent();
            buttonOK.Click += new EventHandler(buttonOK_Click);
            textboxValue.KeyPress += new KeyPressEventHandler(textboxValue_KeyPress);
        }

        void textboxValue_KeyPress(object sender, KeyPressEventArgs e) {
            if (onlyNumeric)
                if ((e.KeyChar < (char)Keys.D0 || e.KeyChar > (char)Keys.D9) && e.KeyChar != (char)Keys.Back) {
                    e.Handled = true;
                }
        }

        /// <summary>
        /// InputBox的静态函数，返回输入的字符串
        /// </summary>
        /// <param name="Title">窗口标题</param>
        /// <param name="Text">提示文本</param>
        /// <param name="DefaultValue">默认值</param>
        /// <returns>返回字符串</returns>
        public static string Input(string Title, string Text, string DefaultValue) {
            InputBox inputBox = new InputBox();
            inputBox.Text = Title;
            inputBox.labelText.Text = Text;
            inputBox.textboxValue.Text = DefaultValue;
            DialogResult result = inputBox.ShowDialog();
            if (result == DialogResult.OK)
                return inputBox.textboxValue.Text;
            else
                return null;
        }
        /// <summary>
        /// InputBox的静态函数，返回输入的字符串
        /// </summary>
        /// <param name="Title">窗口标题</param>
        /// <param name="Text">提示文本</param>
        /// <param name="DefaultValue">默认值</param>
        /// <param name="OnlyNumeric">是否只允许输入数字</param>
        /// <returns>返回字符串</returns>
        public static string Input(string Title, string Text, string DefaultValue, bool OnlyNumeric) {
            InputBox inputBox = new InputBox();
            inputBox.Text = Title;
            inputBox.labelText.Text = Text;
            inputBox.onlyNumeric = OnlyNumeric;
            inputBox.textboxValue.Text = DefaultValue;
            DialogResult result = inputBox.ShowDialog();
            if (result == DialogResult.OK) {
                return inputBox.textboxValue.Text;
            } else {
                return null;
            }
        }
        private void buttonOK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent() {
            this.labelText = new System.Windows.Forms.Label();
            this.textboxValue = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(13, 13);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(41, 12);
            this.labelText.TabIndex = 0;
            this.labelText.Text = "label1";
            // 
            // textboxValue
            // 
            this.textboxValue.Location = new System.Drawing.Point(15, 29);
            this.textboxValue.Name = "textboxValue";
            this.textboxValue.Size = new System.Drawing.Size(424, 21);
            this.textboxValue.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(277, 56);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "确定";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(364, 56);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // InputBox
            // 
            this.AcceptButton = this.buttonOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(455, 91);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textboxValue);
            this.Controls.Add(this.labelText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
