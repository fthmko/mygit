﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

namespace Poster {
    public partial class Main : Form {
        CfgMgr<Story> cfg;

        public Main() {
            InitializeComponent();
            this.Icon = global::Poster.Properties.Resources.mico;
            cfg = new CfgMgr<Story>("store.dat");
            loadConfig();
        }

        void msg(string txt, string title = "") {
            MessageBox.Show(this, txt, title);
        }

        void loadConfig() {
            if (cfg.Load()) {
                lstStory.ValueMember = "Id";
                lstStory.DisplayMember = "Desc";
                lstStory.DataSource = cfg.Config.xtems;
            } else {
                cfg.Config = new Story();
            }
        }

        private void btnRun_Click(object sender, EventArgs e) {
            if (lstStory.SelectedIndex > -1) {
                XtemData xtd = lstStory.SelectedItem as XtemData;

                Exception ex = xtd.Run();
                if (ex != null) {
                    msg("异常：" + ex.Message + "\n" + "详见mod目录下log文件");
                } else {
                    msg("执行成功！");
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            XtemData xtd = new XtemData();
            Xtem xt = new Xtem(xtd);
            if (xt.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                cfg.Config.xtems.Add(xtd);
                cfg.Save();
                loadConfig();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e) {
            if (lstStory.SelectedIndex > -1) {
                Xtem xt = new Xtem(lstStory.SelectedItem as XtemData);
                if (xt.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    cfg.Save();
                    loadConfig();
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e) {
            if (lstStory.SelectedIndex > -1) {
                XtemData xtd = lstStory.SelectedItem as XtemData;
                if (MessageBox.Show("确认删除 " + xtd.Desc + "?", "确认", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes) {
                    cfg.Config.xtems.Remove(xtd);
                    cfg.Save();
                    loadConfig();
                }
            }
        }
    }
}
