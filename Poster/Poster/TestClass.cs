﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poster {
    class TestClass : BaseProc {
        void copyEcif() {
            string prj = "elis_ecif_dev";
            string urlBase = "http://10.25.204.31:8080/jenkins/";
            string lastUser = getFile("lastUser.txt");
            string user = input("请输入用户名", lastUser);
            string pwd = input("请输入密码", "");

            if (user == null || pwd == null) {
                return;
            }
            string src = postSrc(urlBase + "j_acegi_security_check", "j_username=" + user + "&j_password=" + pwd + "&from=%2fjenkins%2f&Submit=%b5%c7%c2%bc&json=%7b%22j_username%22%3a+%22" + user + "%22%2c+%22j_password%22%3a+%22" + pwd + "%22%2c+%22remember_me%22%3a+false%2c+%22from%22%3a+%22%2fjenkins%2f%22%7d");
            src = getSrc(urlBase);
            if (!src.Contains(user)) {
                msg("登录失败！");
                return;
            } else {
                setFile("lastUser.txt", user);
            }

            string jobType = "ana";
            string lastVersion = getFile("lastVersion.txt");
            if (lastVersion == null) {
                lastVersion = input("请输入复制JOB版本", prj + "1.15.0");
            }
            string destVersion = input("请输入新JOB版本", prj + "1.17.0");
            src = postSrc(urlBase + "view/d02/view/d02_all/createItem", String.Format("name=d2_{2}_{0}&mode=copy&from=d2_{1}_{0}&Submit=OK&json=%7b%22name%22%3a%22d2_{2}_{0}%22%2c%22mode%22%3a%22copy%22%2c%22from%22%3a%22d2_{2}_{1}%22%2c%22Submit%22%3a%22OK%22%7d", jobType, lastVersion, destVersion));
            if (src.Contains(destVersion + "_" + jobType)) {
                postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + destVersion + "_" + jobType + "/configSubmit", getFile(prj + "_" + jobType + ".txt").Replace("#VERSION#", destVersion));
                setFile("lastVersion.txt", destVersion);
            } else {
                msg("复制失败：" + destVersion);
                return;
            }

            jobType = "cud";
            src = postSrc(urlBase + "view/d02/view/d02_all/createItem", String.Format("name=d2_{2}_{0}&mode=copy&from=d2_{1}_{0}&Submit=OK&json=%7b%22name%22%3a%22d2_{2}_{0}%22%2c%22mode%22%3a%22copy%22%2c%22from%22%3a%22d2_{2}_{1}%22%2c%22Submit%22%3a%22OK%22%7d", jobType, lastVersion, destVersion));
            if (src.Contains(destVersion + "_" + jobType)) {
                postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + destVersion + "_" + jobType + "/configSubmit", getFile(prj + "_" + jobType + ".txt").Replace("#VERSION#", destVersion));
            }

            jobType = "fortify";
            src = postSrc(urlBase + "view/d02/view/d02_all/createItem", String.Format("name=d2_{2}_{0}&mode=copy&from=d2_{1}_{0}&Submit=OK&json=%7b%22name%22%3a%22d2_{2}_{0}%22%2c%22mode%22%3a%22copy%22%2c%22from%22%3a%22d2_{2}_{1}%22%2c%22Submit%22%3a%22OK%22%7d", jobType, lastVersion, destVersion));
            if (src.Contains(destVersion + "_" + jobType)) {
                postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + destVersion + "_" + jobType + "/configSubmit", getFile(prj + "_" + jobType + ".txt").Replace("#VERSION#", destVersion));
            }
        }

        void delEcif() {
            string prj = "elis_ecif_dev";
            string urlBase = "http://10.25.204.31:8080/jenkins/";
            string lastUser = getFile("lastUser.txt");
            string user = input("请输入用户名", lastUser);
            string pwd = input("请输入密码", "");
            if (user == null || pwd == null) {
                return;
            }
            string src = postSrc(urlBase + "j_acegi_security_check", "j_username=" + user + "&j_password=" + pwd + "&from=%2fjenkins%2f&Submit=%b5%c7%c2%bc&json=%7b%22j_username%22%3a+%22" + user + "%22%2c+%22j_password%22%3a+%22" + pwd + "%22%2c+%22remember_me%22%3a+false%2c+%22from%22%3a+%22%2fjenkins%2f%22%7d");
            src = getSrc(urlBase);
            if (!src.Contains(user)) {
                msg("登录失败！");
                return;
            } else {
                setFile("lastUser.txt", user);
            }

            string delVersion = getFile("lastVersion.txt");
            if (delVersion == null) {
                delVersion = input("请输入删除JOB版本", prj + "1.15.0");
            }

            string confrm = input("请重新输入版本号确认：" + delVersion, "");
            if (confrm == null || confrm != delVersion) {
                return;
            }


            string jobType = "fortify";
            getSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doWipeOutWorkspace");
            postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doDelete", "");

            jobType = "cud";
            getSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doWipeOutWorkspace");
            postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doDelete", "");

            jobType = "ana";
            getSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doWipeOutWorkspace");
            postSrc(urlBase + "view/d02/view/d02_all/job/d2_" + delVersion + "_" + jobType + "/doDelete", "");
        }

        protected override void process() {


            // view/d02/view/d02_all/job/d2_elis_ecif_dev1.15.0_fortify/doWipeOutWorkspace
        }
    }
}
