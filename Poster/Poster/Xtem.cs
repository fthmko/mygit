﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Poster {
    public partial class Xtem : Form {
        XtemData data;
        public Xtem(XtemData xtd) {
            InitializeComponent();
            data = xtd;
            txtId.Text = data.Id;
            txtDesc.Text = data.Desc;
            txtCode.Text = data.Code;
        }

        private void btnSave_Click(object sender, EventArgs e) {
            data.Desc = txtDesc.Text;
            data.Code = txtCode.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e) {
            XtemData test = new XtemData();
            test.Id = txtId.Text;
            test.Desc = txtDesc.Text;
            test.Code = txtCode.Text;

            string cmp = test.TestCompile();
            if (cmp == string.Empty) {
                cmp = "编译通过！";
            }
            MessageBox.Show(this, cmp);
        }
    }
}
