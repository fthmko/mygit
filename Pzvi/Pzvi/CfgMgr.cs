﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Pzvi {
    public class CfgMgr<T> {
        public String FileName { get; private set; }
        public T Config { get; set; }

        public bool Status {
            get;
            set;
        }

        public CfgMgr(String fileName) {
            FileName = fileName;
        }

        public bool Save() {
            Status = false;
            try {
                if (File.Exists(FileName)) File.Delete(FileName);
                var fs = new FileStream(FileName, FileMode.CreateNew);
                var saver = new BinaryFormatter();
                saver.Serialize(fs, Config);
                fs.Close();
                Status = true;
            }
            catch
            {
                // ignored
            }
            return Status;
        }

        public bool Load() {
            Status = false;
            try {
                if (!File.Exists(FileName)) return false;
                var fs = new FileStream(FileName, FileMode.Open);
                var loader = new BinaryFormatter();
                Config = (T)loader.Deserialize(fs);
                fs.Close();
                Status = true;
            }
            catch
            {
                // ignored
            }
            return Status;
        }
    }


    [Serializable]
    public class MapDict : Dictionary<string, string>, IComparable {
        public string DispKey { get; set; }
        public string SortKey { get; set; }

        public string Name
        {
            get { return ToString(); }
        }

        public MapDict Value
        {
            get { return this; }
        }

        public MapDict()
        {
            DispKey = null;
            SortKey = null;
        }

        public MapDict(string disp)
        {
            DispKey = disp;
            SortKey = disp;
        }

        public MapDict(string disp, string sort)
        {
            DispKey = disp;
            SortKey = sort;
        }

        int IComparable.CompareTo(object obj)
        {
            return String.Compare(this[SortKey], ((MapDict)obj)[SortKey], StringComparison.Ordinal);
        }

        public override string ToString()
        {
            return this[DispKey];
        }
    }
}