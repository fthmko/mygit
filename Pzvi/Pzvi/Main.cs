﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Pzvi {
    public partial class Main : Form {
        public Main()
        {
            InitializeComponent();
            InitCustom();
        }

        private void InitCustom() {
            Icon = Properties.Resources.logviewer;
            MsgBox(SvnHelper.GetBranchVersion());
        }

        private void MsgBox(string message) {
            MessageBox.Show(message, "", MessageBoxButtons.OK);
        }
    }
}
