﻿using System;
using System.Collections.Generic;
using System.Text;
using SharpSvn;

namespace Pzvi {
    class SvnHelper {
        private static readonly SvnClient Client = new SvnClient();

        public static string GetAppPath() {
            //return Environment.CurrentDirectory;
            return @"D:\ccviews\mamc_core\mamc_doc";
        }

        public static SvnInfoEventArgs GetSvnInfo(string localPath) {
            var local = new SvnPathTarget(localPath);
            SvnInfoEventArgs clientInfo;
            Client.GetInfo(local, out clientInfo);
            return clientInfo;
        }

        public static string GetSvnAbsolutePath(string localPath) {
            return GetSvnInfo(localPath).Uri.AbsolutePath;
        }

        public static string GetBranchVersion() {
            return GetSvnAbsolutePath(GetAppPath()).Split('/')[4];
        }
    }
}
