VERSION 5.00
Begin VB.Form MainForm 
   BackColor       =   &H00808080&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "QI"
   ClientHeight    =   2640
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6555
   BeginProperty Font 
      Name            =   "Arial Narrow"
      Size            =   10.5
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   6555
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOpt 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "������"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2505
      ItemData        =   "MainForm.frx":0000
      Left            =   4800
      List            =   "MainForm.frx":0007
      Style           =   1  'Checkbox
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   60
      Width           =   1680
   End
   Begin VB.ListBox lstResult 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "������"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      ItemData        =   "MainForm.frx":0013
      Left            =   60
      List            =   "MainForm.frx":0015
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   600
      Width           =   4695
   End
   Begin VB.TextBox txtInp 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4695
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  Const SWP_NOSIZE = &H1
  Const SWP_NOMOVE = &H2
  Const SWP_NOACTIVATE = &H10
  Const HWND_TOPMOST = -1
  Const HWND_NOTOPMOST = -2
  Const SWP_SHOWWINDOW = &H40
  Private Declare Function SetWindowPos Lib "user32" ( _
  ByVal hwnd As Long, _
  ByVal hWndInsertAfter As Long, ByVal X As Long, _
  ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, _
  ByVal wFlags As Long) As Long

Private Declare Sub InitCommonControls Lib "comctl32.dll" ()

Dim data As Dictionary

Private Sub Form_Initialize()
InitCommonControls
End Sub

Private Sub Form_Load()
    loadData
    SetWindowPos hwnd, HWND_TOPMOST, 0, 0, 0, 0, _
                  SWP_SHOWWINDOW Or SWP_NOMOVE Or SWP_NOSIZE
End Sub

Sub loadData()
    Dim ss() As String
    Dim sa() As String
    Dim s As String
    Dim i As Integer
    Dim cnt As Integer
    Dim key As String
    Set data = New Dictionary
    
    s = GetFile(App.Path & "\data.txt")
    sa = Split(s, vbCrLf)
    
    lstOpt.Clear
    key = "other"
    For i = 0 To UBound(sa)
        If Left(sa(i), 1) = "[" And Right(sa(i), 1) = "]" Then
            If key <> "other" Then
                data.Add key, ss
                lstOpt.AddItem key
                lstOpt.Selected(lstOpt.ListCount - 1) = True
            End If
            key = Mid(sa(i), 2, Len(sa(i)) - 2)
            cnt = 0
        Else
            ReDim Preserve ss(cnt)
            ss(cnt) = sa(i)
            cnt = cnt + 1
        End If
    Next i
    data.Add key, ss
    lstOpt.AddItem key
    lstOpt.Selected(lstOpt.ListCount - 1) = True
    lstOpt.ListIndex = -1
End Sub

Function GetFile(FileName As String) As String
    Dim i As Integer, s As String, BB() As Byte
    If Dir(FileName) = "" Then Exit Function
    i = FreeFile
    ReDim BB(FileLen(FileName) - 1)
    Open FileName For Binary As #i
    Get #i, , BB
    Close #i
    s = StrConv(BB, vbUnicode)
    GetFile = s
End Function

Private Sub lstOpt_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        Dim ca As Boolean
        ca = True
        For i = 0 To lstOpt.ListCount - 1
            If Not lstOpt.Selected(i) Then
                ca = False
            End If
        Next i
        For i = 0 To lstOpt.ListCount - 1
            lstOpt.Selected(i) = Not ca
        Next i
    End If
End Sub

Private Sub txtInp_Change()
    lstResult.Clear
    If txtInp.Text = "" Then
        Exit Sub
    End If
    Dim i As Integer
    For i = 0 To lstOpt.ListCount - 1
        If lstOpt.Selected(i) Then
            treatKey (lstOpt.List(i))
        End If
    Next i
End Sub

Sub treatKey(key As String)
    If Not data.Exists(key) Then
        Exit Sub
    End If
    Dim va() As String
    Dim i As Integer
    va = data(key)
    For i = 0 To UBound(va)
        If InStr(va(i), txtInp.Text) > 0 Then
            lstResult.AddItem "[" & key & "] " & va(i)
        End If
    Next i
End Sub

Private Sub txtInp_GotFocus()
    txtInp.SelStart = 0
    txtInp.SelLength = Len(txtInp.Text)
    txtInp.BackColor = &HC0FFFF
End Sub

Private Sub txtInp_LostFocus()
    txtInp.BackColor = &HE0E0E0
End Sub
