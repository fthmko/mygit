﻿/*
 * Created by SharpDevelop.
 * User: ZHANGZHENG419
 * Date: 2011-07-29
 * Time: 11:23
 *
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using System.Deployment.Application;
using System.Diagnostics;
using System.Reflection;

namespace Qmeet
{
    public partial class MainForm : Form
    {
        const string BASE_URL = "http://svcm.paic.com.cn/svcm/login.do";
        const string LOGIN_URL = "http://svcm.paic.com.cn/svcm/j_security_check";
        const string SEARCH_URL = "http://svcm.paic.com.cn/svcm/confmngt/queryBoardroomNew.do?method=submit";
        const string MEET_URL = "http://svcm.paic.com.cn/svcm/confmngt/queryBoardroomNew.do?method=queryConfDetail&roomId={0}&time={1}";
        const string REGIST_URL = "http://svcm.paic.com.cn/svcm/confmngt/createLocalConf.do?method=createLocalConfStep1";
        const string ENCODE = "GBK";
        const string CHECK_TITLE = "<title>中国平安-电子会议管理系统</title>";
        const string REG_P1 = "confName={0}&confSubject=subject&csNowStart=0&regularMeetingType=1&regularMeetingNum=1&everyFewMonths=1&theFirstFew=1&week={1}&currYearHidden={2}&currMonthHidden={3}&currDayHidden={4}&currHourHidden={5}&currMinuteHidden={6}&preTime=30&multemediaHidden=&year={2}&month={3}&day={4}&hour={5}&minute={6}&";
        const string REG_P2 = "now_year={0}&now_month={1}&now_day={2}&now_hour={3}&now_min={4}&hours={5}&minutes={6}&";
        const string REG_P3 = "participatorNumber={0}&proposerTelephone={1}&confProperty=2&leader=&leaderRoom=&seriesGroupId={2}&seriesGroupIdHidden={3}&roomIdHidden=0&roomId={4},0,0&multimediaList=3&signNumber=&participator=";

        const int LABEL_WIDTH = 100;
        const int ROW_HEIGHT = 30;
        const int HEAD_HEIGHT = 20;
        const int HOUR_FROM = 8;
        const int HOURS = 12;
        const int HOUR_WIDTH = 60;
        const int ROWS_PAGE = 18;
        const int MIN_MEET = 20;

        HttpClient hc;
        string hcMsg = "";
        List<Room> roomData;
        List<Button> btnLib;
        List<Label> lblLib;
        List<Button> btnOn;
        List<Label> lblOn;
        string myName = "";

        string slcType;
        string slcDate;
        int slcIndex;

        AddMeet aForm;

        delegate void DoSomeThing();
        DoSomeThing doDelay;

        public MainForm()
        {
            InitializeComponent();
            this.Icon = global::Qmeet.Properties.Resources.system_users;
            this.Text = "山寨会议室 V" + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            initLine();
            //cbSeries.Enabled = false;
            Control.CheckForIllegalCrossThreadCalls = false;
            Form.CheckForIllegalCrossThreadCalls = false;
        }

        void MainFormLoad(object sender, EventArgs e)
        {
            aForm = new AddMeet();
            roomData = new List<Room>();
            btnLib = new List<Button>();
            lblLib = new List<Label>();
            btnOn = new List<Button>();
            lblOn = new List<Label>();

            for (int i = 0; i < 50; i++)
            {
                Label r = new Label();
                r.Left = 1;
                r.Size = new Size(LABEL_WIDTH - 5, ROW_HEIGHT - 4);
                r.BorderStyle = BorderStyle.Fixed3D;
                r.BackColor = Color.WhiteSmoke;
                r.TextAlign = ContentAlignment.MiddleLeft;
                pnlRoom.Controls.Add(r);
                lblLib.Add(r);
                r.Visible = false;
            }
            for (int i = 0; i < 300; i++)
            {
                Button b = new Button();
                b.Height = ROW_HEIGHT - 3;
                b.TabStop = false;
                pnlRoom.Controls.Add(b);
                b.Click += new EventHandler(r_Click);
                btnLib.Add(b);
                tip.SetToolTip(b, "NULL");
                b.Visible = false;
            }

            hc = new HttpClient();
            txtDate.Value = DateTime.Now;
            txtDate.MinDate = DateTime.Now;
            txtDate.MaxDate = DateTime.Now.AddDays(40);
            cbSeries.SelectedIndex = 15;
            pnlSearch.Enabled = false;
            pbLine.Visible = false;
            pnlRoomBg.Visible = false;
            pgBar.Visible = false;
            doDelay = null;

            string src = getSrc(BASE_URL);
            if (src.Contains(CHECK_TITLE))
            {
                pnlLogin.Visible = false;
                pnlSearch.Enabled = true;
            }
        }

        #region ASSIST
        string getSrc(string url)
        {
            string src = hc.GetSrc(url, ENCODE, out hcMsg);
            if (hcMsg != string.Empty)
            {
                msg(hcMsg);
            }
            dbg(src);
            return src;
        }

        string postSrc(string url, string data)
        {
            string src = hc.PostData(url, data, ENCODE, ENCODE, out hcMsg);
            if (hcMsg != string.Empty)
            {
                msg(hcMsg);
            }
            dbg(src);
            return src;
        }

        void dbg(string txt)
        {
#if DEBUG
            System.IO.File.WriteAllText("xdebug.html", txt);
#endif
        }

        void msg(string txt)
        {
            MessageBox.Show(txt);
        }

        string findText(string src, string reg)
        {
            if (string.IsNullOrEmpty(src)) return "";
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return "";
            return mc[0].Value.Replace("&amp;", "&");
        }

        string to2(int i)
        {
            if (i < 10) return "0" + i;
            return "" + i;
        }

        int getMinutes(DateTime a, DateTime b)
        {
            return (a.Hour - b.Hour) * 60 + a.Minute - b.Minute;
        }
        #endregion

        /// <summary>
        /// 登录事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void BtnLoginClick(object sender, EventArgs e)
        {
            string src = postSrc(LOGIN_URL, "j_username=" + txtName.Text + "&Submit=%B5%C7+%C2%BC&j_password=" + txtPwd.Text);
            if (src.Contains(CHECK_TITLE))
            {
                pnlLogin.Visible = false;
                pnlLogin.Enabled = false;
                pnlSearch.Enabled = true;
                pbLine.Visible = true;
                pnlRoomBg.Visible = true;
                this.AcceptButton = btnSearch;
                myName = findText(src, "(?<=clickName\\(\\);return false;\">)[^<]+(?=</)");
                this.Text = this.Text + " - " + myName;
            }
            else
            {
                msg("登录失败！");
            }
        }

        /// <summary>
        /// 搜索事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void BtnSearchClick(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            txtDate.Enabled = false;
            cbSeries.Enabled = false;
            pgBar.Value = 0;
            pgBar.Visible = true;
            string data = "allVideoType=on&boardroomStateList=1&boroughIndex=0&capacityIndex=0&cityIndex=0&confTypeList=2&eminute=00&groupIndex=0&levelIndex=1&levelList=1&provinceIndex=0&seriesIndex=3&sminute=00&";
            Dictionary<string, string> exprm = new Dictionary<string, string>();
            string[] srs = cbSeries.Text.Split('-');
            exprm.Add("seriesList", srs[0].Trim());
            exprm.Add("seriesList1", srs[1].Trim());
            string[] date = txtDate.Value.ToString("yyyy-MM-dd").Split('-');

            exprm.Add("shour", to2(HOUR_FROM));
            exprm.Add("sday", date[2]);
            exprm.Add("smonth", date[1]);
            exprm.Add("syear", date[0]);
            exprm.Add("ehour", to2(HOUR_FROM + HOURS));
            exprm.Add("eday", date[2]);
            exprm.Add("emonth", date[1]);
            exprm.Add("eyear", date[0]);

            string src = postSrc(SEARCH_URL, data + buildParam(exprm));
            //System.IO.File.WriteAllText("bef.log", src);
            if (hcMsg != string.Empty)
            {
                return;
            }
            src = Regex.Replace(src, " +", " ");
            src = Regex.Replace(src, "^.*?(?=<tr class=\"listcontent-black12)", "");
            src = Regex.Replace(src, "</table>.*$", "");
            src = Regex.Replace(src, "<img.*?>", "");
            src = src.Replace("<br>", "");
            src = src.Replace("<tr class=\"listcontent-black12 \">", "");
            src = src.Replace("</tr>", "\n");
            //System.IO.File.WriteAllText("aft.log", src);
            dbg(src);
            slcDate = txtDate.Value.ToString("yyyy-MM-dd");
            slcType = srs[0].Trim();
            slcIndex = cbSeries.SelectedIndex;
            new Thread(delegate()
            {
                process(src);
                pgBar.Visible = false;
                cbSeries.Enabled = true;
                btnSearch.Enabled = true;
                txtDate.Enabled = true;
                doDelay = activeMeet;
            }).Start();
        }

        private void activeMeet()
        {
            btnOn[0].Focus();
        }

        public static string buildParam(Dictionary<string, string> prm)
        {
            string param = "";
            foreach (var i in prm)
            {
                param = param + "&" + i.Key + "=" + i.Value;
            }
            return param.Substring(1);
        }

        /// <summary>
        /// 搜索结果处理
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        bool process(string src)
        {
            string[] line = src.Split('\n');
            roomData.Clear();
            Room rm;
            pgBar.Maximum = line.Length + 1;
            foreach (string ln in line)
            {
                if (ln.Length > 5)
                {
                    rm = new Room();
                    rm.Name = findText(ln, "(?<=show_room[^>]+>).*?(?=</span>)").Trim();
                    rm.Id = findText(ln, "(?<=onClick=\"show_room\\().*?(?=\\))").Trim();
                    rm.Video = ln.Contains("<br>视频</td>");
                    rm.Data = ln;
                    if (!loadmeet(rm))
                    {
                        return false;
                    }
                    roomData.Add(rm);
                }
                pgBar.Value++;
            }

            draw();
            pgBar.Value++;
            return true;
        }

        /// <summary>
        /// 初始化背景区
        /// </summary>
        void initLine()
        {
            Bitmap bmpHead = new Bitmap(850, 20);
            Bitmap bmpBody = new Bitmap(850, 20);
            Graphics gcHead = Graphics.FromImage(bmpHead);
            Graphics gcBody = Graphics.FromImage(bmpBody);
            gcHead.Clear(pnlRoom.BackColor);
            gcBody.Clear(pnlRoom.BackColor);
            gcHead.DrawLine(Pens.Black, LABEL_WIDTH, HEAD_HEIGHT, LABEL_WIDTH + HOURS * HOUR_WIDTH, HEAD_HEIGHT);
            gcBody.FillRectangle(Brushes.Gainsboro, LABEL_WIDTH + 4 * HOUR_WIDTH, 0, HOUR_WIDTH * 1.5f, 20);
            for (int i = 1; i <= HOURS; i++)
            {
                int h = HOUR_FROM + i;
                gcBody.DrawLine(Pens.Silver, LABEL_WIDTH + i * HOUR_WIDTH, 0, LABEL_WIDTH + i * HOUR_WIDTH, HEAD_HEIGHT);
                gcBody.DrawLine(Pens.LightGray, LABEL_WIDTH + (i - 1) * HOUR_WIDTH + HOUR_WIDTH / 2, 0, LABEL_WIDTH + (i - 1) * HOUR_WIDTH + HOUR_WIDTH / 2, 20);

                gcHead.DrawString(to2(h) + ":00", this.Font, Brushes.Black, LABEL_WIDTH + i * HOUR_WIDTH - 16, 3);
            }
            gcHead.Dispose();
            gcBody.Dispose();
            pbLine.Image = bmpHead;
            pnlRoomBg.BackgroundImage = bmpBody;
        }

        /// <summary>
        /// 绘制会议室
        /// </summary>
        /// <returns></returns>
        bool draw()
        {
            pnlRoom.Visible = false;
            clearControl();
            DateTime bSt = DateTime.Parse(slcDate).AddHours(HOUR_FROM);
            DateTime bEd = bSt.AddHours(HOURS);
            for (int i = 0; i < roomData.Count; i++)
            {
                Label lb = getLabel();
                lb.Text = roomData[i].Name;
                lb.Top = i * ROW_HEIGHT + 2;
                tip.SetToolTip(lb, roomData[i].print());
                Meet lastMeet = null;
                foreach (Meet mt in roomData[i].Meets)
                {
                    Button bt = getButton(mt);
                    bt.Top = lb.Top;
                    Meet tmt = getMeet(roomData[i], lastMeet, mt);
                    if (tmt.Minutes >= MIN_MEET)
                    {
                        Button br = getButton(tmt);
                        br.Top = lb.Top;
                    }
                    lastMeet = mt;
                }
                Meet xmt = getMeet(roomData[i], lastMeet, null);
                if (xmt.Minutes >= MIN_MEET)
                {
                    Button br = getButton(xmt);
                    br.Top = lb.Top;
                }
            }
            pnlRoom.Height = roomData.Count * ROW_HEIGHT + 4;
            pnlRoom.Visible = true;
            return true;
        }

        Meet getMeet(Room rm, Meet frm, Meet to)
        {
            Meet mt = new Meet();
            mt.Color = Color.WhiteSmoke;
            mt.Name = "空闲";
            mt.Creator = "未预定";
            mt.Phone = "无";
            if (frm == null)
            {
                DateTime tim = DateTime.Parse(slcDate);
                mt.StartTime = tim.AddHours(HOUR_FROM);
            }
            else
            {
                mt.StartTime = frm.EndTime;
            }
            if (to == null)
            {
                DateTime tom = DateTime.Parse(slcDate);
                mt.EndTime = tom.AddHours(HOUR_FROM + HOURS);
            }
            else
            {
                mt.EndTime = to.StartTime;
            }
            mt.Rm = rm.Name;
            mt.RmId = rm.Id;
            if (mt.StartTime.Hour < HOUR_FROM)
            {
                mt.StartTime = mt.StartTime.AddHours(HOUR_FROM - mt.StartTime.Hour);
                mt.StartTime = mt.StartTime.AddMinutes(-mt.StartTime.Minute);
            }
            if (mt.EndTime.Hour >= HOUR_FROM + HOURS)
            {
                mt.EndTime = mt.EndTime.AddHours(HOUR_FROM + HOURS - mt.EndTime.Hour);
                mt.EndTime = mt.EndTime.AddMinutes(-mt.EndTime.Minute);
            }
            mt.Minutes = getMinutes(mt.EndTime, mt.StartTime);
            return mt;
        }

        Button getButton(Meet mt)
        {
            Button r = btnLib[btnLib.Count - 1];
            btnLib.RemoveAt(btnLib.Count - 1);
            r.Visible = true;
            btnOn.Add(r);

            r.Text = mt.Name;
            r.Width = mt.Minutes - 1;
            r.Left = (mt.StartTime.Hour - HOUR_FROM) * HOUR_WIDTH + mt.StartTime.Minute + LABEL_WIDTH + 1;
            r.Tag = mt;
            tip.SetToolTip(r, mt.print());
            r.BackColor = mt.Color;
            if (mt.EndTime < DateTime.Now.AddMinutes(30))
            {
                r.ForeColor = Color.DarkGray;
            }
            else
            {
                r.ForeColor = Color.Black;
            }
            return r;
        }

        Label getLabel()
        {
            Label r = lblLib[lblLib.Count - 1];
            lblLib.RemoveAt(lblLib.Count - 1);
            r.Visible = true;
            lblOn.Add(r);
            return r;
        }

        /// <summary>
        /// 清理控件
        /// </summary>
        void clearControl()
        {
            foreach (Button b in btnOn)
            {
                b.Visible = false;
                b.Tag = null;
            }
            foreach (Label l in lblOn)
            {
                l.Visible = false;
            }
            btnLib.AddRange(btnOn);
            lblLib.AddRange(lblOn);
            btnOn.Clear();
            lblOn.Clear();
        }

        /// <summary>
        /// 加载会议室下面的会议信息
        /// </summary>
        /// <param name="rm"></param>
        /// <returns></returns>
        bool loadmeet(Room rm)
        {
            MatchCollection mc = Regex.Matches(rm.Data, "(?<=onClick=\"show_conf\\().*?(?=,\\d+\\))", RegexOptions.IgnoreCase);
            if (mc.Count == 0) return true;
            int i = 0;
            while (i < mc.Count)
            {
                Meet mt = getMeetInfo(rm, mc[i].Value);
                if (mt == null)
                {
                    return false;
                }
                int step = 1;
                if (rm.Meets.Count == 0 || mt.StartTime != rm.Meets[0].StartTime)
                {
                    rm.Meets.Insert(0, mt);
                    step = mt.Minutes / 30 - 1;
                }
                if (step <= 0)
                {
                    step = 1;
                }
                i += step;
            }
            rm.Meets.Sort((Meet a, Meet b) => a.StartTime.CompareTo(b.StartTime));
            return true;
        }

        /// <summary>
        /// 获取会议详情
        /// </summary>
        /// <param name="rm"></param>
        /// <param name="tid"></param>
        /// <returns></returns>
        Meet getMeetInfo(Room rm, string tid)
        {
            string rid = rm.Id;
            string src = getSrc(String.Format(MEET_URL, rid, tid));
            if (hcMsg != string.Empty)
            {
                return null;
            }
            src = findText(src, "(?<=<td height=\"22\"> 1 </td>).*?(?=</tr>)");
            src = Regex.Replace(src, " +", " ");
            src = src.Replace("<td>", "");
            src = src.Replace("</td>", "\n");
            string[] info = src.Split('\n');
            if (info.Length < 5)
            {
                msg("获取会议信息出错：\n" + rm.Id + " - " + tid);
                return null;
            }
            string[] usr = info[4].Trim().Split('/');
            Meet mt = new Meet();
            mt.Rm = rm.Name;
            mt.RmId = rm.Id;
            mt.Tid = tid;
            mt.Name = info[0].Trim();
            DateTime sTime = new DateTime(0);
            mt.StartTime = DateTime.TryParse(info[1].Trim(), out sTime) ? sTime : new DateTime(0);
            int minute = 0;
            mt.Minutes = Int32.TryParse(info[2].Trim(), out minute) ? minute : -1;
            mt.Creator = usr[0];
            mt.Phone = usr.Length > 1 ? usr[1] : "000000";
            if (mt.StartTime.Ticks == 0 || mt.Minutes == -1)
            {
                msg("获取会议信息出错：\n" + mt.print());
                return null;
            }
            mt.EndTime = mt.StartTime.AddMinutes(mt.Minutes);
            return mt;
        }

        /// <summary>
        /// 申请会议
        /// </summary>
        void registMeet()
        {
            string post1, post2, post3;
            DateTime sTime = aForm.getStartTime();
            post1 = String.Format(REG_P1, aForm.getTitle(), sTime.DayOfWeek, sTime.Year, to2(sTime.Month), to2(sTime.Day), to2(sTime.Hour), to2(sTime.Minute));
            int mts = int.Parse(aForm.getMinutes());
            sTime = DateTime.Now;
            post2 = String.Format(REG_P2, sTime.Year, to2(sTime.Month), to2(sTime.Day), to2(sTime.Hour), to2(sTime.Minute), to2(mts / 60), to2(mts % 60));
            post3 = String.Format(REG_P3, aForm.getMens(), aForm.getPhone(), slcType, slcIndex, aForm.getId());
            dbg(post1 + post2 + post3);
            string src = postSrc(REGIST_URL, post1 + post2 + post3);
            string mg = "";
            if (detectResult(src, out mg))
            {
                msg(mg);
                Meet mt = new Meet();
                mt.Name = aForm.getTitle();
                mt.Creator = myName;
                mt.Phone = aForm.getPhone();
                mt.StartTime = aForm.getStartTime();
                mt.EndTime = aForm.getEndTime();
                mt.Minutes = int.Parse(aForm.getMinutes());
                mt.Rm = aForm.getRoom();
                mt.Color = Color.Honeydew;
                Room rrm = null;
                foreach (Room r1 in roomData)
                {
                    if (r1.Name == mt.Rm)
                    {
                        rrm = r1;
                        break;
                    }
                }
                mt.RmId = rrm.Id;
                rrm.Meets.Add(mt);
                rrm.Meets.Sort((Meet a, Meet b) => a.StartTime.CompareTo(b.StartTime));
                draw();
            }
            else
            {
                aForm.setAlert(mg);
                if (aForm.ShowDialog() == DialogResult.OK)
                {
                    registMeet();
                }
            }
        }

        /// <summary>
        /// 处理申请结果
        /// </summary>
        /// <param name="src"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        bool detectResult(string src, out string msg)
        {
            if (src.Contains("错误信息"))
            {
                src = Regex.Replace(src, "^.*错误信息", "");
                msg = "失败：" + findText(src, "(?<=<p>).+?(?=</p>)");
                return false;
            }
            else if (src.Contains("预定成功"))
            {
                msg = "预定成功，请等待审批";
                return true;
            }

            msg = "未知结果！";
            return true;
        }

        #region EVENT

        void PnlRoomClick(object sender, EventArgs e)
        {
            pnlRoom.Focus();
        }

        void r_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Text != "空闲")
            {
                return;
            }
            if (aForm.setTime((Meet)btn.Tag, slcDate))
            {
                if (aForm.ShowDialog() == DialogResult.OK)
                {
                    registMeet();
                }
            }
        }
        #endregion

        private void UpdateApplication()
        {
            if (ApplicationDeployment.IsNetworkDeployed == true)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                string msg = "当前版本:" + ad.CurrentVersion;
                UpdateCheckInfo checkInfo = ad.CheckForDetailedUpdate();
                if (checkInfo.UpdateAvailable == true)
                {
                    msg += "\n最新版本:" + checkInfo.AvailableVersion;
                    MessageBox.Show(this, msg, "版本更新");
                    ad.Update();
                    MessageBox.Show("更新完毕，程序将重新启动");
                    Application.Restart();
                }
            }
        }

        private void tmrDelay_Tick(object sender, EventArgs e)
        {
            if (doDelay != null)
            {
                doDelay();
                doDelay = null;
            }
        }
    }

    [Serializable]
    public class Room
    {
        public string Name;
        public string Id;
        public bool Video;
        public string Data;
        public List<Meet> Meets;
        public Room()
        {
            Meets = new List<Meet>();
        }
        public string print()
        {
            return "编号：" + Id + "\n名称：" + Name + "\n视频：" + (Video ? "有" : "无") + "\n会议：" + Meets.Count + "个";
        }
    }

    [Serializable]
    public class Meet
    {
        public string Rm;
        public string RmId;
        public string Tid;
        public string Name;
        public DateTime StartTime;
        public DateTime EndTime;
        public int Minutes;
        public string Creator;
        public string Phone;
        public Color Color = Color.Gainsboro;
        public string print()
        {
            return "会议室：" + Rm + "\n标题：" + Name + "\n时间：" + StartTime.ToString("HH:mm") + " - " + EndTime.ToString("HH:mm") + "\n时长：" + Minutes + "分钟\n预订者：" + Creator + "(" + Phone + ")";
        }
    }
}
