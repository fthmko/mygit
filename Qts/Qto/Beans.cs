﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Qto {
    [Serializable]
    class Storage {
        private string _pwd;
        public string user { get; set; }
        public string pwd { get { return StringDecoding(_pwd); } set { _pwd = StringEncoding(value); } }
        public Storage() {
            user = "";
            pwd = "";
        }
        static string StringEncoding(string pwd) {
            char[] arrChar = pwd.ToCharArray();
            string strChar = "";
            for (int i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] + 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }

        static string StringDecoding(string pwd) {
            char[] arrChar = pwd.ToCharArray();
            string strChar = "";
            for (int i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] - 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }

        public string uid { get; set; }

        public string deptid { get; set; }

        public string projId { get; set; }

        public string serviceNo { get; set; }

        public string serviceDet { get; set; }

        public string taskNo { get; set; }

        public string taskDet { get; set; }

        public string remark { get; set; }

        public int selectedIndex { get; set; }
    }
}
