﻿namespace Qto
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.dtWork = new System.Windows.Forms.DateTimePicker();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.btnReg = new System.Windows.Forms.Button();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pgPPR = new System.Windows.Forms.TabPage();
            this.txtTaskDet = new System.Windows.Forms.TextBox();
            this.txtSrvDet = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTaskNo = new System.Windows.Forms.TextBox();
            this.txtSrvNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPrjId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pgPER = new System.Windows.Forms.TabPage();
            this.txtDptId = new System.Windows.Forms.TextBox();
            this.txtUid = new System.Windows.Forms.TextBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblWeek = new System.Windows.Forms.Label();
            this.cbAuto = new System.Windows.Forms.CheckBox();
            this.pnlMain.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.pgPPR.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtWork
            // 
            this.dtWork.CustomFormat = "yyyy-MM-dd";
            this.dtWork.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtWork.Location = new System.Drawing.Point(47, 10);
            this.dtWork.Name = "dtWork";
            this.dtWork.Size = new System.Drawing.Size(99, 21);
            this.dtWork.TabIndex = 0;
            this.dtWork.ValueChanged += new System.EventHandler(this.dtWork_ValueChanged);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(47, 10);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(217, 21);
            this.txtUser.TabIndex = 0;
            // 
            // btnReg
            // 
            this.btnReg.Location = new System.Drawing.Point(297, 10);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(38, 21);
            this.btnReg.TabIndex = 11;
            this.btnReg.Text = "录入";
            this.btnReg.UseVisualStyleBackColor = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // txtTime
            // 
            this.txtTime.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTime.Location = new System.Drawing.Point(51, 191);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(50, 21);
            this.txtTime.TabIndex = 7;
            this.txtTime.Text = "7.58";
            this.txtTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.lblResult);
            this.pnlMain.Controls.Add(this.cbAuto);
            this.pnlMain.Controls.Add(this.lblWeek);
            this.pnlMain.Controls.Add(this.tabControl1);
            this.pnlMain.Controls.Add(this.txtDptId);
            this.pnlMain.Controls.Add(this.txtUid);
            this.pnlMain.Controls.Add(this.dtWork);
            this.pnlMain.Controls.Add(this.btnReg);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.txtRemark);
            this.pnlMain.Controls.Add(this.txtTime);
            this.pnlMain.Controls.Add(this.label13);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label9);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(342, 219);
            this.pnlMain.TabIndex = 14;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.pgPPR);
            this.tabControl1.Controls.Add(this.pgPER);
            this.tabControl1.Location = new System.Drawing.Point(5, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(334, 119);
            this.tabControl1.TabIndex = 14;
            // 
            // pgPPR
            // 
            this.pgPPR.Controls.Add(this.txtTaskDet);
            this.pgPPR.Controls.Add(this.txtSrvDet);
            this.pgPPR.Controls.Add(this.label10);
            this.pgPPR.Controls.Add(this.label7);
            this.pgPPR.Controls.Add(this.txtTaskNo);
            this.pgPPR.Controls.Add(this.txtSrvNo);
            this.pgPPR.Controls.Add(this.label12);
            this.pgPPR.Controls.Add(this.txtPrjId);
            this.pgPPR.Controls.Add(this.label8);
            this.pgPPR.Controls.Add(this.label6);
            this.pgPPR.Location = new System.Drawing.Point(4, 22);
            this.pgPPR.Name = "pgPPR";
            this.pgPPR.Padding = new System.Windows.Forms.Padding(3);
            this.pgPPR.Size = new System.Drawing.Size(326, 93);
            this.pgPPR.TabIndex = 0;
            this.pgPPR.Text = "PPR";
            this.pgPPR.UseVisualStyleBackColor = true;
            // 
            // txtTaskDet
            // 
            this.txtTaskDet.Location = new System.Drawing.Point(167, 68);
            this.txtTaskDet.Name = "txtTaskDet";
            this.txtTaskDet.Size = new System.Drawing.Size(153, 21);
            this.txtTaskDet.TabIndex = 13;
            // 
            // txtSrvDet
            // 
            this.txtSrvDet.Location = new System.Drawing.Point(167, 37);
            this.txtSrvDet.Name = "txtSrvDet";
            this.txtSrvDet.Size = new System.Drawing.Size(153, 21);
            this.txtSrvDet.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(106, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "任务描述";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(106, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 21);
            this.label7.TabIndex = 3;
            this.label7.Text = "服务目录";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTaskNo
            // 
            this.txtTaskNo.Location = new System.Drawing.Point(51, 68);
            this.txtTaskNo.Name = "txtTaskNo";
            this.txtTaskNo.Size = new System.Drawing.Size(61, 21);
            this.txtTaskNo.TabIndex = 13;
            // 
            // txtSrvNo
            // 
            this.txtSrvNo.Location = new System.Drawing.Point(51, 37);
            this.txtSrvNo.Name = "txtSrvNo";
            this.txtSrvNo.Size = new System.Drawing.Size(61, 21);
            this.txtSrvNo.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(6, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 21);
            this.label12.TabIndex = 3;
            this.label12.Text = "任务号";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPrjId
            // 
            this.txtPrjId.Location = new System.Drawing.Point(51, 7);
            this.txtPrjId.Name = "txtPrjId";
            this.txtPrjId.Size = new System.Drawing.Size(124, 21);
            this.txtPrjId.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 21);
            this.label8.TabIndex = 3;
            this.label8.Text = "服务号";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 21);
            this.label6.TabIndex = 3;
            this.label6.Text = "项目号";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pgPER
            // 
            this.pgPER.Location = new System.Drawing.Point(4, 22);
            this.pgPER.Name = "pgPER";
            this.pgPER.Padding = new System.Windows.Forms.Padding(3);
            this.pgPER.Size = new System.Drawing.Size(326, 93);
            this.pgPER.TabIndex = 1;
            this.pgPER.Text = "PER";
            this.pgPER.UseVisualStyleBackColor = true;
            // 
            // txtDptId
            // 
            this.txtDptId.Location = new System.Drawing.Point(208, 39);
            this.txtDptId.Name = "txtDptId";
            this.txtDptId.Size = new System.Drawing.Size(124, 21);
            this.txtDptId.TabIndex = 13;
            // 
            // txtUid
            // 
            this.txtUid.Location = new System.Drawing.Point(47, 38);
            this.txtUid.Name = "txtUid";
            this.txtUid.Size = new System.Drawing.Size(106, 21);
            this.txtUid.TabIndex = 13;
            // 
            // lblResult
            // 
            this.lblResult.ForeColor = System.Drawing.Color.Red;
            this.lblResult.Location = new System.Drawing.Point(139, 65);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(193, 16);
            this.lblResult.TabIndex = 12;
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(159, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 21);
            this.label5.TabIndex = 3;
            this.label5.Text = "部门号";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(152, 191);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(183, 21);
            this.txtRemark.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(108, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 21);
            this.label13.TabIndex = 3;
            this.label13.Text = "备注";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "工号";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(7, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 21);
            this.label9.TabIndex = 3;
            this.label9.Text = "工时";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "日期";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(276, 9);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(56, 21);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "登录";
            this.btnLogin.UseVisualStyleBackColor = true;
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(47, 36);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '$';
            this.txtPwd.Size = new System.Drawing.Size(217, 21);
            this.txtPwd.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "用户";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlLogin
            // 
            this.pnlLogin.Controls.Add(this.label1);
            this.pnlLogin.Controls.Add(this.label2);
            this.pnlLogin.Controls.Add(this.txtUser);
            this.pnlLogin.Controls.Add(this.txtPwd);
            this.pnlLogin.Controls.Add(this.btnLogin);
            this.pnlLogin.Location = new System.Drawing.Point(0, 221);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(342, 63);
            this.pnlLogin.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "密码";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWeek
            // 
            this.lblWeek.AutoSize = true;
            this.lblWeek.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblWeek.Location = new System.Drawing.Point(155, 14);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(0, 12);
            this.lblWeek.TabIndex = 15;
            // 
            // cbAuto
            // 
            this.cbAuto.AutoSize = true;
            this.cbAuto.Location = new System.Drawing.Point(280, 14);
            this.cbAuto.Name = "cbAuto";
            this.cbAuto.Size = new System.Drawing.Size(15, 14);
            this.cbAuto.TabIndex = 16;
            this.cbAuto.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 220);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "快速日报 Ver1.4";
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.pgPPR.ResumeLayout(false);
            this.pgPPR.PerformLayout();
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtWork;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox txtDptId;
        private System.Windows.Forms.TextBox txtUid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage pgPPR;
        private System.Windows.Forms.TabPage pgPER;
        private System.Windows.Forms.TextBox txtSrvDet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSrvNo;
        private System.Windows.Forms.TextBox txtPrjId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTaskDet;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTaskNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblWeek;
        private System.Windows.Forms.CheckBox cbAuto;
    }
}

