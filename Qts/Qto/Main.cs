﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.Web;

namespace Qto
{
    public partial class Main : Form
    {
        const string LOGIN_URL = "https://timesheetv2.paic.com.cn/timesheet/j_security_check";
        const string COMMIT_URL = "http://timesheetv2.paic.com.cn/timesheet/createSheet.do";
        const string CUST_URL = "http://timesheetv2.paic.com.cn/timesheet/querySystemAjax.do";
        const string CQ_URL = "http://timesheetv2.paic.com.cn/timesheet/checkCQ.do";
        string ENCODE = "GBK";

        HttpClient hc;
        string hcMsg = "";
        string lastSrc = "";

        List<Panel> pls;
        CfgMgr<Storage> cfg;
        string myId;
        string myDepart;
        string myName;

        public Main()
        {
            InitializeComponent();
            Icon = global::Qto.Properties.Resources.personalization;

            this.Load += new System.EventHandler(this.MainFormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
            this.btnLogin.Click += new System.EventHandler(this.BtnLoginClick);

            pls = new List<Panel>();
            pls.Add(pnlLogin);
            pls.Add(pnlMain);
            hc = new HttpClient();
            Control.CheckForIllegalCrossThreadCalls = false;
            dtWork_ValueChanged(null, null);
        }

        void RunCore()
        {
            bool pass = false;
            dtWork.Value = DateTime.Parse(dtWork.Value.ToString("yyyy-MM-dd"));

            Dictionary<string, string> postParam = new Dictionary<string, string>();
            postParam.Add("departmentId", txtDptId.Text);
            postParam.Add("edit_daily_time_label", "0");
            postParam.Add("edit_sum_time_label", txtTime.Text);
            postParam.Add("empNum", txtUid.Text);
            postParam.Add("isEditTimeSheet", "N");
            postParam.Add("isSubstitute", "N");
            postParam.Add("dailyWorkTimeItem", "");

            postParam.Add("workDate", dtWork.Value.ToString("yyyy-MM-dd"));

            if (tabControl1.SelectedTab.Name == "pgPPR")
            {
                postParam.Add("edit_per_time_label", "0");
                postParam.Add("edit_proj_time_label", txtTime.Text);
                postParam.Add("projectWorkTimeItem", "1");
                postParam.Add("systemWorkTimeItem", "");
                postParam.Add("projectWorkTimes[0].costHours", txtTime.Text);
                postParam.Add("projectWorkTimes[0].projectId", txtPrjId.Text);
                postParam.Add("projectWorkTimes[0].remark", txtRemark.Text);
                postParam.Add("projectWorkTimes[0].serviceItems", txtSrvDet.Text);
                postParam.Add("projectWorkTimes[0].serviceNo", txtSrvNo.Text);
                postParam.Add("projectWorkTimes[0].statusT", "");
                postParam.Add("projectWorkTimes[0].projPercent", "");
                postParam.Add("projectWorkTimes[0].taskDetail", txtTaskDet.Text);
                postParam.Add("projectWorkTimes[0].taskId", txtTaskNo.Text);
            }
            else if (tabControl1.SelectedTab.Name == "pgPER")
            {
                postParam.Add("edit_proj_time_label", "0");
                postParam.Add("edit_per_time_label", txtTime.Text);
                postParam.Add("systemWorkTimeItem", "1");
                postParam.Add("projectWorkTimeItem", "");
                postParam.Add("systemWorkTimes[0].interProjId", "");
                postParam.Add("systemWorkTimes[0].costHours", txtTime.Text);
                postParam.Add("systemWorkTimes[0].projectId", "");
                postParam.Add("systemWorkTimes[0].statusT", "");
                //postParam.Add("systemWorkTimes[0].customer", sysInfo.customerId);
                //postParam.Add("systemWorkTimes[0].needType", cqInfo.needType);
                //postParam.Add("systemWorkTimes[0].remark", txtDesc.Text);
                //postParam.Add("systemWorkTimes[0].serviceItems", serviceInfo.itemName);
                //postParam.Add("systemWorkTimes[0].serviceItemSelect", serviceInfo.itemAll);
                //postParam.Add("systemWorkTimes[0].serviceNo", serviceInfo.serviceNo);
                //postParam.Add("systemWorkTimes[0].srNo", txtTaskId.Text);
                //postParam.Add("systemWorkTimes[0].taskDetail", cqInfo.needDesc);
                //postParam.Add("systemWorkTimes[0].taskDetailDisplay", cqInfo.needNo + "-" + cqInfo.needDesc);
                //postParam.Add("systemWorkTimes[0].taskId", cqInfo.needNo);
                //postParam.Add("systemWorkTimes[0].taskIdDisplay", txtTaskId.Text);
                //postParam.Add("systemWorkTimes[0].taskOwner", taskInfo.csTaskId);
                //postParam.Add("systemWorkTimes[0].taskOwnerInput", taskInfo.taskName);
                msg("TODO");
                return;
            }

            getSrc(COMMIT_URL + "?workDate=" + dtWork.Value.ToString("yyyy-MM-dd"));
            if (lastSrc.Contains("systemWorkTimes[0].costHours") || lastSrc.Contains("projectWorkTimes[0].costHours"))
            {
                msg("这天已经填过了！");
            }
            else
            {
                Thread.Sleep(1000);
                postSrc(COMMIT_URL, build(postParam));
                Thread.Sleep(1000);
                getSrc(COMMIT_URL + "?workDate=" + dtWork.Value.ToString("yyyy-MM-dd"));
                if (lastSrc.Contains("systemWorkTimes[0].costHours") || lastSrc.Contains("projectWorkTimes[0].costHours"))
                {
                    msg("录入成功！");
                    pass = true;
                }
                else
                {
                    msg("录入失败！");
                }
            }
            if (pass && cbAuto.Checked)
            {
                dtWork.Value = dtWork.Value.AddDays(1);
            }
            pnlMain.Enabled = true;
        }

        private string build(Dictionary<string, string> prm)
        {
            string param = "";
            foreach (var i in prm)
            {
                param = param + "&" + i.Key + "=" + i.Value;
            }
            return param.Substring(1);
        }

        #region EVENT
        void BtnLoginClick(object sender, EventArgs e)
        {
            pnlLogin.Enabled = false;
            new Thread(new ThreadStart(delegate()
            {
                if (login(txtUser.Text, txtPwd.Text, LOGIN_URL))
                {
                    maxPanel(pnlMain);
                    Text = Text + " - " + myName;
                    if (Environment.GetCommandLineArgs().Length > 1 && btnReg.Enabled)
                    {
                        btnReg_Click(null, null);
                    }
                }
                else
                {
                    MessageBox.Show(this, "登录失败！");
                    pnlLogin.Enabled = true;
                }
            })).Start();
        }

        void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!pnlLogin.Visible)
            {
                cfg.Config.user = txtUser.Text;
                cfg.Config.pwd = txtPwd.Text;
                cfg.Config.uid = txtUid.Text;
                cfg.Config.deptid = txtDptId.Text;
                cfg.Config.projId = txtPrjId.Text;
                cfg.Config.serviceNo = txtSrvNo.Text;
                cfg.Config.serviceDet = txtSrvDet.Text;
                cfg.Config.taskNo = txtTaskNo.Text;
                cfg.Config.taskDet = txtTaskDet.Text;
                cfg.Config.remark = txtRemark.Text;
                cfg.Config.selectedIndex = tabControl1.SelectedIndex;
                cfg.Save();
            }
        }

        void MainFormLoad(object sender, EventArgs e)
        {
            maxPanel(pnlLogin);
            cfg = new CfgMgr<Storage>("save.dat");
            if (cfg.Load())
            {
                txtUser.Text = cfg.Config.user;
                txtPwd.Text = cfg.Config.pwd;
                txtUid.Text = cfg.Config.uid;
                txtDptId.Text = cfg.Config.deptid;
                txtPrjId.Text = cfg.Config.projId;
                txtSrvNo.Text = cfg.Config.serviceNo;
                txtSrvDet.Text = cfg.Config.serviceDet;
                txtTaskNo.Text = cfg.Config.taskNo;
                txtTaskDet.Text = cfg.Config.taskDet;
                txtRemark.Text = cfg.Config.remark; 

                tabControl1.SelectedIndex = cfg.Config.selectedIndex;
                btnLogin.Focus();
                BtnLoginClick(null, null);
            }
            else
            {
                cfg.Config = new Storage();
            }
        }
        #endregion

        #region CORE
        bool login(string usr, string pwd, string url)
        {
            string src = postSrc(url, "j_username=" + usr + "&Submit=%B5%C7+%C2%BC&j_password=" + pwd);
            if (src.Contains("欢迎您"))
            {
                myName = findText(src, "(?<=欢迎您).+?(?=！)").Split(';')[1];
                src = getSrc(COMMIT_URL);
                myId = findText(src, "(?<=empNum\" value=\").+?(?=\")");
                myDepart = findText(src, "(?<=departmentId\" value=\").+?(?=\")");
                txtUid.Text = myId;
                txtDptId.Text = myDepart;
                return true;
            }
            return false;
        }

        void maxPanel(Panel pa)
        {
            foreach (Panel xp in pls)
            {
                xp.Enabled = false;
                xp.Visible = false;
            }

            pa.Location = new Point(0, 0);
            pa.Width = Width;
            //pa.Height = Height;
            pa.Visible = true;
            pa.Enabled = true;
        }
        #endregion

        #region ASSIST
        string getSrc(string url)
        {
            string src = hc.GetSrc(url, ENCODE, out hcMsg);
            lastSrc = src;
            if (hcMsg != string.Empty)
            {
                msg(hcMsg);
            }
            dbg(src);
            return src;
        }

        string postSrc(string url, string data)
        {
            string src = hc.PostData(url, data, ENCODE, ENCODE, out hcMsg);
            lastSrc = src;
            if (hcMsg != string.Empty)
            {
                msg(hcMsg);
            }
            dbg(src);
            return src;
        }

        void dbg(string txt)
        {
#if DEBUG
            System.IO.File.WriteAllText("xdebug.log", txt);
#endif
        }

        void msg(string txt)
        {
            lblResult.Text = txt;
            //MessageBox.Show(this, txt);
        }

        string findText(string src, string reg)
        {
            if (string.IsNullOrEmpty(src)) return "";
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return "";
            return mc[0].Value.Replace("&amp;", "&");
        }

        #endregion

        private void btnReg_Click(object sender, EventArgs e)
        {
            msg("");
            double time;
            try
            {
                time = Convert.ToDouble(txtTime.Text);
            }
            catch (Exception)
            {
                time = 0;
            }
            if (txtTime.Text.Trim().Length == 0 || time < 1 || time > 16)
            {
                msg("时间不对");
            }
            if (txtRemark.Text.Trim().Length < 3)
            {
                msg("没写备注");
            }
            if (lblResult.Text.Length > 2) return;
            pnlMain.Enabled = false;
            new Thread(new ThreadStart(RunCore)).Start();
        }

        private void dtWork_ValueChanged(object sender, EventArgs e)
        {
            lblWeek.Text = dtWork.Value.DayOfWeek.ToString();
        }
    }
}
