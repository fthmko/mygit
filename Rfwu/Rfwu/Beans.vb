﻿Imports ALinq

Module Extension
    <System.Runtime.CompilerServices.Extension()> _
    Public Function getNestStage(ByVal p As TaskData) As StageData
        Return (From x In p.StageDatas Where x.Finish = 0 Order By x.Edate, x.Sdate Select x).First()
    End Function
End Module

Public Class TaskBase
    Public ReadOnly Property NestStage As StageData
        Get
            Return Util.Nest(TryCast(Me, TaskData))
        End Get
    End Property
End Class