﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DetailForm
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <DebuggerNonUserCodeAttribute()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As Global.System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <DebuggerStepThroughAttribute()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtKey = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtContent = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtStime = New System.Windows.Forms.DateTimePicker()
        Me.txtEtime = New System.Windows.Forms.DateTimePicker()
        Me.btnManage = New System.Windows.Forms.LinkLabel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.pnlCustom = New System.Windows.Forms.GroupBox()
        Me.pnlCustom.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "任务编号"
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(69, 11)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.ReadOnly = True
        Me.txtKey.Size = New System.Drawing.Size(65, 21)
        Me.txtKey.TabIndex = 1
        Me.txtKey.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(140, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 12)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "标题"
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(175, 11)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(299, 21)
        Me.txtTitle.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "具体内容"
        '
        'txtContent
        '
        Me.txtContent.Location = New System.Drawing.Point(69, 36)
        Me.txtContent.Multiline = True
        Me.txtContent.Name = "txtContent"
        Me.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtContent.Size = New System.Drawing.Size(405, 194)
        Me.txtContent.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 238)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 12)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "开始时间"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(254, 238)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 12)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "结束时间"
        '
        'txtStime
        '
        Me.txtStime.CustomFormat = "yyyy/MM/dd HH:mm"
        Me.txtStime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtStime.Location = New System.Drawing.Point(70, 234)
        Me.txtStime.MaxDate = New Date(2099, 12, 31, 0, 0, 0, 0)
        Me.txtStime.MinDate = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.txtStime.Name = "txtStime"
        Me.txtStime.Size = New System.Drawing.Size(139, 21)
        Me.txtStime.TabIndex = 10
        Me.txtStime.Value = New Date(2011, 1, 1, 0, 0, 0, 0)
        '
        'txtEtime
        '
        Me.txtEtime.CustomFormat = "yyyy/MM/dd HH:mm"
        Me.txtEtime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtEtime.Location = New System.Drawing.Point(313, 234)
        Me.txtEtime.MaxDate = New Date(2099, 12, 31, 0, 0, 0, 0)
        Me.txtEtime.MinDate = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.txtEtime.Name = "txtEtime"
        Me.txtEtime.Size = New System.Drawing.Size(139, 21)
        Me.txtEtime.TabIndex = 11
        Me.txtEtime.Value = New Date(2099, 12, 31, 0, 0, 0, 0)
        '
        'btnManage
        '
        Me.btnManage.AutoSize = True
        Me.btnManage.Location = New System.Drawing.Point(406, 0)
        Me.btnManage.Name = "btnManage"
        Me.btnManage.Size = New System.Drawing.Size(53, 12)
        Me.btnManage.TabIndex = 12
        Me.btnManage.TabStop = True
        Me.btnManage.Text = "属性管理"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(399, 474)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "关闭"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(318, 474)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "保存"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.Location = New System.Drawing.Point(12, 474)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 13
        Me.btnEdit.Text = "修改"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'pnlCustom
        '
        Me.pnlCustom.Controls.Add(Me.btnManage)
        Me.pnlCustom.Location = New System.Drawing.Point(9, 261)
        Me.pnlCustom.Name = "pnlCustom"
        Me.pnlCustom.Size = New System.Drawing.Size(465, 207)
        Me.pnlCustom.TabIndex = 14
        Me.pnlCustom.TabStop = False
        Me.pnlCustom.Text = "自定义属性"
        '
        'DetailForm
        '
        Me.ClientSize = New System.Drawing.Size(484, 507)
        Me.Controls.Add(Me.pnlCustom)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtEtime)
        Me.Controls.Add(Me.txtStime)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtContent)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtKey)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DetailForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Detail"
        Me.pnlCustom.ResumeLayout(False)
        Me.pnlCustom.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents txtKey As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtTitle As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtContent As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtStime As DateTimePicker
    Friend WithEvents txtEtime As DateTimePicker
    Friend WithEvents btnManage As LinkLabel
    Friend WithEvents btnClose As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents pnlCustom As GroupBox
End Class
