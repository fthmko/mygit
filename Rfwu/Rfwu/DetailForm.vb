﻿Public Class DetailForm
    Public Task As TaskData

    Public Sub New()
        InitializeComponent()
        init()
    End Sub

    Public Sub setMode(ByVal m As Integer)
        If m = 0 Then
            txtTitle.ReadOnly = True
            txtContent.ReadOnly = True
            txtStime.Enabled = False
            txtEtime.Enabled = False
            pnlCustom.Enabled = False
            btnSave.Visible = False
            btnEdit.Enabled = True
            Me.Text = "查看任务"
        ElseIf m = 1 Then
            txtTitle.ReadOnly = False
            txtContent.ReadOnly = False
            txtStime.Enabled = True
            txtEtime.Enabled = True
            pnlCustom.Enabled = True
            btnSave.Visible = True
            btnEdit.Enabled = False
            Me.Text = "修改任务"
        ElseIf m = 2 Then
            setMode(1)
            Me.Text = "新建任务"
            Task = New TaskData()
            Task.Sdate = DateTime.Now
            Task.Edate = txtEtime.MaxDate
            Task.Cdate = DateTime.Now
            Task.Prop1 = -1
            Task.Prop2 = -1
            Task.Prop3 = -1
            Task.Prop4 = -1
            Task.Prop5 = -1
            Task.Prop6 = -1
            Task.Prop7 = -1
            Task.Prop8 = -1
            Task.Finish = 0
        End If
    End Sub

    Public Sub setData(ByRef data As TaskData)
        Task = data
    End Sub

    Private Sub init()
        For i = 1 To 8
            'ID: P1~p8
            'V1: 1:combo 2:text
            'V2: 
            Dim dts = (From d In Util.context.PropDatas Where d.Group = "PROPERTY" And d.Id = "P" & i Select d)
            Dim dbs = (From d In Util.context.PropDatas Where d.Group = "HEADER" And d.Id = "P" & i Select d)
            If dts.Count = 0 Or dbs.Count = 0 Then
                Exit For
            End If
            Dim dt = dts.First()
            Dim db = dbs.First()
            Dim lb = New Label()
            Dim inp
            If dt.Val1 = "1" Then
                inp = New ComboBox
                inp.DropDownStyle = ComboBoxStyle.DropDownList
                inp.DisplayMember = "Id"
                inp.ValueMember = "Key"
                inp.DataSource = (From pl In Util.context.PropDatas Where pl.Group = ("prop" & i) Or pl.Key = -1 Select pl Order By pl.Sequence).ToList()
            ElseIf dt.Val1 = "2" Then
                inp = New TextBox
            Else
                inp = New Label
            End If
            lb.Text = db.Val1
            If i < 5 Then
                lb.Left = 20
                inp.left = 20
            Else
                lb.Left = 245
                inp.Left = 245
            End If
            lb.Top = 20 + ((i - 1) Mod 4) * 45
            inp.top = lb.Top + 15
            inp.width = 200
            lb.Name = "p" & i & "_lbl"
            inp.Name = "p" & i & "_inp"
            pnlCustom.Controls.Add(inp)
            pnlCustom.Controls.Add(lb)
        Next
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.No
        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        setMode(1)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Task.Title = txtTitle.Text
        Task.Content = txtContent.Text
        Task.Sdate = txtStime.Value
        Task.Edate = txtEtime.Value
        Task.Udate = DateTime.Now

        Dim tp As Type = Task.GetType
        Dim inp
        For i = 1 To 8
            inp = pnlCustom.Controls("p" & i & "_inp")
            If TypeOf inp Is TextBox Then
                If tp.GetProperty("PropData" & i).GetValue(Task, Nothing).Key = -1 Then
                    Dim np = New PropData
                    np.Id = Task.Key
                    np.Val4 = inp.Text
                    np.Group = "prop" & i
                    Util.context.PropDatas.InsertOnSubmit(np)
                    Util.Save()
                    tp.GetProperty("Prop" & i).SetValue(Task, np.Key, Nothing)
                    tp.GetProperty("PropData" & i).SetValue(Task, np, Nothing)
                Else
                    tp.GetProperty("PropData" & i).GetValue(Task, Nothing).Val4 = inp.Text
                End If
            Else
                tp.GetProperty("Prop" & i).SetValue(Task, inp.SelectedValue, Nothing)
            End If
        Next i

        Util.Save()
        Me.DialogResult = DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub DetailForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        txtKey.Text = Task.Key
        txtTitle.Text = Task.Title
        txtContent.Text = Task.Content
        txtStime.Value = Task.Sdate
        txtEtime.Value = Task.Edate
        setMode(0)
        Dim tp As Type = Task.GetType
        Dim inp
        For i = 1 To 8
            inp = pnlCustom.Controls("p" & i & "_inp")
            If TypeOf inp Is TextBox Then
                inp.Text = tp.GetProperty("PropData" & i).GetValue(Task, Nothing).Val4
            Else
                inp.SelectedValue = tp.GetProperty("Prop" & i).GetValue(Task, Nothing)
            End If
        Next i
    End Sub
End Class