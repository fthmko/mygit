﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <DebuggerNonUserCodeAttribute()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As Global.System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <DebuggerStepThroughAttribute()> _
    Private Sub InitializeComponent()
        Me.components = New Global.System.ComponentModel.Container()
        Me.lstTask = New ListView()
        Me.ToolStrip1 = New ToolStrip()
        Me.btnAdd = New ToolStripButton()
        Me.ToolStripSeparator1 = New ToolStripSeparator()
        Me.btnsGroupBy = New ToolStripDropDownButton()
        Me.btnGroupPro1 = New ToolStripMenuItem()
        Me.btnGroupPro2 = New ToolStripMenuItem()
        Me.btnGroupPro3 = New ToolStripMenuItem()
        Me.btnGroupPro4 = New ToolStripMenuItem()
        Me.btnGroupPro5 = New ToolStripMenuItem()
        Me.btnGroupPro6 = New ToolStripMenuItem()
        Me.btnGroupPro7 = New ToolStripMenuItem()
        Me.btnGroupPro8 = New ToolStripMenuItem()
        Me.ToolStripSeparator2 = New ToolStripSeparator()
        Me.btnGroupStart = New ToolStripMenuItem()
        Me.btnGroupEnd = New ToolStripMenuItem()
        Me.btnGroupAdd = New ToolStripMenuItem()
        Me.btnsViewMode = New ToolStripDropDownButton()
        Me.btnViewDetail = New ToolStripMenuItem()
        Me.btnViewBig = New ToolStripMenuItem()
        Me.btnViewSmall = New ToolStripMenuItem()
        Me.btnViewList = New ToolStripMenuItem()
        Me.btnsColumn = New ToolStripSplitButton()
        Me.ToolStripSeparator3 = New ToolStripSeparator()
        Me.btnsFilter = New ToolStripDropDownButton()
        Me.txtSearch = New ToolStripComboBox()
        Me.nfIcon = New NotifyIcon(Me.components)
        Me.mnuTask = New ContextMenuStrip(Me.components)
        Me.btnNext = New ToolStripMenuItem()
        Me.btnEnd = New ToolStripMenuItem()
        Me.btnDel = New ToolStripMenuItem()
        Me.ToolStrip1.SuspendLayout()
        Me.mnuTask.SuspendLayout()
        Me.SuspendLayout()
        '
        'lstTask
        '
        Me.lstTask.AllowColumnReorder = True
        Me.lstTask.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
                    Or AnchorStyles.Left) _
                    Or AnchorStyles.Right), AnchorStyles)
        Me.lstTask.BackColor = Color.WhiteSmoke
        Me.lstTask.FullRowSelect = True
        Me.lstTask.LabelWrap = False
        Me.lstTask.Location = New Point(1, 27)
        Me.lstTask.Name = "lstTask"
        Me.lstTask.Size = New Size(282, 484)
        Me.lstTask.TabIndex = 0
        Me.lstTask.UseCompatibleStateImageBehavior = False
        Me.lstTask.View = View.Details
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New ToolStripItem() {Me.btnAdd, Me.ToolStripSeparator1, Me.btnsGroupBy, Me.btnsViewMode, Me.btnsColumn, Me.ToolStripSeparator3, Me.btnsFilter, Me.txtSearch})
        Me.ToolStrip1.Location = New Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New Size(284, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnAdd
        '
        Me.btnAdd.DisplayStyle = ToolStripItemDisplayStyle.Image
        Me.btnAdd.Image = Global.Rfwu.My.Resources.Resources.Add
        Me.btnAdd.ImageTransparentColor = Color.Magenta
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New Size(23, 22)
        Me.btnAdd.Text = "新增"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New Size(6, 25)
        '
        'btnsGroupBy
        '
        Me.btnsGroupBy.DisplayStyle = ToolStripItemDisplayStyle.Image
        Me.btnsGroupBy.DropDownItems.AddRange(New ToolStripItem() {Me.btnGroupPro1, Me.btnGroupPro2, Me.btnGroupPro3, Me.btnGroupPro4, Me.btnGroupPro5, Me.btnGroupPro6, Me.btnGroupPro7, Me.btnGroupPro8, Me.ToolStripSeparator2, Me.btnGroupStart, Me.btnGroupEnd, Me.btnGroupAdd})
        Me.btnsGroupBy.Image = Global.Rfwu.My.Resources.Resources.ThumbnailView
        Me.btnsGroupBy.ImageTransparentColor = Color.Magenta
        Me.btnsGroupBy.Name = "btnsGroupBy"
        Me.btnsGroupBy.Size = New Size(29, 22)
        Me.btnsGroupBy.Text = "分组"
        '
        'btnGroupPro1
        '
        Me.btnGroupPro1.Name = "btnGroupPro1"
        Me.btnGroupPro1.Size = New Size(124, 22)
        Me.btnGroupPro1.Text = "prop1"
        '
        'btnGroupPro2
        '
        Me.btnGroupPro2.Name = "btnGroupPro2"
        Me.btnGroupPro2.Size = New Size(124, 22)
        Me.btnGroupPro2.Text = "prop2"
        '
        'btnGroupPro3
        '
        Me.btnGroupPro3.Name = "btnGroupPro3"
        Me.btnGroupPro3.Size = New Size(124, 22)
        Me.btnGroupPro3.Text = "prop3"
        '
        'btnGroupPro4
        '
        Me.btnGroupPro4.Name = "btnGroupPro4"
        Me.btnGroupPro4.Size = New Size(124, 22)
        Me.btnGroupPro4.Text = "prop4"
        '
        'btnGroupPro5
        '
        Me.btnGroupPro5.Name = "btnGroupPro5"
        Me.btnGroupPro5.Size = New Size(124, 22)
        Me.btnGroupPro5.Text = "prop5"
        '
        'btnGroupPro6
        '
        Me.btnGroupPro6.Name = "btnGroupPro6"
        Me.btnGroupPro6.Size = New Size(124, 22)
        Me.btnGroupPro6.Text = "prop6"
        '
        'btnGroupPro7
        '
        Me.btnGroupPro7.Name = "btnGroupPro7"
        Me.btnGroupPro7.Size = New Size(124, 22)
        Me.btnGroupPro7.Text = "prop7"
        '
        'btnGroupPro8
        '
        Me.btnGroupPro8.Name = "btnGroupPro8"
        Me.btnGroupPro8.Size = New Size(124, 22)
        Me.btnGroupPro8.Text = "prop8"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New Size(121, 6)
        '
        'btnGroupStart
        '
        Me.btnGroupStart.Name = "btnGroupStart"
        Me.btnGroupStart.Size = New Size(124, 22)
        Me.btnGroupStart.Text = "开始时间"
        '
        'btnGroupEnd
        '
        Me.btnGroupEnd.Name = "btnGroupEnd"
        Me.btnGroupEnd.Size = New Size(124, 22)
        Me.btnGroupEnd.Text = "结束时间"
        '
        'btnGroupAdd
        '
        Me.btnGroupAdd.Name = "btnGroupAdd"
        Me.btnGroupAdd.Size = New Size(124, 22)
        Me.btnGroupAdd.Text = "添加时间"
        '
        'btnsViewMode
        '
        Me.btnsViewMode.DisplayStyle = ToolStripItemDisplayStyle.Image
        Me.btnsViewMode.DropDownItems.AddRange(New ToolStripItem() {Me.btnViewDetail, Me.btnViewBig, Me.btnViewSmall, Me.btnViewList})
        Me.btnsViewMode.Image = Global.Rfwu.My.Resources.Resources.ViewThumbnailsHS
        Me.btnsViewMode.ImageTransparentColor = Color.Magenta
        Me.btnsViewMode.Name = "btnsViewMode"
        Me.btnsViewMode.Size = New Size(29, 22)
        Me.btnsViewMode.Text = "视图"
        '
        'btnViewDetail
        '
        Me.btnViewDetail.CheckOnClick = True
        Me.btnViewDetail.Name = "btnViewDetail"
        Me.btnViewDetail.Size = New Size(124, 22)
        Me.btnViewDetail.Text = "详细内容"
        '
        'btnViewBig
        '
        Me.btnViewBig.CheckOnClick = True
        Me.btnViewBig.Name = "btnViewBig"
        Me.btnViewBig.Size = New Size(124, 22)
        Me.btnViewBig.Text = "大图标"
        '
        'btnViewSmall
        '
        Me.btnViewSmall.CheckOnClick = True
        Me.btnViewSmall.Name = "btnViewSmall"
        Me.btnViewSmall.Size = New Size(124, 22)
        Me.btnViewSmall.Text = "小图标"
        '
        'btnViewList
        '
        Me.btnViewList.CheckOnClick = True
        Me.btnViewList.Name = "btnViewList"
        Me.btnViewList.Size = New Size(124, 22)
        Me.btnViewList.Text = "列表"
        '
        'btnsColumn
        '
        Me.btnsColumn.DisplayStyle = ToolStripItemDisplayStyle.Image
        Me.btnsColumn.Image = Global.Rfwu.My.Resources.Resources.ShowGridlinesHS
        Me.btnsColumn.ImageTransparentColor = Color.Magenta
        Me.btnsColumn.Name = "btnsColumn"
        Me.btnsColumn.Size = New Size(32, 22)
        Me.btnsColumn.Text = "选择列"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New Size(6, 25)
        '
        'btnsFilter
        '
        Me.btnsFilter.DisplayStyle = ToolStripItemDisplayStyle.Image
        Me.btnsFilter.Image = Global.Rfwu.My.Resources.Resources.Filter2HS
        Me.btnsFilter.ImageTransparentColor = Color.Magenta
        Me.btnsFilter.Name = "btnsFilter"
        Me.btnsFilter.Size = New Size(29, 22)
        Me.btnsFilter.Text = "过滤"
        '
        'txtSearch
        '
        Me.txtSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        Me.txtSearch.AutoCompleteSource = AutoCompleteSource.ListItems
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New Size(121, 25)
        '
        'nfIcon
        '
        Me.nfIcon.Text = "计划任务"
        Me.nfIcon.Visible = True
        '
        'mnuTask
        '
        Me.mnuTask.Items.AddRange(New ToolStripItem() {Me.btnNext, Me.btnEnd, Me.btnDel})
        Me.mnuTask.Name = "mnuTask"
        Me.mnuTask.ShowImageMargin = False
        Me.mnuTask.Size = New Size(92, 70)
        '
        'btnNext
        '
        Me.btnNext.DisplayStyle = ToolStripItemDisplayStyle.Text
        Me.btnNext.Name = "btnNext"
        Me.btnNext.ShowShortcutKeys = False
        Me.btnNext.Size = New Size(127, 22)
        Me.btnNext.Text = "下一阶段"
        Me.btnNext.TextImageRelation = TextImageRelation.Overlay
        '
        'btnEnd
        '
        Me.btnEnd.DisplayStyle = ToolStripItemDisplayStyle.Text
        Me.btnEnd.Name = "btnEnd"
        Me.btnEnd.ShowShortcutKeys = False
        Me.btnEnd.Size = New Size(127, 22)
        Me.btnEnd.Text = "终止任务"
        Me.btnEnd.TextImageRelation = TextImageRelation.Overlay
        '
        'btnDel
        '
        Me.btnDel.DisplayStyle = ToolStripItemDisplayStyle.Text
        Me.btnDel.Name = "btnDel"
        Me.btnDel.ShowShortcutKeys = False
        Me.btnDel.Size = New Size(127, 22)
        Me.btnDel.Text = "删除任务"
        Me.btnDel.TextImageRelation = TextImageRelation.Overlay
        '
        'Main
        '
        Me.AutoScaleDimensions = New SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.BackColor = Color.LightGray
        Me.ClientSize = New Size(284, 512)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lstTask)
        Me.MinimumSize = New Size(300, 550)
        Me.Name = "Main"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "计划任务"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.mnuTask.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstTask As ListView
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnsGroupBy As ToolStripDropDownButton
    Friend WithEvents btnsViewMode As ToolStripDropDownButton
    Friend WithEvents btnGroupPro1 As ToolStripMenuItem
    Friend WithEvents btnGroupPro2 As ToolStripMenuItem
    Friend WithEvents btnGroupPro3 As ToolStripMenuItem
    Friend WithEvents btnGroupPro4 As ToolStripMenuItem
    Friend WithEvents btnGroupPro5 As ToolStripMenuItem
    Friend WithEvents btnGroupPro6 As ToolStripMenuItem
    Friend WithEvents btnGroupPro7 As ToolStripMenuItem
    Friend WithEvents btnGroupPro8 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnGroupStart As ToolStripMenuItem
    Friend WithEvents btnGroupEnd As ToolStripMenuItem
    Friend WithEvents btnGroupAdd As ToolStripMenuItem
    Friend WithEvents btnViewDetail As ToolStripMenuItem
    Friend WithEvents btnViewBig As ToolStripMenuItem
    Friend WithEvents btnViewSmall As ToolStripMenuItem
    Friend WithEvents btnViewList As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents btnsColumn As ToolStripSplitButton
    Friend WithEvents btnsFilter As ToolStripDropDownButton
    Friend WithEvents btnAdd As ToolStripButton
    Friend WithEvents nfIcon As NotifyIcon
    Friend WithEvents txtSearch As ToolStripComboBox
    Friend WithEvents mnuTask As ContextMenuStrip
    Friend WithEvents btnNext As ToolStripMenuItem
    Friend WithEvents btnEnd As ToolStripMenuItem
    Friend WithEvents btnDel As ToolStripMenuItem

End Class
