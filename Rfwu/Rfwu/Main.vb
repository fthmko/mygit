﻿Imports ALinq
Imports System.Linq.Dynamic
Imports System.Windows.Forms.ListViewItem

Public Class Main
    '' 表格标题列
    Public Shared ColumnsHeader As List(of PropData)
    Dim dtlForm As New DetailForm

    Private Sub Main_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        init()
        loadConfig()
        loadData()
        test()
    End Sub
    Private Sub Main_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
        saveConfig()
    End Sub

    Private Sub init()
        Me.Icon = Global.Rfwu.My.Resources.Settings
        nfIcon.Icon = Icon.FromHandle(Global.Rfwu.My.Resources.TaskHS.GetHicon())
        'Dim ds = New DataLoadOptions()
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData1)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData2)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData3)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData4)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData5)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData6)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData7)
        'ds.LoadWith(Of TaskData)(Function(p) p.PropData8)
        'ds.LoadWith(Of TaskData)(Function(p) p.StageDatas)
        'Util.context.LoadOptions = ds
    End Sub

    Private Sub loadConfig()
        ''窗口设置
        Me.Height = Util.GetConfig("WINDOW_HEIGHT", Me.Height).Value
        Me.Width = Util.GetConfig("WINDOW_WIDTH", Me.Width).Value
        Me.Left = Util.GetConfig("WINDOW_LEFT", Me.Left).Value
        Me.Top = Util.GetConfig("WINDOW_TOP", Me.Top).Value

        ''读取标题列
        ColumnsHeader = (From p In Util.context.PropDatas Where p.Group = "HEADER" Select p order By p.Sequence).ToList()

        ''标题列
        '' v1 TEXT
        '' v2 WIDTH
        '' v3 DISP
        '' v4 EVAL
        '' v5 1:PRIMARY 2:L-ALIGN 3:R-ALIGN
        '' v6 ORDER
        For Each di In ColumnsHeader
            Dim ti As New ToolStripMenuItem
            ti.Text = di.Val1
            ti.Tag = di.Id
            ti.CheckOnClick = True
            ti.Checked = (di.Val3 = "1")
            AddHandler ti.CheckedChanged, AddressOf btnColumn_CheckedChanged
            btnsColumn.DropDownItems.Add(ti)

            If ti.Checked Then
                Dim ch As New ColumnHeader()
                ch.Text = ti.Text
                ch.Width = di.Val2
                ch.Name = di.Id
                ch.Tag = di.Key
                ch.TextAlign = HorizontalAlignment.Center
                'ch.DisplayIndex = di.Sequence
                lstTask.Columns.Add(ch)
                If di.Val5 = "2" Then ch.TextAlign = HorizontalAlignment.Left
                If di.Val5 = "3" Then ch.TextAlign = HorizontalAlignment.Right
            Else
                di.Sequence = 99
            End If
        Next

        ''显示方式
        Dim si = Util.GetConfig("VIEW_MODE", "btnViewDetail").Value
        btnsViewMode.DropDownItems.Item(si).PerformClick()
    End Sub

    ''' <summary>
    ''' 动态生成显示用数据
    ''' </summary>
    ''' <param name="wh">where</param>
    ''' <param name="od">order</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function loadDynamic(ByVal wh As String, ByVal od As String) As Object
        Dim slt = "New("
        For Each ch In ColumnsHeader
            slt = slt & ch.Val4 & " as " & ch.Id & ","
        Next
        slt = slt.Substring(0, slt.Length - 1) & ")"
        Return Util.context.TaskDatas.Where(wh).OrderBy(od).Select(slt)
    End Function

    ''' <summary>
    ''' 读取任务数据
    ''' </summary>
    ''' <remarks>使用动态Linq</remarks>
    Private Sub loadData()
        Dim datas = loadDynamic("Finish == 0", "Key")
        For Each task In datas
            baData(task)
        Next
    End Sub

    ''' <summary>
    ''' 向listview插入一条数据
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Private Sub baData(ByRef dt)
        Dim strs(lstTask.Columns.Count - 1) As ListViewSubItem
        Dim tp As Type = dt.GetType
        For Each di In (From c In ColumnsHeader Where c.Val3 = "1" Select c Order By c.Sequence)
            strs(di.Sequence) = New ListViewSubItem()
            strs(di.Sequence).Text = (From pi In tp.GetProperties() Where pi.Name.ToLower() = di.Id.ToLower() Select pi).First().GetValue(dt, Nothing)
            strs(di.Sequence).Tag = di.Id
        Next
        Dim li As New ListViewItem()
        Dim priKey = (From pi In ColumnsHeader Where pi.Val5 = "1" Select pi.Id).First()
        li.Tag = (From pi In tp.GetProperties() Where pi.Name.ToLower() = priKey.ToLower() Select pi).First().GetValue(dt, Nothing)
        li.SubItems.AddRange(strs)
        li.SubItems.RemoveAt(0)
        lstTask.Items.Add(li)
    End Sub

    ''' <summary>
    ''' 显示/隐藏列控制
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnColumn_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim di As Object = Nothing '(From c In lstHead Where c.Key = sender.Tag Select c)(0)
        di.Display = sender.Checked
        If di.Display Then
            Dim ch As New ColumnHeader()
            ch.Text = di.Text
            ch.Width = di.Width
            ch.Name = di.Key
            ch.Tag = di.OrderSeq
            di.Seq = lstTask.Columns.Count
            lstTask.Columns.Add(ch)
        Else
            lstTask.Columns.RemoveByKey(di.Key)
            '' 隐藏后，排序就不再起作用
            di.OrderSeq = 99
            di.Seq = 99
        End If
    End Sub

    ''' <summary>
    ''' 保存配置
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub saveConfig()
        ''标题列
        For i = 0 To lstTask.Columns.Count - 1
            Dim cn = i
            Dim ci As PropData = (From c In ColumnsHeader Where c.Key = lstTask.Columns.Item(cn).Tag Select c).First()
            ci.Sequence = lstTask.Columns.Item(cn).DisplayIndex
            ci.Val2 = lstTask.Columns.Item(cn).Width
        Next (i)

        ''窗口设置
        Util.SaveConfig("WINDOW_HEIGHT", Me.Height)
        Util.SaveConfig("WINDOW_WIDTH", Me.Width)
        Util.SaveConfig("WINDOW_LEFT", Me.Left)
        Util.SaveConfig("WINDOW_TOP", Me.Top)

        Util.Save()
    End Sub

    Private Sub test()

    End Sub

    ''' <summary>
    ''' 刷新一条数据
    ''' </summary>
    ''' <param name="tsk"></param>
    ''' <remarks></remarks>
    Private Sub refreshTask(ByRef tsk As TaskData)
        Dim tk = tsk.Key
        Dim dispData = loadDynamic("Finish == 0 and Key == " & tsk.Key, "Key").First()

        Dim cis = From ci In lstTask.Items Where CType(ci.tag, Long) = tk Select ci
        For Each cc As ListViewItem In cis
            cc.SubItems.Item(0).Text = "OK"
            Dim cust = tsk.StageDatas.Where(Function(c) c.Finish = 0).OrderBy(Function(c) c.Edate)(0)
        Next
    End Sub

#Region "ViewSetting"
    Private Sub btnViewDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewDetail.Click
        Dim od As Object = btnsViewMode.DropDownItems.Item(Util.GetConfig("VIEW_MODE", "btnViewDetail").Value)
        od.Checked = False
        lstTask.View = Windows.Forms.View.Details
        Util.SaveConfig("VIEW_MODE", sender.Name)
        sender.Checked = True
    End Sub

    Private Sub btnViewBig_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewBig.Click
        Dim od As Object = btnsViewMode.DropDownItems.Item(Util.GetConfig("VIEW_MODE", "btnViewDetail").Value)
        od.Checked = False
        lstTask.View = Windows.Forms.View.LargeIcon
        Util.SaveConfig("VIEW_MODE", sender.Name)
        sender.Checked = True
    End Sub

    Private Sub btnViewSmall_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewSmall.Click
        Dim od As Object = btnsViewMode.DropDownItems.Item(Util.GetConfig("VIEW_MODE", "btnViewDetail").Value)
        od.Checked = False
        lstTask.View = Windows.Forms.View.SmallIcon
        Util.SaveConfig("VIEW_MODE", sender.Name)
        sender.Checked = True
    End Sub

    Private Sub btnViewList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewList.Click
        Dim od As Object = btnsViewMode.DropDownItems.Item(Util.GetConfig("VIEW_MODE", "btnViewDetail").Value)
        od.Checked = False
        lstTask.View = Windows.Forms.View.Tile
        Util.SaveConfig("VIEW_MODE", sender.Name)
        sender.Checked = True
    End Sub
#End Region

    Private Sub lstTask_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles lstTask.MouseClick
        If Not lstTask.GetItemAt(e.X, e.Y) Is Nothing Then
            If e.Button = MouseButtons.Right And lstTask.SelectedItems.Count > 0 Then
                mnuTask.Show(lstTask, e.Location)
            End If
        End If
    End Sub

    Private Sub lstTask_MouseDoubleClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles lstTask.MouseDoubleClick
        If Not lstTask.GetItemAt(e.X, e.Y) Is Nothing Then
            If e.Button = MouseButtons.Left And lstTask.SelectedItems.Count > 0 Then
                Dim td = (From d In Util.context.TaskDatas Where d.Key = CType(lstTask.SelectedItems(0).Tag, Long) Select d).First()
                dtlForm.setData(td)
                If dtlForm.ShowDialog(Me) = DialogResult.Yes Then
                    refreshTask(td)
                End If
            End If
        End If
    End Sub
End Class
