﻿Imports System.IO
Imports System.ComponentModel
Imports ALinq

Public Class Util
    Public Shared MyPath As String = Application.StartupPath + "\"
    Public Shared DbFileName As String = MyPath + "data.sqlite"
    Public Shared context As TaskContext = New TaskContext

    Public Shared Function GetConfig(ByVal key As String, ByVal def As String) As ConfigData
        Dim cfgs = From c In context.ConfigDatas Where c.Key = key Select c
        Dim result As ConfigData
        If cfgs.Count() = 0 Then
            result = New ConfigData()
            result.Key = key
            result.Value = def
            context.ConfigDatas.InsertOnSubmit(result)
            Save()
        Else
            result = cfgs.First()
        End If
        Return result
    End Function

    Public Shared Sub SaveConfig(ByVal key As String, ByVal val As String)
        Dim c = GetConfig(key, val)
        c.Value = val
        Save()
    End Sub

    Public Shared Sub Save()
        context.SubmitChanges()
    End Sub

    Public Shared Function Nest(ByRef ss As TaskData) As StageData
        Return (From x In ss.StageDatas Where x.Finish =0 order By x.Edate,x.Sdate Select x).First()
    End Function
End Class
