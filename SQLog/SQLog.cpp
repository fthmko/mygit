#include "stdafx.h"
#include "SQLog.h"
#include "sqlite3.h"
#include <string>
#include <sstream>
#include <io.h>

char *const Desc = "BlumSQLog";
int         PlugInID;
sqlite3 *   pDB;

BOOL    (*REF_IDE_GetConnectionInfoEx)(int ix, char **Username, char **Password, char **Database, char **ConnectAs) = NULL;
int     (*REF_IDE_GetWindowConnection)() = NULL;
char*   (*REF_SYS_RootDir)() = NULL;
char*   (*REF_IDE_GetText)() = NULL;
char*   (*REF_IDE_GetSelectedText)() = NULL;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

const char* IntToStr(int n)
{
	static std::string s;
	std::stringstream ss;
	ss << n;
	s = ss.str();
	return s.c_str();
}

void ErrorMessage(const char* ErrMsg)
{
	std::stringstream ss;
	ss << "Error: (" << ErrMsg << ") Function not initialized!!!" ;
	MessageBox(NULL, ss.str().c_str(), NULL, 0);
}

void ShowMessage(const char* Data)
{
	MessageBox(NULL, Data, "SQLog", 0);
}

void  Configure()
{
	ShowMessage("No configue menu");
}

char* IdentifyPlugIn(int ID)
{
	PlugInID = ID;
	return Desc;
}

void  OnCreate()
{
	//ShowMessage("LOAD");
}

int insertDB(char* db,char* usr,char* text, int windowType)
{
	char* errMsg;
	int res = sqlite3_exec(pDB,"begin transaction;",0,0, &errMsg);

	sqlite3_stmt* stmt3 = NULL;
	const char* insertSQL = "insert into sqlog(db_sid,db_user,sqltext,exec_time,exec_date,window_type) values(?,?,?,time('now','8 hour'),date('now','8 hour'),?)";

	if (sqlite3_prepare_v2(pDB,insertSQL,strlen(insertSQL),&stmt3,NULL) != SQLITE_OK) {
		if (stmt3) {
			sqlite3_finalize(stmt3);
			MessageBox(NULL, "sqlite3_prepare_v2", "SQLog-记录日志异常", 0);
			return 1;
		}
	}

	sqlite3_bind_text(stmt3, 1, db, strlen(db), SQLITE_TRANSIENT);
	sqlite3_bind_text(stmt3, 2, usr, strlen(usr), SQLITE_TRANSIENT);
	sqlite3_bind_text(stmt3, 3, text, strlen(text), SQLITE_TRANSIENT);
	sqlite3_bind_text(stmt3, 4, IntToStr(windowType), strlen(IntToStr(windowType)), SQLITE_TRANSIENT);

	if (sqlite3_step(stmt3) != SQLITE_DONE) {
		sqlite3_finalize(stmt3);
		MessageBox(NULL, "sqlite3_step", "SQLog-记录日志异常", 0);
		return 1;
	}

	sqlite3_finalize(stmt3);
	res = sqlite3_exec(pDB,"commit transaction;",0,0, &errMsg);
	
	return 0;
}

void  AfterExecuteWindow(int WindowType, int Result)
{
	if (Result > 0) {
		if (WindowType == 1 || WindowType == 2 || WindowType == 4 || WindowType == 5) {
			if(!pDB) {
				char ph[512];
				ph[0] = 0;
				strcat(ph, SYS_RootDir());
				strcat(ph, "PlugIns\\sqlog.db");
				//ShowMessage(ph);
				if( (_access(ph, 0)) != -1 ){
					int res = sqlite3_open(ph, &pDB); 
				} else {
					MessageBox(NULL, ph, "SQLog-DB文件丢失", 0);
					return;
				}
			}
			int windowId = IDE_GetWindowConnection();
			char** usr = new char *[100];
			char** pwd = new char *[100];
			char** db = new char *[100];
			char** conn = new char *[100];
			IDE_GetConnectionInfoEx(windowId, usr, pwd, db, conn);

			char* xt = IDE_GetSelectedText();
			char* gt = IDE_GetText();
			
			if(strlen(xt) == 0 ){
				insertDB(db[0],usr[0],gt,WindowType);
			} else {
				insertDB(db[0],usr[0],xt,WindowType);
			}
		}
	}
}

void  RegisterCallback(int Index, void *Addr)
{
    switch (Index)
    {
	case 3 :
        {
			memcpy(&REF_SYS_RootDir, &Addr, sizeof(Addr));
            break;
        }
	case 30 :
        {
			memcpy(&REF_IDE_GetText, &Addr, sizeof(Addr));
            break;
        }
	case 31 :
        {
			memcpy(&REF_IDE_GetSelectedText, &Addr, sizeof(Addr));
            break;
        }
	case 240 :
        {
			memcpy(&REF_IDE_GetConnectionInfoEx, &Addr, sizeof(Addr));
            break;
        }	
	case 245 :
        {
			memcpy(&REF_IDE_GetWindowConnection, &Addr, sizeof(Addr));
            break;
        }
    }
}

void IDE_GetConnectionInfoEx(int ix, char **Username, char **Password, char **Database, char **ConnectAs)
{
    if (!REF_IDE_GetConnectionInfoEx) {ErrorMessage("IDE_GetConnectionInfoEx");return;}
    REF_IDE_GetConnectionInfoEx(ix, Username, Password, Database, ConnectAs);
}

int IDE_GetWindowConnection()
{
    if (!REF_IDE_GetWindowConnection) {ErrorMessage("IDE_GetWindowConnection");return -1;}
    return REF_IDE_GetWindowConnection();
}

char* SYS_RootDir()
{
    if (!REF_SYS_RootDir) {ErrorMessage("SYS_RootDir");return "";}
    return REF_SYS_RootDir();
}

char* IDE_GetText()
{
    if (!REF_IDE_GetText) {ErrorMessage("IDE_GetText");return "";}
    return REF_IDE_GetText();
}

char* IDE_GetSelectedText()
{
    if (!REF_IDE_GetSelectedText) {ErrorMessage("IDE_GetSelectedText");return "";}
    return REF_IDE_GetSelectedText();
}
