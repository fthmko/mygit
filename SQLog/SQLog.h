#if !defined(SQLog_H_INCLUDED_)
#define SQLog_H_INCLUDED_

//Functions to expert to PL/SQL Dev
extern "C"
{
  __declspec(dllexport) char* IdentifyPlugIn(int);
  __declspec(dllexport) void  RegisterCallback(int, void *);
  __declspec(dllexport) void  OnCreate();
  __declspec(dllexport) void  AfterExecuteWindow(int, int);
  __declspec(dllexport) void  Configure();
}

void    IDE_GetConnectionInfoEx(int ix, char **Username, char **Password, char **Database, char **ConnectAs);
int     IDE_GetWindowConnection();
char*   SYS_RootDir();
char*   IDE_GetText();
char*   IDE_GetSelectedText();

#endif // !defined(SQLog_H_INCLUDED_)
