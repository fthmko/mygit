﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;

namespace Sosh
{
    public partial class Main : Form
    {
        string myPath;
        Process zp;
        Thread srThead;
        bool run;
        string lastFile;

        public Main()
        {
            InitializeComponent();
            Icon = global::Sosh.Properties.Resources.folder_saved_search;
            myPath = Application.StartupPath + "\\";
            zp = new Process();
            zp.StartInfo.FileName = myPath + "7z.exe";
            zp.StartInfo.RedirectStandardError = true;
            zp.StartInfo.RedirectStandardOutput = true;
            zp.StartInfo.CreateNoWindow = true;
            zp.StartInfo.UseShellExecute = false;
            Control.CheckForIllegalCrossThreadCalls = false;
            setStatus(0);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (dlgFolder.ShowDialog() == DialogResult.OK)
            {
                txtFolder.Text = dlgFolder.SelectedPath;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string errMsg = "";
            if (!Directory.Exists(txtFolder.Text))
            {
                errMsg += "目录不存在！\n";
            }
            if (txtSearch.Text.Length == 0)
            {
                errMsg += "请输入搜索文本！\n";
            }
            else if (!validateRegex())
            {
                errMsg += "无效的正则表达式！\n";
            }
            if (txtFilter.Text.LastIndexOfAny(new char[] { '\\', '/', ':', '|', '"', '<', '>' }) > -1)
            {
                errMsg += "文件类型无效！\n";
            }

            if (errMsg.Length > 0)
            {
                msg(errMsg);
                return;
            }

            setStatus(1);
            srThead = new Thread(new ThreadStart(searchMain));
            srThead.IsBackground = true;
            srThead.Start();
        }

        private void searchMain()
        {
            try
            {
                List<string> files = new List<string>();
                string[] filters = txtFilter.Text.Split(';');
                foreach (string filter in filters)
                {
                    if (filter.Trim().Length > 0)
                    {
                        files.AddRange(Directory.GetFiles(txtFolder.Text, filter, cbSubfolder.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
                    }
                }
                files.Sort();
                setStatus(2);
                pbProgress.Maximum = files.Count;
                pbProgress.Value = 0;
                foreach (string file in files)
                {
                    if (!run) break;
                    searchFile(file);
                }
            }
            catch (Exception ex)
            {
                msg(ex.Source + " - " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                setStatus(0);
            }
            return;
        }

        private void searchFile(string path)
        {
            List<string> files = getFileList(path);
            string rfn;
            foreach (string file in files)
            {
                if (!run) break;
                rfn = Path.GetFileName(file);
                if (cbRegx.Checked)
                {
                    if (Regex.IsMatch(rfn, txtSearch.Text, RegexOptions.IgnoreCase))
                    {
                        addMatch(path, file);
                    }
                }
                else
                {
                    if (cbStrict.Checked)
                    {
                        if (rfn.ToLower().StartsWith(txtSearch.Text.ToLower()))
                        {
                            addMatch(path, file);
                        }
                    }
                    else
                    {
                        if (rfn.ToLower().Contains(txtSearch.Text.ToLower()))
                        {
                            addMatch(path, file);
                        }
                    }
                }
            }
            pbProgress.Value++;
        }

        private List<string> getFileList(string path)
        {
            string[] res = getListInFile(path).Split('\n');
            List<string> lst = new List<string>();
            foreach (string rn in res)
            {
                if (!run) break;
                if (Regex.IsMatch(rn, @"^([\d\-\/]+) +([\d:]+) +([\w\.]+) +([\d]+) +([\d]+) +(.+)$", RegexOptions.IgnoreCase))
                {
                    Match mc = Regex.Match(rn, @"^([\d\-\/]+) +([\d:]+) +([\w\.]+) +([\d]+) +([\d]+) +(.+)$", RegexOptions.IgnoreCase);
                    if (!mc.Groups[3].Value.StartsWith("D")) lst.Add(mc.Groups[6].Value);
                }
            }
            return lst;
        }

        private string getListInFile(string path)
        {
            zp.StartInfo.Arguments = " l \"" + path + "\"";
            zp.Start();
            return zp.StandardOutput.ReadToEnd().Replace("\r", "");
        }

        private void setStatus(int t)
        {
            if (t == 1)
            {
                pnlMain.Enabled = false;
                lstResult.Items.Clear();
                lblStatus.Text = "正在获取列表...";
            }
            else if (t == 2)
            {
                lblStatus.Text = "正在查找文件...";
                btnStop.Enabled = true;
                btnStop.BackColor = Color.Red;
            }
            else
            {
                pbProgress.Value = 0;
                pbProgress.Maximum = 1;
                lblStatus.Text = "";
                pnlMain.Enabled = true;
                run = true;
                btnStop.Enabled = false;
                btnStop.BackColor = Color.IndianRed;
                lastFile = "-";
            }
        }

        private void addMatch(string filePath, string innerPath)
        {
            if (filePath != lastFile)
            {
                lastFile = filePath;
                lstResult.Items.Add(filePath + ":");
            }
            lstResult.Items.Add(innerPath);
        }

        private bool validateRegex()
        {
            if (cbRegx.Checked)
            {
                try
                {
                    Regex.IsMatch("abcdefg", txtSearch.Text, RegexOptions.IgnoreCase);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private void msg(string txt)
        {
            MessageBox.Show(txt);
        }

        private void cbRegx_CheckedChanged(object sender, EventArgs e)
        {
            cbStrict.Locked = cbRegx.Checked;
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            run = false;
            Environment.Exit(0);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            run = false;
        }

        private void txtFolder_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void txtFolder_DragDrop(object sender, DragEventArgs e)
        {
            string pt = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            if (Directory.Exists(pt))
            {
                txtFolder.Text = pt;
            }
        }

        private void lstResult_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (e.Index >= 0)
            {
                string txt = lstResult.Items[e.Index].ToString();
                bool hd = txt.Contains(":") && e.Index > 0;
                e.Graphics.DrawString(txt, new Font(lstResult.Font.FontFamily, lstResult.Font.Size, txt.Contains(":") ? FontStyle.Bold : FontStyle.Regular), new SolidBrush(lstResult.ForeColor), e.Bounds.Left, e.Bounds.Top + (hd ? 4 : 0));
            }
        }

        private void lstResult_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            string txt = lstResult.Items[e.Index].ToString();
            bool hd = txt.Contains(":") && e.Index > 0;
            e.ItemHeight += hd ? 4 : 1;
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            TextBox o = (TextBox)sender;
            o.BackColor = Color.WhiteSmoke;
        }

        private void txtFolder_Leave(object sender, EventArgs e)
        {
            TextBox o = (TextBox)sender;
            o.BackColor = Color.LightGray;
        }
    }

    public class XCheckBox : CheckBox
    {
        bool _lock = false;
        public bool Locked
        {
            get { return _lock; }
            set { _lock = value; this.Refresh(); }
        }
        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
            if (this.Checked)
            {
                if (!this.Locked)
                {
                    pevent.Graphics.FillRectangle(Brushes.DarkOrange, 2, 3, 9, 9);
                }
                else
                {
                    pevent.Graphics.FillRectangle(Brushes.Gray, 2, 3, 9, 9);
                }
            }
        }
    }
}
