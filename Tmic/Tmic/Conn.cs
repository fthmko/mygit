﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Data.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace Tmic
{
    /// <summary>
    /// Description of Conn.
    /// </summary>
    public class Conn
    {

        public const string BASE_SQL = "select Id, Key, Value, Type, Parent from tmic where type={0}";

        private static bool init = false;

        public static SQLiteConnection Connection;
        public static DataContext Context;

        public static void Init()
        {
            if (!init)
            {
                bool tbl = System.IO.File.Exists(Environment.CurrentDirectory + "\\data.sqlite");
                Connection = new SQLiteConnection("Data Source=data.sqlite;Pooling=true;FailIfMissing=false");
                init = true;
                Context = new DataContext(Connection);
                if (!tbl) CreateTable();
            }
        }

        private static void CreateTable()
        {
            string sqlMsg = "CREATE TABLE [tmic] ([id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[key] text  NOT NULL,[value] text  NOT NULL,[type] text  NOT NULL,[parent] inTEGER  NULL);";
            Context.ExecuteCommand(sqlMsg);
        }

        public static bool Execute(string sql, params object[] param)
        {
            if (!init) return false;
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            int result = 0;
            result = Context.ExecuteCommand(sql, param);
            //try
            //{
            //    result = context.ExecuteCommand(sql, param);
            //}
            //catch
            //{
            //    result = 0;
            //}
            return result > 0;
        }

        public static BindingList<T> QueryB<T>(string sql, params object[] param)
        {
        	List<T> lst = Query<T>(sql, param);
            BindingList<T> result = new BindingList<T>();
            foreach(T i in lst){
            	result.Add(i);
            }
            return result;
        }
        public static List<T> Query<T>(string sql, params object[] param)
        {
            if (!init) return null;
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            List<T> result = new List<T>();
            result.AddRange(Context.ExecuteQuery<T>(sql, param));
            return result;
        }
    }
}
