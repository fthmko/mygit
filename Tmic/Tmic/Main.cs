﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Tmic
{
    public partial class Main : Form
    {
        int nowConfig = -1;

        public Main()
        {
            InitializeComponent();
            Conn.Init();
            loadData();
        }

        private void loadData()
        {
            cmbConfig.DataSource = QueryB(Conn.BASE_SQL, "CONFIG");
            cmbConfig.DisplayMember = "Key";
            cmbConfig.ValueMember = "Id";
        }

        private void loadMap(){
            if (cmbConfig.SelectedIndex == -1) return;
            try
            {
                tmicTableAdapter.FillByParent(this.mapData.tmic, (int)cmbConfig.SelectedValue);
                nowConfig = (int)cmbConfig.SelectedValue;
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private BindingList<DataBean> QueryB(String sql, params object[] param)
        {
            return Conn.QueryB<DataBean>(Conn.BASE_SQL, param);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            loadMap();
        }

        private void gridMap_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[3].Value = nowConfig;
        }
    }

    public class DataBean
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public int? Parent { get; set; }
    }
}
