Public Class CfgMgr(Of T)

    Private real As T
    Private nm As String

    Public Sub New(ByVal filename As String)
        nm = filename
    End Sub
    Public Property Config() As T
        Get
            Return real
        End Get
        Set(ByVal value As T)
            real = value
        End Set
    End Property

    Public ReadOnly Property FileName() As String
        Get
            Return nm
        End Get
    End Property

    Public Function Load() As Boolean
        Try
            If IO.File.Exists(nm) Then
                Dim loader As System.Runtime.Serialization.IFormatter
                Dim fStream As New System.IO.FileStream(nm, IO.FileMode.Open)

                loader = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter

                real = loader.Deserialize(fStream)
                fStream.Close()
                Load = True
            Else
                Load = False
            End If
        Catch ex As Exception
            Load = False
        End Try
    End Function

    Public Function Save() As Boolean
        Try
            If IO.File.Exists(nm) Then IO.File.Delete(nm)
            Dim saver As System.Runtime.Serialization.IFormatter
            Dim fStream As New System.IO.FileStream(nm, IO.FileMode.CreateNew)

            saver = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter

            saver.Serialize(fStream, real)
            fStream.Flush()
            fStream.Close()
            Save = True
        Catch ex As Exception
            Save = False
        End Try
    End Function
End Class

<Serializable()> Public Class Cfgs
    Public ForeColor As Integer
    Public BackColor As Integer
    Public Font As Font
    Public Path As String
    Public TopMost As Boolean
    Public CodeId As Integer
    Public Size As Size
    Public Location As Point
    Public Opcity As Double
End Class