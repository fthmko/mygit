﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class kabone
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.kOpen = New System.Windows.Forms.ToolStripButton
        Me.kSave = New System.Windows.Forms.ToolStripButton
        Me.kCode = New System.Windows.Forms.ToolStripSplitButton
        Me.kGb = New System.Windows.Forms.ToolStripMenuItem
        Me.kBig = New System.Windows.Forms.ToolStripMenuItem
        Me.kUTF = New System.Windows.Forms.ToolStripMenuItem
        Me.kShift = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.kFind = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.kFont = New System.Windows.Forms.ToolStripButton
        Me.kFore = New System.Windows.Forms.ToolStripButton
        Me.kBg = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.kTop = New System.Windows.Forms.ToolStripButton
        Me.kNfi = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.kTxt = New System.Windows.Forms.RichTextBox
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.kOpen, Me.kSave, Me.kCode, Me.ToolStripSeparator1, Me.kFind, Me.ToolStripSeparator2, Me.kFont, Me.kFore, Me.kBg, Me.ToolStripSeparator3, Me.kTop})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(242, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'kOpen
        '
        Me.kOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kOpen.Image = Global.kAbone.My.Resources.Resources.OpenFolder
        Me.kOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kOpen.Name = "kOpen"
        Me.kOpen.Size = New System.Drawing.Size(23, 22)
        Me.kOpen.Text = "Open"
        '
        'kSave
        '
        Me.kSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kSave.Image = Global.kAbone.My.Resources.Resources.Save
        Me.kSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kSave.Name = "kSave"
        Me.kSave.Size = New System.Drawing.Size(23, 22)
        Me.kSave.Text = "Save"
        '
        'kCode
        '
        Me.kCode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kCode.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.kGb, Me.kBig, Me.kUTF, Me.kShift})
        Me.kCode.Image = Global.kAbone.My.Resources.Resources.VSObject_Enum
        Me.kCode.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kCode.Name = "kCode"
        Me.kCode.Size = New System.Drawing.Size(32, 22)
        Me.kCode.Text = "Encoding"
        '
        'kGb
        '
        Me.kGb.Checked = True
        Me.kGb.CheckState = System.Windows.Forms.CheckState.Checked
        Me.kGb.Name = "kGb"
        Me.kGb.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D1), System.Windows.Forms.Keys)
        Me.kGb.Size = New System.Drawing.Size(159, 22)
        Me.kGb.Text = "GB2312"
        '
        'kBig
        '
        Me.kBig.Name = "kBig"
        Me.kBig.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D2), System.Windows.Forms.Keys)
        Me.kBig.Size = New System.Drawing.Size(159, 22)
        Me.kBig.Text = "Big5"
        '
        'kUTF
        '
        Me.kUTF.Name = "kUTF"
        Me.kUTF.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D3), System.Windows.Forms.Keys)
        Me.kUTF.Size = New System.Drawing.Size(159, 22)
        Me.kUTF.Text = "UTF-8"
        '
        'kShift
        '
        Me.kShift.Name = "kShift"
        Me.kShift.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D4), System.Windows.Forms.Keys)
        Me.kShift.Size = New System.Drawing.Size(159, 22)
        Me.kShift.Text = "Shift_JIS"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'kFind
        '
        Me.kFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kFind.Image = Global.kAbone.My.Resources.Resources.Search
        Me.kFind.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kFind.Name = "kFind"
        Me.kFind.Size = New System.Drawing.Size(23, 22)
        Me.kFind.Text = "Search"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'kFont
        '
        Me.kFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kFont.Image = Global.kAbone.My.Resources.Resources.Font
        Me.kFont.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kFont.Name = "kFont"
        Me.kFont.Size = New System.Drawing.Size(23, 22)
        Me.kFont.Text = "Font"
        '
        'kFore
        '
        Me.kFore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kFore.Image = Global.kAbone.My.Resources.Resources.Forecolor
        Me.kFore.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kFore.Name = "kFore"
        Me.kFore.Size = New System.Drawing.Size(23, 22)
        Me.kFore.Text = "ForeColor"
        '
        'kBg
        '
        Me.kBg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kBg.Image = Global.kAbone.My.Resources.Resources.Color_fill
        Me.kBg.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kBg.Name = "kBg"
        Me.kBg.Size = New System.Drawing.Size(23, 22)
        Me.kBg.Text = "BackColor"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'kTop
        '
        Me.kTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.kTop.Image = Global.kAbone.My.Resources.Resources.Control_Checkbox_Un
        Me.kTop.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.kTop.Name = "kTop"
        Me.kTop.Size = New System.Drawing.Size(23, 22)
        Me.kTop.Text = "TopMost"
        '
        'kNfi
        '
        Me.kNfi.Text = "kAbone"
        '
        'kTxt
        '
        Me.kTxt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.kTxt.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.kTxt.Location = New System.Drawing.Point(0, 25)
        Me.kTxt.Name = "kTxt"
        Me.kTxt.ReadOnly = True
        Me.kTxt.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.kTxt.Size = New System.Drawing.Size(242, 248)
        Me.kTxt.TabIndex = 2
        Me.kTxt.Text = ""
        '
        'kabone
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(242, 273)
        Me.Controls.Add(Me.kTxt)
        Me.Controls.Add(Me.ToolStrip1)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(250, 300)
        Me.Name = "kabone"
        Me.Text = "kAbone"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents kOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents kSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents kFont As System.Windows.Forms.ToolStripButton
    Friend WithEvents kTop As System.Windows.Forms.ToolStripButton
    Friend WithEvents kFind As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents kCode As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents kGb As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents kUTF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents kShift As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents kBig As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents kFore As System.Windows.Forms.ToolStripButton
    Friend WithEvents kBg As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents kNfi As System.Windows.Forms.NotifyIcon
    Friend WithEvents kTxt As System.Windows.Forms.RichTextBox

End Class
