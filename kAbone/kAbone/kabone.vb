Public Class kabone

    Dim chk As ToolStripMenuItem
    Dim code As System.Text.Encoding
    Dim fd As srch
    Dim cfg As CfgMgr(Of Cfgs)

    Private Sub kabone_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.kNfi.Icon = Global.kAbone.My.Resources.Globe
        Me.Icon = Global.kAbone.My.Resources.Globe
        chk = kGb
        code = System.Text.Encoding.GetEncoding("GB2312")
        cfg = New CfgMgr(Of Cfgs)(Application.StartupPath & "\kAbone.dat")
        If cfg.Load Then
            kTxt.ForeColor = Color.FromArgb(cfg.Config.ForeColor)
            kTxt.BackColor = Color.FromArgb(cfg.Config.BackColor)
            kTxt.Font = cfg.Config.Font
            Me.Location = cfg.Config.Location
            Me.Size = cfg.Config.Size
            Me.Opacity = cfg.Config.Opcity
            Me.TopMost = Not cfg.Config.TopMost
            kTop_Click(Nothing, Nothing)
        Else
            cfg.Config = New Cfgs
            cfg.Config.Path = String.Empty
            cfg.Config.CodeId = 0
            cfg.Config.Font = kTxt.Font
            cfg.Config.ForeColor = kTxt.ForeColor.ToArgb
            cfg.Config.BackColor = kTxt.BackColor.ToArgb
            cfg.Config.TopMost = False
            cfg.Config.Size = Size
        End If
        Select Case cfg.Config.CodeId
            Case 0
                kGb_Click(kGb, Nothing)
            Case 1
                kUTF_Click(kUTF, Nothing)
            Case 2
                kBig_Click(kBig, Nothing)
            Case 3
                kShift_Click(kShift, Nothing)
        End Select
        doOpen()
    End Sub

    Private Sub kabone_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Space Then
            System.Windows.Forms.SendKeys.Send("{PGDN}")
        ElseIf e.KeyCode = Keys.O Then
            kOpen_Click(0, New System.EventArgs)
        ElseIf e.KeyCode = Keys.S Then
            kSave_Click(0, New System.EventArgs)
        ElseIf e.KeyCode = Keys.F Then
            kFind_Click(0, New System.EventArgs)
        ElseIf e.KeyCode = Keys.T Then
            kTop_Click(0, New System.EventArgs)
        ElseIf e.KeyCode = Keys.U Then
            If Me.Opacity < 1 Then Me.Opacity = Me.Opacity + 0.1
        ElseIf e.KeyCode = Keys.D Then
            If Me.Opacity > 0.1 Then Me.Opacity = Me.Opacity - 0.1
        ElseIf e.KeyCode = Keys.M Then
            ToolStrip1.Visible = Not ToolStrip1.Visible
        ElseIf e.KeyCode = Keys.G Then
            If Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable Then
                ToolStrip1.Visible = False
                Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            Else
                Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
                ToolStrip1.Visible = True
            End If
        ElseIf e.KeyCode = Keys.Z Then
            Me.Hide()
            kNfi.Visible = True
        End If
    End Sub

    Private Sub kabone_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        cfg.Config.Opcity = Me.Opacity
        cfg.Config.Size = Me.Size
        cfg.Config.Location = Me.Location
        cfg.Save()
        If cfg.Config.Path = String.Empty Then
            Return
        End If
        If kTxt.GetCharIndexFromPosition(New System.Drawing.Point(5, 5)) > 30 Then
            If MsgBox("SAVE before exit?", MsgBoxStyle.YesNo, "Message") = MsgBoxResult.Yes Then
                doSave()
            End If
        End If
    End Sub

    Private Sub kOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kOpen.Click
        Dim dlgOp As New OpenFileDialog
        dlgOp.Filter = "Text File(*.txt)|*.txt"
        dlgOp.Title = "Open"
        If dlgOp.ShowDialog() = Windows.Forms.DialogResult.OK Then
            cfg.Config.Path = dlgOp.FileName
            doOpen()
            dlgOp.Dispose()
        End If
        ReduceMemory()
    End Sub

    Private Sub kSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kSave.Click
        If Not cfg.Config.Path = String.Empty Then
            If MsgBox("The file [ " & cfg.Config.Path & " ] will be CHANGED." & vbCrLf & " Are you sure?", MsgBoxStyle.YesNo, "Message") = MsgBoxResult.Yes Then
                doSave()
                Me.Text = "[" & kTxt.Text.Length & "] " & cfg.Config.Path
            End If
        End If
        cfg.Save()
        ReduceMemory()
    End Sub

    Private Sub kFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kFind.Click
        fd = New srch(kTxt)
        fd.ShowDialog()
        ReduceMemory()
    End Sub

    Private Sub kNfi_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles kNfi.MouseDoubleClick
        Me.Show()
        kNfi.Visible = False
    End Sub
#Region "Encoding"
    Private Sub kGb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kGb.Click
        If Not sender.Checked Then
            chk.Checked = False
            chk = sender
            chk.Checked = True
            code = System.Text.Encoding.GetEncoding("GB2312")
            cfg.Config.CodeId = 0
            If Not cfg.Config.Path = String.Empty Then
                doOpen()
            End If
        End If
        ReduceMemory()
    End Sub

    Private Sub kUTF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kUTF.Click
        If Not sender.Checked Then
            chk.Checked = False
            chk = sender
            chk.Checked = True
            code = System.Text.Encoding.UTF8
            cfg.Config.CodeId = 1
            If Not cfg.Config.Path = String.Empty Then
                doOpen()
            End If
        End If
        ReduceMemory()
    End Sub

    Private Sub kBig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kBig.Click
        If Not sender.Checked Then
            chk.Checked = False
            chk = sender
            chk.Checked = True
            code = System.Text.Encoding.GetEncoding("Big5")
            cfg.Config.CodeId = 2
            If Not cfg.Config.Path = String.Empty Then
                doOpen()
            End If
        End If
        ReduceMemory()
    End Sub

    Private Sub kShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kShift.Click
        If Not sender.Checked Then
            chk.Checked = False
            chk = sender
            chk.Checked = True
            code = System.Text.Encoding.GetEncoding("Shift_JIS")
            cfg.Config.CodeId = 3
            If Not cfg.Config.Path = String.Empty Then
                doOpen()
            End If
        End If
        ReduceMemory()
    End Sub
#End Region

#Region "Style"
    Private Sub kFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kFont.Click
        Dim dlgFont As New FontDialog
        dlgFont.Font = kTxt.Font
        If dlgFont.ShowDialog() = Windows.Forms.DialogResult.OK Then
            kTxt.Font = dlgFont.Font
            cfg.Config.Font = kTxt.Font
        End If
        dlgFont.Dispose()
        ReduceMemory()
    End Sub

    Private Sub kFore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kFore.Click
        Dim dlgColor As New ColorDialog
        dlgColor.Color = kTxt.ForeColor
        If dlgColor.ShowDialog() = Windows.Forms.DialogResult.OK Then
            kTxt.ForeColor = dlgColor.Color
            cfg.Config.ForeColor = kTxt.ForeColor.ToArgb
        End If
        dlgColor.Dispose()
        ReduceMemory()
    End Sub

    Private Sub kBg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kBg.Click
        Dim dlgColor As New ColorDialog
        dlgColor.Color = kTxt.BackColor
        If dlgColor.ShowDialog() = Windows.Forms.DialogResult.OK Then
            kTxt.BackColor = dlgColor.Color
            cfg.Config.BackColor = kTxt.BackColor.ToArgb
        End If
        dlgColor.Dispose()
        ReduceMemory()
    End Sub

    Private Sub kTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kTop.Click
        TopMost = Not TopMost
        cfg.Config.TopMost = TopMost
        If TopMost Then
            Me.kTop.Image = Global.kAbone.My.Resources.Control_Checkbox
        Else
            Me.kTop.Image = Global.kAbone.My.Resources.Control_Checkbox_Un
        End If
        ReduceMemory()
    End Sub
#End Region

    Private Sub ReduceMemory()
        Dim A As Process = Process.GetCurrentProcess()
        A.MaxWorkingSet = Process.GetCurrentProcess.MaxWorkingSet
        A.Dispose()
    End Sub

    Private Sub doOpen()
        If cfg.Config.Path = String.Empty Then
            Return
        End If
        Try
            Dim reader As System.IO.StreamReader = New System.IO.StreamReader(cfg.Config.Path, code)
            kTxt.Text = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Me.Text = "[" & kTxt.Text.Length & "] " & cfg.Config.Path
        Catch ex As Exception
            kTxt.Text = String.Empty
            MsgBox("Can't open [ " & cfg.Config.Path & " ].")
            cfg.Config.Path = String.Empty
        End Try
    End Sub

    Private Sub doSave()
        Dim writer As System.IO.StreamWriter = New System.IO.StreamWriter(cfg.Config.Path, False, code)
        kTxt.Select(0, kTxt.GetCharIndexFromPosition(New System.Drawing.Point(5, 5)))
        kTxt.SelectedText = " "
        writer.Write(kTxt.Text)
        writer.Flush()
        writer.Close()
        writer.Dispose()
    End Sub

End Class
