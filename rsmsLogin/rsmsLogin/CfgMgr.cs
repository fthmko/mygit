﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;

namespace rsmsLogin {
    public class CfgMgr<T> {
        public static CfgMgr<Storage> Manager;

        public String FileName {
            get { return nm; }
        }
        public T Config {
            get { return real; }
            set { real = value; }
        }
        public bool Status {
            get;
            set;
        }
        private T real;
        private String nm;


        public CfgMgr(String fileName) {
            nm = fileName;
        }

        public bool Save() {
            Status = false;
            try {
                if (File.Exists(nm)) File.Delete(nm);
                FileStream fs = new FileStream(nm, FileMode.CreateNew);
                IFormatter saver = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                saver.Serialize(fs, real);
                fs.Close();
                Status = true;
            } catch {
            }
            return Status;
        }

        public bool Load() {
            Status = false;
            try {
                if (!File.Exists(nm)) return false;
                FileStream fs = new FileStream(nm, FileMode.Open);
                IFormatter loader = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                real = (T)loader.Deserialize(fs);
                fs.Close();
                Status = true;
            } catch {
            }
            return Status;
        }
    }


    [Serializable]
    public class Storage {
        private string _umpwd;
        private string _pcpwd;
        public string UMUser { get; set; }
        public string UMPwd { get { return StringDecoding(_umpwd); } set { _umpwd = StringEncoding(value); } }
        public string PCUser { get; set; }
        public string PCPwd { get { return StringDecoding(_pcpwd); } set { _pcpwd = StringEncoding(value); } }
        public Storage()
        {
            UMUser = "";
            UMPwd = "";
            PCUser = "";
            PCPwd = "";
        }
        static string StringEncoding(string pwd) {
            char[] arrChar = pwd.ToCharArray();
            string strChar = "";
            for (int i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] + 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }

        static string StringDecoding(string pwd) {
            char[] arrChar = pwd.ToCharArray();
            string strChar = "";
            for (int i = 0; i < arrChar.Length; i++) {
                arrChar[i] = Convert.ToChar(arrChar[i] - 1);
                strChar = strChar + arrChar[i].ToString();
            }
            return strChar;
        }
    }
}