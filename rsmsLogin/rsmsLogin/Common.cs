﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Runtime.InteropServices;
using System.Net;
using Microsoft.Win32;
using System.Threading;

namespace rsmsLogin
{
    class Common
    {
        public const string URL_HOME = "http://rsms.paic.com.cn/rsms/";
        public const string URL_LOGIN = "j_security_check";
        public const string URL_QUERY = "appListNormal.screen?cmd=app&txtAppName={0}";
        ///appinfoNormal.screen?cmd=ajax&appType=2012&appid=27413&appEnv=0&t=1413708040109
        public const string URL_DETAIL = "appinfoNormal.screen?cmd=ajax&appid={0}";

        public static string lastSrc;
        private static string hcMsg;
        private const string ENCODE = "gbk";
        private static HttpClient hc = new HttpClient();

        public static void Login(string usr, string pwd)
        {
            getSrc(URL_HOME);
            MapDict param = new MapDict();
            param.Add("j_username", usr);
            param.Add("j_password", pwd);
            postSrc(URL_HOME + URL_LOGIN, buildParam(param));
            getSrc(URL_HOME);
        }

        public static List<MapDict> SearchEnv(string appName){
            string res = getSrc(URL_HOME + String.Format(URL_QUERY, appName));
            List<MapDict> result = new List<MapDict>();
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(res);
            HtmlNode tbl = hd.DocumentNode.SelectSingleNode("//table[2]");
            if (tbl != null)
            {
                HtmlNodeCollection trs = tbl.SelectNodes("tr[position() mod 2=0]");
                foreach (HtmlNode nd in trs)
                {
                    MapDict mp = new MapDict("appName", "appName");
                    mp.Add("envId", nd.SelectSingleNode("td[3]").Attributes["appid"].Value.Trim());
                    mp.Add("appName", nd.SelectSingleNode("td[4]").InnerText.Trim());
                    mp.Add("envType", nd.SelectSingleNode("td[6]").InnerText.Trim());
                    mp.Add("envName", nd.SelectSingleNode("td[7]").InnerText.Trim());
                    result.Add(mp);
                }
            }
            return result;
        }

        public static List<List<MapDict>> GetEnvDetail(string envid)
        {
            List<List<MapDict>> result = new List<List<MapDict>>();
            string res = getSrc(URL_HOME + String.Format(URL_DETAIL, envid));
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(res);
            HtmlNode tbl = hd.DocumentNode.SelectSingleNode("/html/body/div[1]/table[2]");
            if (tbl != null)
            {
                HtmlNodeCollection trs = tbl.SelectNodes("tr[position() mod 2=0]");
                if (trs != null)
                {
                    foreach (HtmlNode nd in trs)
                    {
                        MapDict info = new MapDict();
                        info.Add("le", nd.SelectSingleNode("//input[contains(@class,'txtLE')]").Attributes["value"].Value.Trim());
                    }
                }
            }
            return result;
        }

        public static List<MapDict> GetRmContent(string rmid)
        {
            String res = getSrc("" + "?rmId=" + rmid);
            if (res.Contains("查看 <a href=\"javascript:showhide('detail')\">详细错误信息"))
            {
                return null;
            }
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(res);
            HtmlNode tbl = hd.DocumentNode.SelectSingleNode("//input[@id='changereason']/following::table");
            HtmlNodeCollection trs = tbl.SelectNodes("tr[position()>1]");
            List<MapDict> content = new List<MapDict>();
            if (trs != null)
            {
                foreach (HtmlNode nd in trs)
                {
                    MapDict mp = new MapDict("srId", "srId");
                    mp.Add("srId", nd.SelectSingleNode("td[2]").InnerText.Trim());
                    mp.Add("title", nd.SelectSingleNode("td[3]").InnerHtml.Trim());
                    mp.Add("source", nd.SelectSingleNode("td[5]").InnerHtml.Trim());
                    mp.Add("status", nd.SelectSingleNode("td[4]").InnerHtml.Trim());
                    mp.Add("needId", nd.SelectSingleNode("td[6]").InnerText.Trim());
                    mp.Add("subSys", nd.SelectSingleNode("td[7]").InnerHtml.Trim());
                    mp.Add("commiter", nd.SelectSingleNode("td[8]").InnerHtml.Trim());
                    mp.Add("devs", nd.SelectSingleNode("td[10]").InnerHtml.Trim());
                    content.Add(mp);
                }
            }
            return content;
        }

        #region ASSIST
        public static string Join(List<string> array, string splitStr)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string info in array)
            {
                sb.AppendFormat("{0}{1}", info.ToString(), splitStr);
            }
            return sb.ToString().Substring(0, sb.Length - splitStr.Length);
        }
        public static string getSrc(string url)
        {
            string src = hc.GetSrc(url, ENCODE, out hcMsg);
            lastSrc = src;
            if (hcMsg != string.Empty)
            {
                Console.WriteLine(hcMsg);
                return null;
            }
            dbg(src);
            return src;
        }

        public static string postSrc(string url, string data)
        {
            string src = hc.PostData(url, data, ENCODE, ENCODE, out hcMsg);
            lastSrc = src;
            if (hcMsg != string.Empty)
            {
                Console.WriteLine(hcMsg);
                return null;
            }
            dbg(src);
            return src;
        }

        public static string buildParam(MapDict prm)
        {
            string param = "";
            foreach (var i in prm)
            {
                param = param + "&" + i.Key + "=" + i.Value;
            }
            return param.Substring(1);
        }

        public static string one(string html)
        {
            return html.Replace("\t", "").Replace("\r", "").Replace("\n", "");
        }

        public static void dbg(string txt)
        {
#if DEBUG
            System.IO.File.WriteAllText("xdebug.log", txt);
#endif
        }

        public static string findText(string src, string reg)
        {
            if (string.IsNullOrEmpty(src)) return null;
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return null;
            return mc[0].Value.Replace("&amp;", "&");
        }

        public static string findText(string src, string reg, int group)
        {
            if (string.IsNullOrEmpty(src)) return null;
            MatchCollection mc = Regex.Matches(src, reg, RegexOptions.IgnoreCase);
            if (mc.Count < 1) return null;
            return mc[0].Groups[group].Value.Replace("&amp;", "&");
        }

        #endregion
    }

    public class MapDict : Dictionary<string, string>, IComparable
    {
        public string DispKey { get; set; }
        public string SortKey { get; set; }
        public string Name { get { return this.ToString(); } }
        public MapDict Value { get { return this; } }

        public MapDict()
        {
            DispKey = null;
            SortKey = null;
        }
        public MapDict(string disp)
        {
            DispKey = disp;
            SortKey = disp;
        }
        public MapDict(string disp, string sort)
        {
            DispKey = disp;
            SortKey = sort;
        }

        int IComparable.CompareTo(object obj)
        {
            return this[SortKey].CompareTo(((MapDict)obj)[SortKey]);
        }

        public override string ToString()
        {
            return this[DispKey];
        }
    }
}
