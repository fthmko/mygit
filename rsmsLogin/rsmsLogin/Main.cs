﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace rsmsLogin
{
    public partial class Main : Form
    {
        CfgMgr<Storage> cfgMgr = new CfgMgr<Storage>("config.bin");


        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if (cfgMgr.Load())
            {

            }
            else
            {

            };
            Common.Login(cfgMgr.Config.UMUser, cfgMgr.Config.UMPwd);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            List<MapDict> result = Common.SearchEnv(txtAppName.Text);
            Dictionary<string, TreeNode> cache = new Dictionary<string, TreeNode>();
            foreach (MapDict md in result)
            {
                TreeNode appTr;
                if (cache.ContainsKey(md["appName"]))
                {
                    appTr = cache[md["appName"]];
                }
                else
                {
                    appTr = new TreeNode(md["appName"]);
                    treeView.Nodes.Add(appTr);
                    cache.Add(md["appName"], appTr);
                }
                TreeNode tn = new TreeNode("[" + md["envType"] + "]" + md["envName"]);
                tn.Tag = md;
                appTr.Nodes.Add(tn);

            }
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Nodes.Count == 0)
            {
                MapDict env = e.Node.Tag as MapDict;
                List<List<MapDict>> detail = Common.GetEnvDetail(env["envId"]);
            }
        }

    }
}
