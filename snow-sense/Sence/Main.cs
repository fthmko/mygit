using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Sence
{
    public partial class Main : Form
    {
        static int BPP = 4;
        int maxWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
        int maxHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - 20;
        int _windDelta;
        int _speedDelta;
        Brush snowBrush;
        Brush transBrush = Brushes.Black;
        Random rnd;
        Graphics gc;
        Bitmap screenBitmap;
        bool refreshFlag;
        bool lockFlag;
        bool symbolFlag;

        List<float[]> runningPoint;
        List<float[]> stayingPoint;

        PrcDelegate procFunc;

        delegate Bitmap PrcDelegate(Bitmap bmp);

        public Main()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            init();
        }

        public void init()
        {
            screenBitmap = new Bitmap(maxWidth, maxHeight);
            Cfg.Load();
            symbolFlag = Cfg.Symbol.Length > 0;

            if (Cfg.Type == 0) procFunc = new PrcDelegate(GRoberts);
            else procFunc = new PrcDelegate(GSobel);

            runningPoint = new List<float[]>();
            stayingPoint = new List<float[]>();

            _windDelta = Cfg.MaxWind - Cfg.MinWind;
            _speedDelta = Cfg.MaxSpeed - Cfg.MinSpeed;
            timer1.Interval = Cfg.Time;
            timer2.Interval = Cfg.RefreshTime;
            snowBrush = new SolidBrush(Color.FromArgb(Cfg.Color));
            rnd = new Random();
            gc = Graphics.FromHwnd(mbd.Handle);
            gc.Clear(this.TransparencyKey);

            timer2_Tick(null, null);
            timer1.Enabled = true;

            lockFlag = true;
            refreshFlag = true;
            if (Cfg.Auto) timer2.Enabled = true;
            else timer2.Enabled = false;
        }

        private void fillPoint()
        {
            float[] point;
            if (runningPoint.Count + stayingPoint.Count < Cfg.Max)
            {
                point = new float[4];
                point[0] = rnd.Next(maxWidth); // X position
                point[1] = 0;                  // Y position
                point[2] = (float)rnd.NextDouble() * _speedDelta + Cfg.MinSpeed; // Y Speed
                point[3] = (float)rnd.NextDouble() * _windDelta + Cfg.MinWind;   // X Speed
                runningPoint.Add(point);
            }
            else if (stayingPoint.Count > 0)
            {
                gc = Graphics.FromHwnd(mbd.Handle);
                point = stayingPoint[0];
                stayingPoint.RemoveAt(0);
                gc.FillRectangle(transBrush, point[0], point[1], Cfg.Size, Cfg.Size);
                point[0] = rnd.Next(maxWidth);
                point[1] = 0;
                runningPoint.Add(point);
            }
            else
            {
                timer1.Enabled = false;
                MessageBox.Show("Running Error!");
            }
        }

        private void Flow()
        {
            lockFlag = false;
            float nextX, nextY;
            int realCount, i, staySeed;
            gc = Graphics.FromHwnd(mbd.Handle);
            realCount = stayingPoint.Count;
            i = 0;
            staySeed = rnd.Next(10);

            // 如果屏幕发生了刷新
            while (i < realCount && refreshFlag)
            {
                nextY = stayingPoint[i][1] + stayingPoint[i][2];
                nextX = stayingPoint[i][0] + stayingPoint[i][3];

                if (nextY >= maxHeight || nextY < 0)
                {
                    stayingPoint.RemoveAt(i);
                    realCount--;
                }
                else
                {
                    if (nextX >= maxWidth) nextX -= maxWidth;
                    else if (nextX < 0) nextX += maxWidth;
                    if (!chk(stayingPoint[i][0], stayingPoint[i][1], nextX, nextY))
                    {
                        runningPoint.Add(stayingPoint[i]);
                        stayingPoint.RemoveAt(i);
                        realCount--;
                    }
                    else i++;
                }
            }
            realCount = runningPoint.Count;
            i = 0;

            // 重绘移动中的雪花
            while (i < realCount)
            {
                // 计算下一位置
                nextY = runningPoint[i][1] + runningPoint[i][2];
                nextX = runningPoint[i][0] + runningPoint[i][3];

                // 超出屏幕
                if (nextY >= maxHeight || nextY < 0)
                {
                    gc.FillRectangle(transBrush, runningPoint[i][0], runningPoint[i][1], Cfg.Size, Cfg.Size);
                    runningPoint.RemoveAt(i);
                    realCount--;
                }
                else
                {
                    // 左右循环
                    if (nextX >= maxWidth) nextX -= maxWidth;
                    else if (nextX < 0) nextX += maxWidth;

                    // 控制停留
                    if (staySeed < Cfg.Chance && chk(runningPoint[i][0], runningPoint[i][1], nextX, nextY))
                    {
                        stayingPoint.Add(runningPoint[i]);
                        runningPoint.RemoveAt(i);
                        realCount--;
                    }
                    else
                    {
                        drawPoint(transBrush, runningPoint[i][0], runningPoint[i][1]);
                        drawPoint(snowBrush, nextX, nextY);
                        runningPoint[i][0] = nextX;
                        runningPoint[i][1] = nextY;
                        i++;
                    }
                }
            }
            while (i < Cfg.Keep)
            {
                fillPoint();
                i++;
            }
            if (!Cfg.Auto) refreshFlag = false;
            lockFlag = true;
        }

        private void drawPoint(Brush br, float x, float y)
        {
            if (symbolFlag) gc.DrawString(Cfg.Symbol, this.Font, br, x, y);
            else gc.FillEllipse(br, x, y, Cfg.Size, Cfg.Size);
        }

        private bool chk(float ox, float oy, float nx, float ny)
        {
            if (ox > 3 && Math.Abs(screenBitmap.GetPixel((int)ox, (int)oy).B - screenBitmap.GetPixel((int)nx, (int)ny).B) > Cfg.Level) return true;
            return false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (lockFlag) Flow();
        }

        public Bitmap getArea()
        {
            Bitmap bmp = new Bitmap(maxWidth, maxHeight);
            Graphics gx = Graphics.FromImage(bmp);
            try
            {
                gx.CopyFromScreen(0, 0, 0, 0, new Size(maxWidth, maxHeight), CopyPixelOperation.SourceCopy);
            }
            catch
            {
                MessageBox.Show("Can't Refresh Screen!");
                timer2.Enabled = false;
            }
            return bmp;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            procFunc.BeginInvoke(getArea(), MethodCompleted, procFunc);
        }

        private void MethodCompleted(IAsyncResult asyncResult)
        {
            if (asyncResult == null) return;
            screenBitmap = (asyncResult.AsyncState as PrcDelegate).EndInvoke(asyncResult);
            if (Cfg.Memory) ReduceMemory();
            else System.GC.Collect();
            refreshFlag = true;
        }

        public static Bitmap GRoberts(Bitmap b)
        {
            int width = b.Width;
            int height = b.Height;

            Bitmap dstImage = new Bitmap(width, height);

            BitmapData srcData = b.LockBits(new Rectangle(0, 0, width, height),
              ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            BitmapData dstData = dstImage.LockBits(new Rectangle(0, 0, width, height),
              ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            int stride = srcData.Stride;
            int offset = stride - width * BPP;

            unsafe
            {
                byte* src = (byte*)srcData.Scan0;
                byte* dst = (byte*)dstData.Scan0;

                int A, B;
                int C, D;
                byte gray;

                src += stride;
                dst += stride;

                for (int y = 1; y < height; y++)
                {
                    src += BPP;
                    dst += BPP;
                    for (int x = 1; x < width; x++)
                    {
                        A = src[0 - stride - BPP];
                        B = src[0 - stride];
                        C = src[0 - BPP];
                        D = src[0];
                        dst[0] = (byte)(Math.Sqrt((A - D) * (A - D) + (B - C) * (B - C)));

                        A = src[1 - stride - BPP];
                        B = src[1 - stride];
                        C = src[1 - BPP];
                        D = src[1];
                        dst[1] = (byte)(Math.Sqrt((A - D) * (A - D) + (B - C) * (B - C)));

                        A = src[2 - stride - BPP];
                        B = src[2 - stride];
                        C = src[2 - BPP];
                        D = src[2];
                        dst[2] = (byte)(Math.Sqrt((A - D) * (A - D) + (B - C) * (B - C)));

                        gray = (gray = dst[0] >= dst[1] ? dst[0] : dst[1]) >= dst[2] ? gray : dst[2];
                        dst[0] = gray;
                        dst[3] = src[3];

                        src += BPP;
                        dst += BPP;
                    }

                    src += offset;
                    dst += offset;
                }
            }

            b.UnlockBits(srcData);
            dstImage.UnlockBits(dstData);

            b.Dispose();
            return dstImage;
        }

        public static Bitmap GSobel(Bitmap b)
        {
            Matrix3x3 m = new Matrix3x3();

            m.Init(0);
            m.TopLeft = m.TopRight = -1;
            m.BottomLeft = m.BottomRight = 1;
            m.TopMid = -2;
            m.BottomMid = 2;
            m.calscale();
            Bitmap b1 = m.Convolute((Bitmap)b.Clone());

            m.Init(0);
            m.TopLeft = m.BottomLeft = -1;
            m.TopRight = m.BottomRight = 1;
            m.MidLeft = -2;
            m.MidRight = 2;
            m.calscale();
            Bitmap b2 = m.Convolute((Bitmap)b.Clone());

            m.Init(0);
            m.TopMid = m.MidRight = 1;
            m.MidLeft = m.BottomMid = -1;
            m.TopRight = 2;
            m.BottomLeft = -2;
            m.calscale();
            Bitmap b3 = m.Convolute((Bitmap)b.Clone());

            m.Init(0);
            m.TopMid = m.MidLeft = -1;
            m.MidRight = m.BottomMid = 1;
            m.TopLeft = -2;
            m.BottomRight = 2;
            m.calscale();
            Bitmap b4 = m.Convolute((Bitmap)b.Clone());

            b = Gradient(Gradient(b1, b2), Gradient(b3, b4));

            b1.Dispose(); b2.Dispose(); b3.Dispose(); b4.Dispose();

            return b.Clone() as Bitmap;
        }

        private static Bitmap Gradient(Bitmap b1, Bitmap b2)
        {
            int width = b1.Width;
            int height = b1.Height;

            BitmapData data1 = b1.LockBits(new Rectangle(0, 0, width, height),
              ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData data2 = b2.LockBits(new Rectangle(0, 0, width, height),
              ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* p1 = (byte*)data1.Scan0;
                byte* p2 = (byte*)data2.Scan0;

                int offset = data1.Stride - width * BPP;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int power = (int)Math.Sqrt((p1[0] * p1[0] + p2[0] * p2[0]));
                        p1[0] = (byte)(power > 255 ? 255 : power);
                        p1[1] = p1[2] = p1[0];

                        p1 += BPP;
                        p2 += BPP;
                    }

                    p1 += offset;
                    p2 += offset;
                }
            }

            b1.UnlockBits(data1);
            b2.UnlockBits(data2);

            Bitmap dstImage = (Bitmap)b1.Clone();

            b1.Dispose();
            b2.Dispose();

            return dstImage;
        }

        private void ReduceMemory()
        {
            System.Diagnostics.Process A = System.Diagnostics.Process.GetCurrentProcess();
            A.MaxWorkingSet = System.Diagnostics.Process.GetCurrentProcess().MaxWorkingSet;
            A.Dispose();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (new Settings().ShowDialog() == DialogResult.OK)
            {
                init();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer2_Tick(null, null);
        }
    }

    class Matrix3x3
    {
        public int BPP = 4;
        public int Scale;
        public int Offset;
        public int TopLeft;
        public int TopRight;
        public int BottomLeft;
        public int BottomRight;
        public int Center;
        public int TopMid;
        public int BottomMid;
        public int MidLeft;
        public int MidRight;

        public Matrix3x3()
        {
            Offset = 0;
        }
        public void calscale()
        {
            Scale = TopLeft + TopMid + TopRight + MidLeft + MidRight + Center + BottomLeft + BottomMid + BottomRight;
        }
        public void Init(int nm)
        {
            TopLeft = TopRight = BottomLeft = BottomRight = Center = TopMid = BottomMid = MidLeft = MidRight = nm;
            calscale();
        }
        public Bitmap Convolute(Bitmap srcImage)
        {
            if (Scale == 0) Scale = 1;

            int width = srcImage.Width;
            int height = srcImage.Height;

            Bitmap dstImage = (Bitmap)srcImage.Clone();

            BitmapData srcData = srcImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            BitmapData dstData = dstImage.LockBits(new Rectangle(0, 0, width, height),
            ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            int rectTop = 1;
            int rectBottom = height - 1;
            int rectLeft = 1;
            int rectRight = width - 1;

            unsafe
            {
                byte* src = (byte*)srcData.Scan0;
                byte* dst = (byte*)dstData.Scan0;

                int stride = srcData.Stride;
                int offset = stride - width * BPP;

                int pixel = 0;
                byte gray;

                src += stride;
                dst += stride;
                for (int y = rectTop; y < rectBottom; y++)
                {
                    src += BPP;
                    dst += BPP;

                    for (int x = rectLeft; x < rectRight; x++)
                    {
                        if (src[3] > 0)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                pixel = src[i - stride - BPP] * TopLeft + src[i - stride] * TopMid +
                                    src[i - stride + BPP] * TopRight + src[i - BPP] * MidLeft + src[i] * Center +
                                    src[i + BPP] * MidRight + src[i + stride - BPP] * BottomLeft + src[i + stride] * BottomMid
                                    + src[i + stride + BPP] * BottomRight;
                                pixel = pixel / Scale + Offset;

                                if (pixel < 0) pixel = 0;
                                if (pixel > 255) pixel = 255;

                                dst[i] = (byte)pixel;
                            }
                            gray = (gray = dst[0] >= dst[1] ? dst[0] : dst[1]) >= dst[2] ? gray : dst[2];
                            dst[0] = gray;
                        }
                        src += BPP;
                        dst += BPP;
                    }

                    src += (offset + BPP);
                    dst += (offset + BPP);
                }
            }

            dstImage.UnlockBits(dstData);

            srcImage.Dispose();

            return dstImage;
        }
    }
}